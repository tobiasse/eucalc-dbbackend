.PHONY: dev_install dev_clean dev_werkzeug dev_gunicorn modelapi docs install test load_test clean prod

dev_install:
	. venv/bin/activate; \
	python setup.py sdist; \
	pip install -e .; \
	pip install -r requirements_dev.txt; \
	proxilly -vvvv --development --log_to_console migrate; \
	proxilly -vvvv --development --log_to_console populate --all_countries -l tests/res/levers.txt -o tests/res/outputs.txt -u http://tool.european-calculator.eu/results; \

dev_clean:
	. venv/bin/activate; \
	python setup.py clean --all; \
	pip uninstall -y proxilly; \
	rm -r dist/; \
	rm -r proxilly.egg-info; \
	rm "${PWD}/instance/proxilly.sqlite"; \

dev_werkzeug:
	. venv/bin/activate; \
	export FLASK_APP="proxilly.app:create_app(1)"; \
	export FLASK_ENV='development'; \
	flask run --host 127.0.0.1 --port 5050; \

dev_gunicorn:
	. venv/bin/activate; \
	gunicorn -c python:instance.gunicorn.dev "proxilly.app:create_app(1)"; \

modelapi:
	. venv/bin/activate; \
	gunicorn -c python:tests.modelapimock.dev "tests.modelapimock.app:create_app()"; \

docs:
	. venv/bin/activate; \
	sphinx-build -b html docs/ docs/_build/; \

test:
	. venv/bin/activate; \
	python -m unittest discover -s tests/; \

load_test:
	. venv/bin/activate; \
	locust -L DEBUG -f tests/load_testing.py DefaultPathwayUser; \

install:
	. venv/bin/activate; \
	python setup.py sdist; \
	pip install -e .; \

clean:
	. venv/bin/activate; \
	python setup.py clean --all; \
	pip uninstall -y proxilly; \
	rm -r dist/; \
	rm -r proxilly.egg-info; \

prod_proxilly:
	. venv/bin/activate; \
	gunicorn -c python:instance.gunicorn.prod_proxilly "proxilly.app:create_app(0)"; \

prod_my2050:
	. venv/bin/activate; \
	gunicorn -c python:instance.gunicorn.prod_my2050 "proxilly.app:create_app(0, 1)"; \
