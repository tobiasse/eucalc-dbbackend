.. proxilly documentation master file, created by
   sphinx-quickstart on Mon Dec 30 10:01:13 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to proxilly's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
