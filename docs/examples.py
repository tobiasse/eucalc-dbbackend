from proxilly.marshalling.fields import Dict
from proxilly.marshalling.fields import Int
from proxilly.marshalling.fields import Str
from proxilly.marshalling.errors import ValidationError

schema = Dict(
    fields=[
        Str(key='first'),
        Str(key='last')
    ]
)

print(schema.parse({'first': 'foo', 'last': 'bar'}))  # {'first': 'foo', 'last': 'bar'}

try:
    schema.parse({'first': 'foo'})  # Raises ValidationError('Missing key "last".')
except ValidationError as err:
    print(err)

try:
    # Raises ValidationError('At index/key "first" type miss-match must be "str" but is "int".
    # At index/key "last" type miss-match must be "str" but is "bool".')
    schema.parse({'first': 12, 'last': True})
except ValidationError as err:
    print(err)

schema = Dict(
    fields=[
        Str(key='name'),
        Int(key='age', nullable=True),
        Str(key='gender', required=False, store_missing=True, nullable=True, missing=None),
        Str(key='country', required=False, missing='Taka-Tuka-Land', store_missing=True)
    ]
)

# {'name': 'foo', 'age': 12, 'gender': 'd', 'country': 'Germany'}
print(schema.parse({'name': 'foo', 'age': 12, 'gender': 'd', 'country': 'Germany'}))

# {'name': 'foo', 'age': None, 'gender': None, 'country': 'Taka-Tuka-Land'}
print(schema.parse({'name': 'foo', 'age': None}))


def one_of(gender: str) -> bool:
    return gender in ('m', 'f', 'd')


def lower(gender: str) -> str:
    return gender.lower()


def first(gender: str) -> str:
    return gender[0]


def in_range(number: int) -> bool:
    return 0 < number < 10


def division_by_two(number: int) -> bool:
    return number % 2 == 0


schema = Dict(
    fields=[
        Str(key='gender', validator=one_of, modifier=[lower, first]),
        Str(key='number', modifier=str),
        Int(key='division', validator=[in_range, division_by_two])
    ]
)

print(schema.parse({'gender': 'MALE', 'number': 12, 'division': 4}))  # {'gender': 'm', 'number': '12', 'division': 4}

try:
    # Raises ValidationError(At index/key "gender" validation "one_of(n)" failed.
    # At index/key "division" validation "division_by_two(9)" failed.)
    schema.parse({'gender': 'na', 'number': 12, 'division': 9})
except ValidationError as err:
    print(err)
