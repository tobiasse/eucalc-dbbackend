########
proxilly
########

Proxy server for the model API of the EUCalc project. Manages persistent storage of the model results and performs
basic load balancing.

************
Installation
************

Download the proxilly repository via ``git clone https://gitlab.pik-potsdam.de/tobiasse/eucalc-dbbackend`` and follow
the steps described in the following sections. Proxilly is developed with Python version 3.6.9 but should run as well
under newer versions (without warranty). To avoid polluting your global python environment you may want to install
`pyenv`_ and create a virtual environment within the projects root directory via ``python -m venv venv``.


Setup: development
==================

To install proxilly as a development API execute ``make dev_install`` in the project's root directory. This will install
all the required dependencies and create an SQLite development database. The development database is populated utilizing
:py:func:`proxilly.cli.populate.populate`, to alter this behavior edit in the Makefile the ``dev_install`` rule.
The arguments for the CLI tool are documented in the :doc:`api`.

After, installing proxilly you may start the development server by executing either ``make dev_werkzeug`` or
``make dev_gunicorn``. The latter command starts a gunicorn WSGI server by using the configuration file
``instance/gunicorn/dev.py`` and the former a werkzeug server with the following configuration file
``instance/gunicorn/dev.py``. Additionally, you may start for testing interactions a mock server of the EUCalc model API
by executing ``make modelapi``. The configuration and implementation of this server can be found at
``tests/res/modelapimock``.

Further, you can run via ``make load_test`` a load test through `locust.io`_, build the HTML version of the
documentation with ``make docs``, and test the application by executing ``make tests``.

For reverting the installation just execute ``make dev_clean``, it removes the development database and removes proxilly
from the venv.

Setup: production
=================

To install proxilly as a production API execute ``make install`` in the projects root directory. After, you may migrate
the database structure by executing ``proxilly migrate``. Before, migrating the database structure ensure that a user
and database exists corresponding to the parameters in the ``instance/flask/prod_proxilly.py`` config file (this file is
not content of the repository but available upon request to the author). Additionally, the following permissions are
required for the database user: SELECT, CREATE, REFERENCES, UPDATE, INSERT, and DROP.

- set up production server make prod
- set up production with systemd service ensure the unit file parameters correspond to the repositories props
- Execute systemd enable proxilly
- Check if logrotate has a service
- execute systemd enable logrotate.timer


**************
Work principle
**************

.. image:: db.png

***********
Maintenance
***********

- Outputs changed (alter table outputid), warn user that the application database is not updated

***********
Marshalling
***********

"In computer science, marshalling or marshaling is the process of transforming the memory representation of an
object to a data format suitable for storage or transmission, and it is typically used when data
must be moved between different parts of a computer program or from one program to another. Marshalling is similar
to serialization and is used to communicate to remote objects with an object, in this case a serialized object.
It simplifies complex communication, using composite objects in order to communicate instead of primitives.
The inverse of marshalling is called unmarshalling." (`Wikipedia`_, 31. January 2020)

Proxilly's marshalling sub-package is a library that handles the unmarshalling (deserialization) of JSON requests and
marshalling (serialization) of JSON responses. This package supports structural-, type-, and user-defined-validations.
Further, it supports the user defined modification of JSON values before serial- and deserialization.

The next two sections describe how to define schemas that are used during the marshalling and how to use this schemas
together with Python objects.

Defining schemas
================

Field schemas are used to deserialize JSON strings or Python dictionaries and to serialize objects to JSON strings or
Python dictionaries. Schemas are defined by using the public classes provided in the module
:py:mod:`proxilly.marshalling.fields`. The following example shows how to define a basic field schema.

.. literalinclude:: examples.py
    :lines: 1-25

An important constraint of field schemas is that the root field (the outermost field) must be an instance of
:py:class:`proxilly.marshalling.fields.Dict`. The parsing behavior of fields can be altered by using the instance
attributes detailed in :py:mod:`proxilly.marshalling.fields._Field`. The following example shows how to use these
parameters to alter the parsing results.

.. literalinclude:: examples.py
    :lines: 27-40

Further, all field types support user-defined validations and modifications. Validator functions must accept the
assigned field value as an argument and should return a boolean to indicate if the validation was successful. Similarly,
modification functions must accept the field value as an argument but should return the modified field value which is
subsequently assigned to the field. Programmatically, modifiers are executed before type validation and the validation
functions. Therefore, it is possible to change the field value type before any further validations are applied. The
usage of modifier and validator functions is presented in the following example.

.. literalinclude:: examples.py
    :lines: 43-78

- Dict and List fields (flat, subfield types)
- Dict no duplicate fields
- Keys can be string or regex

Defining objects
================


.. _pyenv: https://github.com/pyenv/pyenv
.. _wikipedia: https://en.wikipedia.org/wiki/Marshalling_(computer_science)
.. _locust.io: https://locust.io/