"""
setup
*****

:Author: Tobias Seydewitz
:Date: 13.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from setuptools import find_packages
from setuptools import setup

with open('proxilly/__init__.py') as src:
    for line in src:
        if line.find('__version__') >= 0:
            version = line.split('=')[-1].strip()
            version = version.strip("'")
            version = version.strip('"')

with open('requirements.txt') as src:
    requirements = [line.strip() for line in src]

setup(
    name='proxilly',
    author='Tobias Seydewitz',
    author_email='seydewitz@pik-potsdam.de',
    description='Database backend for the EUCALC project.',
    version=version,
    install_requires=requirements,
    packages=find_packages(),
    entry_points='''
    [console_scripts]
    proxilly=proxilly.cli.env:env
    ''',
)
