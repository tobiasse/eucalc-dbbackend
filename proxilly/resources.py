"""
resources
*********

:Author: Tobias Seydewitz
:Date: 10.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: This module comprises the webapp endpoints, a collection of error handlers, and decorators for HTTP header
    checking and database connection management.
"""
from typing import Tuple

from flask import Response
from flask import abort
from flask import current_app as app
from flask import jsonify
from flask import request
from flask.views import MethodView
from peewee import IntegrityError
from peewee import OperationalError
from requests import post as POST
from requests.exceptions import ConnectTimeout
from requests.exceptions import ConnectionError
from requests.exceptions import HTTPError
from requests.exceptions import ReadTimeout
from werkzeug.exceptions import HTTPException

from proxilly.marshalling.errors import ValidationError
from proxilly.models import Output
from proxilly.models import OutputID
from proxilly.models import Pathway
from proxilly.parsers import ModelOutputsErrorResponse
from proxilly.parsers import ModelOutputsRequest
from proxilly.parsers import ModelOutputsResponse
from proxilly.utils.outputs import OutputsGenerator


def connect_db(func):
    def wrapper(*args, **kwargs):
        db = app.config['DATABASE']

        try:
            db.connect(reuse_if_open=True)
            result = func(*args, **kwargs)
            return result

        except OperationalError as err:
            abort(503, description=str(err), response='')

        finally:
            db.close()

    return wrapper


def check_header(func):
    def decorator(*args, **kwargs):
        if not request.content_length:
            abort(411, description='Content-Length missing.', response='')

        elif request.content_length > app.config['MAX_CONTENT_LENGTH']:
            abort(
                413,
                description='Content-length exceeds limit of %s bytes.' % app.config['MAX_CONTENT_LENGTH'],
                response=''
            )

        elif not request.content_type or request.content_type.split(';')[0] != 'application/json':
            abort(400, description='Only content-type: application/json accepted.', response='')

        else:
            return func(*args, **kwargs)

    return decorator


def http_error(error: HTTPException) -> Tuple[Response, int]:
    """Return ``werkzeug.exceptions.HTTPExceptions`` as JSON response."""
    err = ModelOutputsErrorResponse(error.code, error.description, str(error.response))
    return jsonify(err.as_dict()), err.code


def validation_error(error: ValidationError) -> Tuple[Response, int]:
    """Return ``proxilly.marshall.ValidationError`` as JSON response."""
    err = ModelOutputsErrorResponse(400, str(error), '')
    return jsonify(err.as_dict()), 400


class Proxilly(MethodView):
    """Proxilly API endpoint."""
    decorators = [check_header, connect_db]

    def post(self) -> Response:
        """Model API proxy manages persistent storage of the model results and performs basic load balancing.

        Operates on basis of the following principle:
        If request in database return filtered result otherwise return nodata status.
        If request not in database and getFromModel flag is set try to call model API.
        If successful store model API results in DB and return results otherwise return busy status.

        IMPORTANT!
        The WSGI-Server of the model API must set the Accept-backlog to 0.
        This ensures that connections send to the model API will remain in the Syn-backlog.
        Sockets in the Syn-backlog are not established therefore a short connection timeout
        ensures that not more connections then workers are available are established to the model
        API.
        """
        req = ModelOutputsRequest.from_json(request.get_data(as_text=True))
        res = Pathway.get_outputs(req)

        if res:
            res = res.filter(req)

        elif not res and not req.get_from_model():
            res = ModelOutputsResponse.empty_nodata()

        else:
            res = self.handle_model_api_call(req)

        return jsonify(res.as_dict())

    def handle_model_api_call(self, req: ModelOutputsRequest) -> ModelOutputsResponse:
        """Method handles calls to the model API by creating a new request with all required frontend outputs, calling
        the model API (with a strict connection timeout to prevent overload of the API), and storing returned model
        results in the applications database.

        Args:
            req (``proxilly.parsers.ModelOutputsRequest``): The request send by the frontend.

        Returns:
            ``proxilly.parsers.ModelOutputsResponse``: In case of free model API resources model results as a
            ``ModelOutputsResponse``, otherwise empty instance of ``ModelOutputsResponse`` with status set to busy.

        Raises:
            ``marshalling.errors.ValidationError``: If model API response does not correspond to response schema.
        """
        # Prepare a new request with all outputs supported by the frontend
        if req.is_exceptional():
            outputs = OutputsGenerator(outputs=OutputID.get_frontend_outputs(), size=1, all_countries=False,
                                       exclude=['emissions-CO2e[Mt]', 'wat_WEI-normal[-]_S1', 'wat_WEI-normal[-]_S2'])
        else:
            outputs = OutputsGenerator(outputs=OutputID.get_frontend_outputs(), size=1, all_countries=True)

        new_req = ModelOutputsRequest(
            default=req.default,
            exceptions=req.exceptions,
            outputs=outputs.get(),
            getFromModel=req.getFromModel
        )

        # Call model API
        try:
            res = POST(
                app.config['MODEL_URL'],
                json=new_req.as_dict(),
                timeout=(app.config['CONN_TIMEOUT'], app.config['READ_TIMEOUT'])
            )
            res.raise_for_status()
        except ConnectTimeout:
            app.logger.warning('CONN TIMEOUT, model API busy can not serve an instance.')
            return ModelOutputsResponse.empty_busy()
        except ReadTimeout:
            app.logger.error('READ TIMEOUT, a instance hit the timeout consider to justify timeouts in the configs.')
            return ModelOutputsResponse.empty_busy()
        except (ConnectionError, HTTPError) as err:
            app.logger.error('"%s" REQUEST "is_exceptional=%s", CHECK MODEL API.', err, req.is_exceptional())
            return ModelOutputsResponse.empty_busy()

        # Set checked=True if the response should be evaluated before it gets stored
        res = ModelOutputsResponse.from_json(res.text, checked=False)

        # Dynamic caching
        try:  # Catches racing conditions
            pathway = Pathway.save_request(req)
            Output.create_response(pathway, res)
        except IntegrityError:
            pass

        return res.filter(req)


class Localised(MethodView):
    decorators = [check_header, connect_db]

    def post(self) -> Response:
        """Returns model outputs tuned for the localised project.

        Handles renaming of output ids according to the values stored in the application database.

        Differences to /results route or Proxilly class:
        - Always forwards the request to the model API
        - Frontend and non-frontend outputs are available
        - Renames output ids according to values in database (table OutputID, column rename)
        - If request is exceptional (exception country is set), returns only data for the selected country otherwise
          only EU data is returned (even when allCountries is set true)
        - Trims timeAxis to >=2015 and output data as well
        - Removes all outputs where data is empty
        """
        req = ModelOutputsRequest.from_json(request.get_data(as_text=True))
        req = req.filter(OutputID.get_all_outputs())

        if req.is_exceptional():
            outputs = OutputsGenerator(outputs=req.output_ids, size=1, all_countries=False,
                                       exclude=['emissions-CO2e[Mt]', 'wat_WEI-normal[-]_S1', 'wat_WEI-normal[-]_S2'])
        else:
            outputs = OutputsGenerator(outputs=req.output_ids, size=1, all_countries=True)

        new_req = ModelOutputsRequest(
            default=req.default,
            exceptions=req.exceptions,
            outputs=outputs.get(),
            getFromModel=req.getFromModel
        )

        # Call model API
        try:
            res = POST(
                app.config['MODEL_URL'],
                json=new_req.as_dict(),
                timeout=(app.config['CONN_TIMEOUT'], app.config['READ_TIMEOUT'])
            )
            res.raise_for_status()
        except ConnectTimeout:
            app.logger.warning('CONN TIMEOUT, model API busy can not serve an instance.')
            return jsonify(ModelOutputsResponse.empty_busy().as_dict())
        except ReadTimeout:
            app.logger.error('READ TIMEOUT, a instance hit the timeout consider to justify timeouts in the configs.')
            return jsonify(ModelOutputsResponse.empty_busy().as_dict())
        except (ConnectionError, HTTPError) as err:
            app.logger.error('"%s" REQUEST "is_exceptional=%s", CHECK MODEL API.', err, req.is_exceptional())
            return jsonify(ModelOutputsResponse.empty_busy().as_dict())

        res = ModelOutputsResponse.from_json(res.text, checked=False)

        res = res.singular(req)
        res = res.trim(2015)
        res = res.remove()
        res = res.rename(OutputID.get_renamed_outputs())

        return jsonify(res.as_dict())
