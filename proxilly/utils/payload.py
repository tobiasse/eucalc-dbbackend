from proxilly.utils.levers import LeversGenerator
from proxilly.utils.outputs import OutputsGenerator
from proxilly.parsers import ModelOutputsRequest


class PayloadGenerator:
    def __init__(self, levers, outputs):
        if not isinstance(levers, LeversGenerator) or not isinstance(outputs, OutputsGenerator):
            raise TypeError

        self.levers = levers
        self.outputs = outputs

    def get(self):
        return ModelOutputsRequest(
            default=self.levers.get(), outputs=self.outputs.get(), exceptions={}, getFromModel=False
        )

    def __iter__(self):
        for levers, outputs in zip(self.levers, self.outputs):
            yield ModelOutputsRequest(
                default=levers, outputs=outputs, exceptions={}, getFromModel=False
            )
