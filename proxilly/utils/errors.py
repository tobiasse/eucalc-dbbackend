"""
errors
******

:Author: Tobias Seydewitz
:Date: 24.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""


class EmptyFileError(Exception):
    """Raise if file is empty."""
