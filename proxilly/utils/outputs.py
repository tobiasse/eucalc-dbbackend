"""
outputs
*******

:Author: Tobias Seydewitz
:Date: 30.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: This module comprises classes to create output sequences that can be used to build
    properly formatted model API requests.
"""
from random import randint
from typing import List
from typing import Union

from proxilly.utils.mixins import FromFile


class OutputsGenerator(FromFile):
    """Creates output sequences that can be used for creating model API requests.

    Examples:
        >>> generator = OutputsGenerator(outputs=['foo', 'bar'], size=1, all_countries=True)
        >>> list(generator)
        [[{'id': 'foo', 'allCountries': True}, {'id': 'bar', 'allCountries': True}]]

    Attributes:
        outputs (``str``, list``): Path to file with outputs or a list of outputs. Outputs stored in file should be
            formatted line-wise (one output per line).
        size (``int``): The total of output sequences to create (see example) if the ``__iter__`` method is called.
        all_countries (``bool``, list``): Defines for which outputs the allCountries field is set to true or false.
            Can be a ``bool`` which sets all outputs to this or a list of outputs that should be set true.
    """
    def __init__(
            self,
            outputs: Union[
                str,
                List[str]
            ],
            size: int,
            all_countries: bool = True,
            exclude: List[str] = None,
    ) -> None:

        if type(outputs) == str:
            self.outputs = self.__class__.read_file(outputs)
        else:
            self.outputs = outputs

        if exclude:
            self.exclude = exclude
        else:
            self.exclude = []

        self.size = size
        self.all_countries = all_countries

    def _get_all_countries(self, name: str) -> bool:
        """Get for an output if allCountries set to true or false.

        Args:
            name (``str``): The name of of the output.

        Returns:
            ``bool``: A boolean indicating if for the output allCountries set to true or false.
        """
        if name in self.exclude:
            return not self.all_countries
        else:
            return self.all_countries

    def get(self) -> List[dict]:
        """Get a single outputs sequence (see example).

        Examples:
            >>> outputs = OutputsGenerator(outputs=['foo', 'bar'], size=20, all_countries=True, exclude=['bar'])
            >>> outputs.get()
            [{'id': 'foo', 'allCountries': True}, {'id': 'bar', 'allCountries': False}]

        Returns:
            ``list``: A list of dictionaries (see example).
        """
        return [
            {'id': name, 'allCountries': self._get_all_countries(name)}
            for name in self.outputs
        ]

    def _generator(self) -> List[dict]:
        """Generator function for output sequences.

        Generates as many sequences as by ``size`` defined.

        Returns:
            ``list``: A list of dictionaries.
        """
        for _ in range(self.size):
            yield [
                {'id': name, 'allCountries': self._get_all_countries(name)}
                for name in self.outputs
            ]

    def __iter__(self):
        yield from self._generator()

    def __len__(self):
        return self.size


class RandomizedOutputsGenerator(OutputsGenerator):
    """Creates random output sequences that can be used for creating model API requests.

    The number of elements per sequence is determined by ``randint(len(outputs))``. For each iteration
    a random element is drawn from ``outputs`` by the same function.

    Attributes:
        outputs (``str``, list``): Path to file with outputs or a list of outputs. Outputs stored in file should be
            formatted line-wise (one output per line).
        size (``int``): The total of output sequences to create (see example) if the ``__iter__`` method is called.
        all_countries (``bool``, list``): Defines for which outputs the allCountries field is set to true or false.
            Can be a ``bool`` which sets all outputs to this or a list of outputs that should be set true.
    """
    def __init__(
            self,
            outputs: Union[
                str,
                List[str]
            ],
            size: int,
            max_items: int,
            all_countries: Union[
                bool,
                List[str]
            ] = True
    ) -> None:

        super().__init__(outputs=outputs, size=size, all_countries=all_countries)

        self.items = len(self.outputs) - 1
        self.max_items = max_items

        if self.max_items < 1:
            raise AttributeError('"max_items" must be greater than one.')

        if self.max_items > self.items:
            raise AttributeError('"max_items" must be smaller than "outputs".')

    def get(self) -> List[dict]:
        """Get a single random outputs sequence.

        Returns:
            ``list``: A list of dictionaries.
        """
        idxs = [randint(0, self.items) for _ in range(randint(1, self.max_items))]

        return [
            {'id': self.outputs[idx], 'allCountries': self._get_all_countries(self.outputs[idx])}
            for idx in idxs
        ]

    def _generator(self) -> List[dict]:
        """Generator function for random output sequences.

        Generates as many sequences as by ``size`` defined.

        Returns:
            ``list``: A list of dictionaries.
        """
        for _ in range(self.size):
            idxs = [randint(0, self.items) for _ in range(randint(1, self.max_items))]

            yield [
                {'id': self.outputs[idx], 'allCountries': self._get_all_countries(self.outputs[idx])}
                for idx in idxs
            ]
