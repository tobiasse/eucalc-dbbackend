"""
mixins
******

:Author: Tobias Seydewitz
:Date: 24.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from proxilly.utils.errors import EmptyFileError


class FromFile:
    """Simple mixin for reading from files."""
    @staticmethod
    def read_file(path):
        """Reads a file and returns the content as list.

        Line-wise list of file content, cleaned from leading and trailing whitespace and empty lines.

        Args:
            path (str): Path to file.

        Returns:
            list(str)

        Raises:
            EmptyFileError: Raise if file is empty.
        """
        with open(path) as src:
            content = [line.strip() for line in src.readlines()]

        if not content:
            raise EmptyFileError('Empty file %s.' % path)

        return [line for line in content if line != '']
