"""
levers
******

:Author: Tobias Seydewitz
:Date: 16.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from random import randint

from proxilly.utils.mixins import FromFile


class _LeversScanner:
    def __init__(self, levers):
        self.levers = levers

        self._idx = -1
        self._length = len(self.levers) - 1

    def get(self):
        if self._idx < self._length:
            self._idx += 1
            return self.levers[self._idx]

        return 'EOL'


class LeversParser:
    def __init__(self):
        self._next = None
        self._scanner = None
        self._current = None

    def parse(self, levers):
        self._next = None
        self._scanner = _LeversScanner(levers)
        self._current = self._scanner.get()

        result = []

        while True:
            _type, value = self._get_token()

            if _type == 'INT':
                result.append(int(value))

            elif _type == 'FLOAT':
                result.append(float(value))

            else:
                break

        return result

    def _get_token(self):
        if self._current != 'EOL':
            self._next = self._scanner.get()

            if self._next.isdigit() or self._next == 'EOL':
                token = 'INT', self._current

            elif self._next == '.':
                token = 'FLOAT', self._current + self._next + self._scanner.get()
                self._next = self._scanner.get()

            else:
                token = 'EOL', 'EOL'

            self._current = self._next
            return token

        return 'EOL', 'EOL'


class LeversGenerator(FromFile):
    def __init__(self, path):
        self.levers = self.__class__.read_file(path)
        self._generator = self._from_file_generator
        self._parser = LeversParser()

    def _from_file_generator(self):
        for lever in self.levers:
            yield self._parser.parse(lever)

    def get(self):
        idx = randint(0, len(self.levers)-1)
        return self._parser.parse(self.levers[idx])

    def __iter__(self):
        yield from self._generator()

    def __len__(self):
        return len(self.levers)


class RandomizedLeversGenerator(LeversGenerator):
    def __init__(self, **kwargs):
        if kwargs.get('path'):
            super().__init__(kwargs.get('path'))
            self.size = kwargs.get('size')
            self.interval = 0, len(self.levers) - 1

        elif kwargs.get('length'):
            self.length = kwargs.get('length')
            self.interval = kwargs.get('interval')
            self.size = kwargs.get('size')
            self._generator = self._from_config_generator

    def _from_file_generator(self):
        for _ in range(self.size):
            choice = self.levers[randint(*self.interval)]
            yield self._parser.parse(choice)

    def _from_config_generator(self):
        for _ in range(self.size):
            yield [randint(*self.interval) for _ in range(self.length)]

    def __len__(self):
        return self.size

    @classmethod
    def from_file(cls, path, size):
        return cls(path=path, size=size)

    @classmethod
    def from_config(cls, length, interval, size):
        return cls(length=length, interval=interval, size=size)
