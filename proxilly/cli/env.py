"""
env
***

:Author: Tobias Seydewitz
:Date: 29.11.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: Module contains the parent of the proxilly command group.
"""
import logging
from os.path import join

import click

from proxilly.app import create_app
from proxilly.cli.migrate import migrate
from proxilly.cli.populate import populate
from proxilly.cli.caching import caching


@click.group()
@click.option('-v', '--verbose', count=True, help='Sets the verbosity of the logging.')
@click.option('--development', is_flag=True,
              help='If set the sub-commands will use the development database otherwise the production database.')
@click.option('--my2050', is_flag=True,
              help='If set the sub-commands will use the my250 production database.')
@click.option('--log_to_console', is_flag=True,
              help='If set the commands will log to sys.stderr otherwise a log file is created.')
@click.pass_context
def env(ctx, verbose: int, development: bool, my2050: bool, log_to_console: bool) -> None:
    """Initializes the context of the sub-commands (database to use, logging level, and verbosity)."""
    app = create_app(development, my2050)

    formatter = logging.Formatter('%(asctime)s/%(levelname)s/%(name)s/%(funcName)s: %(message)s')

    if log_to_console:
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
    else:
        handler = logging.FileHandler(join(app.instance_path, 'log', (ctx.invoked_subcommand + '.log')), mode='a')
        handler.setFormatter(formatter)

    logger = logging.getLogger(ctx.invoked_subcommand)
    logger.setLevel(60 - ((verbose*10) % 60))
    logger.addHandler(handler)

    # Deferred import to ensure the database is initialized
    from proxilly.models import Pathway
    from proxilly.models import Output
    from proxilly.models import OutputID
    from proxilly.models import LeverHeadline
    from proxilly.models import LeverGroup
    from proxilly.models import Lever

    if not ctx.ensure_object(dict):  # Hack CliRunner can be used to call with prepared ctx for testing
        ctx.obj = {
            'app': app,
            'models': [Pathway, Output, OutputID, LeverHeadline, LeverGroup, Lever],
            'pathway': Pathway,
            'output': Output,
            'outputid': OutputID,
            'leverheadline': LeverHeadline,
            'levergroup': LeverGroup,
            'lever': Lever,
        }


env.add_command(migrate)
env.add_command(populate)
env.add_command(caching)


if __name__ == '__main__':
    env()
