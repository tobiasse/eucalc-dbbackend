"""
migrate
*******

:Author: Tobias Seydewitz
:Date: 13.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: Module contains functions to migrate the applications database structure.
"""
import re
from csv import DictReader
from logging import getLogger
from os.path import join
from typing import Tuple

from click import Context
from click import command
from click import pass_context

from proxilly.models import Lever
from proxilly.models import LeverGroup
from proxilly.models import LeverHeadline
from proxilly.models import OutputID

LOGGER = getLogger('migrate')


@command()
@pass_context
def migrate(ctx: Context) -> None:
    """Migrates the database structure and pre-populates the tables with required entries.

    This CLI tool creates the required tables and adds required database entries. Per default the database entries are
    parsed from the files in the directory ``instance/migrate/``.
    """
    app = ctx.obj['app']
    db = app.config['DATABASE']

    with db.connection_context():
        LOGGER.info('MIGRATING DATABASE STRUCTURE')
        db.create_tables(ctx.obj['models'], safe=True)

        LOGGER.info('ADDING OUTPUTS TO DATABASE')
        with open(join(app.instance_path, 'migrate', 'pathway_explorer.csv')) as csv_file:
            populate_outputid(ctx.obj['outputid'], DictReader(csv_file))

        LOGGER.info('ADDING LEVER DETAILS TO DATABASE')
        with open(join(app.instance_path, 'migrate', 'levers.csv')) as csv_file:
            populate_lever_details(
                ctx.obj['leverheadline'],
                ctx.obj['levergroup'],
                ctx.obj['lever'],
                DictReader(csv_file)
            )


def populate_outputid(model: OutputID, content: DictReader) -> None:
    """Adds the output names supported by the ModelAPI to ``OutputID`` database table.

    Args:
        model (``proxilly.models.OutputID``): Table that should be populated.
        content (``csv.DictReader``): Content used for populating the table.
    """
    for row in content:
        name: str = row['name']
        rename: str = row['rename']
        frontend: int = 0

        if re.match(r'true', row['frontend'], re.IGNORECASE):
            frontend = 1

        if rename:
            model.create(name=name, rename=rename, frontend=frontend)
            LOGGER.info(
                'CREATED ENTRY OutputID.name="%s", OutputID.rename="%s", OutputID.frontend="%s"', name, rename, frontend
            )
        else:
            model.create(name=name, frontend=frontend)
            LOGGER.info('CREATED ENTRY OutputID.name="%s", OutputID.frontend="%s"', name, frontend)


def populate_lever_details(headline: LeverHeadline, group: LeverGroup, lever: Lever, content: DictReader) -> None:
    """Populates the lever details tables.

    Args:
        headline (``proxilly.models.LeverHeadline``): Table for storing lever headlines.
        group (``proxilly.models.LeverGroup``): Table for storing lever groups.
        lever (``proxilly.models.Lever``): Table for storing lever details.
        content (``csv.DictReader``): Content used for populating the tables.
    """
    for row in content:
        headline_ = headline.get_or_none(name=row['headline'])
        if not headline_:
            headline_ = headline.create(name=row['headline'])
            LOGGER.info('CREATED ENTRY LeverHeadline.name="%s"', headline_.name)

        group_ = group.get_or_none(name=row['group'])
        if not group_:
            group_ = group.create(headline=headline_, name=row['group'])
            LOGGER.info('CREATED ENTRY LeverGroup.headline="%s", LeverGroup.name="%s"', group_.headline.name,
                        group_.name)

        if re.match(r'true', row['check'], re.IGNORECASE):
            lever_ = lever.get_or_none(name=row['code'])
            if not lever_:
                min, max, increment = parse_display_range(row['display_range'])
                lever_ = Lever.create(
                    group=group_,
                    position=row['#'],
                    name=row['code'],
                    min=min,
                    max=max,
                    increment=increment,
                    title=row['title']
                )
                LOGGER.info('CREATED ENTRY Lever.group="%s", Lever.name="%s", min="%s", max="%s", increment="%s"',
                            lever_.group.name, lever_.name, lever_.min, lever_.max, lever_.increment)


def parse_display_range(text: str) -> Tuple[int, int, float]:
    """Normalizes a display_range columns entry.

    Args:
        text (``str``): A column entry from display_range

    Returns:
        ``int``, ``int``, ``float``: Normalized range column entry.
    """
    normalized = text.strip('[]').split(',')

    try:  # [1,2,3,4]
        normalized = list(map(int, normalized))
        return normalized[0], normalized[-1], 0.1
    except ValueError:  # ["A", "B"], ["A", "B", "C"]
        return 1, len(normalized), 1
