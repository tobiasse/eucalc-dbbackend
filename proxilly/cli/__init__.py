"""
Module: __init__.py
****

:Author: Tobias Seydewitz
:Date: 13.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""