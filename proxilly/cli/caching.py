"""
caching
*******

:Author: Tobias Seydewitz
:Date: 16.01.20
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from copy import copy
from itertools import chain
from itertools import product
from logging import getLogger
from typing import List
from typing import Tuple

import click

from proxilly.utils.levers import LeversGenerator

LOGGER = getLogger('caching')


@click.command()
@click.option('-s', '--scenario', 'scenario', required=True,
              type=click.Choice(['a', 'ba', 'bb', 'ca', 'cb', 'd', 'my2050']), help='')
@click.option('-l', '--levers', 'levers', required=True, type=str, help='Path to levers file.')
@click.option('-o', '--output', 'output', required=True, type=str, help='Target file name.')
@click.pass_context
def caching(ctx: click.Context, scenario: str, levers: str, output: str) -> None:
    db = ctx.obj['app'].config['DATABASE']
    group_model = ctx.obj['levergroup']
    lever_model = ctx.obj['lever']

    with db.connection_context():
        if scenario == 'a':
            pathways = LeversGenerator(levers)
            pathway_length = len(lever_model)
            group_intervals = group_model.get_group_intervals(exclude='Biodiversity')

            results = a(pathway_length, group_intervals, pathways)

        elif scenario == 'ba':
            groups = group_model.select().where(group_model.name != 'Biodiversity').count()
            group_intervals = group_model.get_group_intervals(exclude='Biodiversity')
            positions = [
                lever.position
                for lever in lever_model.select().join(group_model).where(group_model.name == 'Biodiversity')
            ]

            results = ba(groups, group_intervals, positions)

        elif scenario == 'bb':
            pathways = LeversGenerator(levers)
            meta_lever = [(lever.position, lever.min, lever.max) for lever in lever_model.select()]

            results = bb(meta_lever, pathways)

        elif scenario == 'my2050':
            group_intervals = group_model.get_group_intervals(
                exclude=['Biodiversity', 'Demographics & long-term', 'Domestic supply', 'Constraints']
            )
            group_intervals = sorted(group_intervals)
            group_intervals = map(lambda x: x[1] - x[0], group_intervals)
            group_intervals = list(group_intervals)
            group_intervals[-1] += 1

            results = my2050(group_intervals, len(group_intervals), '2.1212.3221', [1, 2, 3, 4])

    with open(output, 'w') as dst:
        for pathway in results:
            pathway = ''.join(list(map(str, pathway)))
            dst.write(pathway + '\n')


def a(pathway_length: int, group_intervals: List[Tuple[int, int]], pathways: LeversGenerator) -> List[List[int]]:
    """Caching scenario A: For an example pathway change each lever group to all possible states within the
    interval [1, 4].

    111111 > 221111 > 331111 > 441111 > 112221 > 113331 > ...

    Args:
        pathway_length:
        group_intervals:
        pathways:

    Returns:

    """
    result = []

    for pathway in pathways:
        if pathway_length != len(pathway):
            LOGGER.info('SKIP: Pathway length (%s) != DB Pathway length (%s).', len(pathway), pathway_length)
            continue

        for interval in group_intervals:
            lower, upper = interval
            elements = upper - lower
            left, right = pathway[:lower], pathway[upper:]

            for state in range(1, 5):
                new_pathway = left + [state]*elements + right
                if new_pathway != pathway:
                    result.append(new_pathway)

    return result


def ba(groups: int, group_intervals: List[Tuple[int, int]], positions: List[int]) -> List[List[int]]:
    """Caching scenario Ba:

    Args:
        groups:
        group_intervals:
        positions:

    Returns:

    """
    results = []
    elements = [
        upper - lower
        for lower, upper in sorted(group_intervals, key=lambda interval: interval[0])
    ]

    for states in product([1, 4], repeat=groups):
        new_pathway = list(chain(*[[state]*ele for state, ele in zip(states, elements)]))
        [new_pathway.insert(pos, 1) for pos in positions]
        results.append(new_pathway)

    return results


def bb(meta_lever: List[Tuple[int, int, int]], pathways: LeversGenerator) -> List[List[int]]:
    results = []
    meta_lever = sorted(meta_lever, key=lambda meta: meta[0])

    for pathway in pathways:
        for lever, meta in zip(pathway, meta_lever):
            pos, lower, upper = meta

            for state in range(lower, upper+1):
                if lever != state:
                    pathway[pos] = state
                    results.append(copy(pathway))

            pathway[pos] = lever

    return results


def my2050(group_lengths, groups, ref, combi):
    results = []
    for p in product(combi, repeat=groups):
        r = map(lambda x: str(x[0])*x[1], zip(p, group_lengths))
        r = ''.join(r)
        if int(r[-1]) == 1:
            r += '1'
        else:
            r += '2'
        r += ref

        results.append(r)

    return results
