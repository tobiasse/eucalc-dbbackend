"""
populate
********

:Author: Tobias Seydewitz
:Date: 13.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from logging import getLogger

import click
from peewee import IntegrityError
from requests import post

from proxilly.parsers import ModelOutputsResponse
from proxilly.utils.levers import LeversGenerator
from proxilly.utils.outputs import OutputsGenerator
from proxilly.utils.payload import PayloadGenerator

LOGGER = getLogger('populate')


@click.command()
@click.option('-l', '--levers', 'levers', type=str, help='Path to levers file.')
@click.option('-o', '--outputs', 'outputs', type=str, help='Path to outputs file.')
@click.option('-u', '--url', 'url', type=str, required=False, help='Set host for request (defaults to Model-API).')
@click.option('--all_countries', is_flag=True, help='Get all countries for each requested output.')
@click.pass_context
def populate(ctx: click.Context, levers: str, outputs: str, url: str, all_countries: bool) -> None:
    """Populates database with data.

    Args:
        ctx (``click.Context``): Context from prepared by ``proxilly.cli.env``.
        levers (``str``): Path to file with levers configurations (eg 111111111111).
        outputs (``str``): Path to file with outputs to request.
        url (``str``): Request send to this host (defaults to model API).
        all_countries (``bool``): Get all countries for each requested output.
    """
    db = ctx.obj['app'].config['DATABASE']
    pathway_model = ctx.obj['pathway']
    output_model = ctx.obj['output']

    if not url:
        url = ctx.obj['app'].config['MODEL_URL']

    leversgen = LeversGenerator(levers)
    outputsgen = OutputsGenerator(outputs, len(leversgen), all_countries=all_countries)

    req_payloads = PayloadGenerator(leversgen, outputsgen)

    with db.connection_context():
        for req_payload in req_payloads:
            response = post(url, json=req_payload.as_dict())

            if response.status_code == 200:
                res_payload = ModelOutputsResponse.from_dict(response.json())
                pathway = pathway_model.save_request(request=req_payload, precached=True)
                missing_outputs = set(req_payload.output_ids) ^ set(res_payload.output_ids)

                try:
                    output_model.create_response(pathway=pathway, response=res_payload)
                    msg = 'CREATED {}, {:03d}res/ {:03d}req, missing {}'.format(req_payload.uuid, len(res_payload),
                                                                                len(req_payload), missing_outputs)

                except IntegrityError:
                    output_model.update_response(pathway=pathway, response=res_payload)
                    msg = 'UPDATED {}, {:03d}res/ {:03d}req, missing {}'.format(req_payload.uuid, len(res_payload),
                                                                                len(req_payload), missing_outputs)

            else:
                msg = 'ERROR {} with status {}'.format(req_payload.uuid, response.status_code)

            LOGGER.info(msg)


if __name__ == '__main__':
    populate()
