"""
models
******

:Author: Tobias Seydewitz
:Date: 10.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: Module contains database abstraction classes. The three classes Pathway, Output, and OutputID are present
    in the database of the application.
"""
from math import inf
from time import time
from typing import Dict
from typing import List
from typing import Tuple
from typing import Union

from peewee import BlobField
from peewee import DecimalField
from peewee import FixedCharField
from peewee import ForeignKeyField
from peewee import IntegerField
from peewee import IntegrityError
from peewee import Model
from peewee import TextField

from proxilly.app import DB
from proxilly.parsers import ModelOutputsRequest
from proxilly.parsers import ModelOutputsResponse


class MediumBlobField(BlobField):
    """Represents a MySQL ``MEDIUMBLOBFIELD`` column datatype (max storage capacity 16MB)."""
    field_type = 'MEDIUMBLOB'


class MediumTextField(TextField):
    """Represents a MySQL ``MEDIUMTEXTFIELD`` column datatype (max storage capacity 16MB)."""
    field_type = 'MEDIUMTEXT'


class TinyIntegerField(IntegerField):
    """Represents a MySQL ``TINYINT`` column datatype (1 byte = 0-255)."""
    field_type = 'TINYINT'


class BaseModel(Model):
    class Meta:
        database = DB


class Pathway(BaseModel):
    """This class represents a pathway configuration for a certain model output.

    Attributes:
        uuid (``str``): MD5 hash of ``eu_lever`` + ``cntry_lever`` + ``EU`` + ``CNTRY``.
        levers (``str``): Lever configuration for the entire EU.
        cntry_levers (``str``): Lever configuration for a certain country.
        cntry (``str``): To cntry_levers corresponding country code.
        time (``int``): Creation date/time as Unix timestamp in UTC +00.
        precached (``int``): One if entry is pre-cached, zero if entry is dynamically cached.
    """
    uuid = FixedCharField(
        null=False,
        unique=True,
        max_length=32,
        help_text='MD5 hash of ``eu_lever`` + ``cntry_lever`` + ``EU`` + ``CNTRY``.'
    )
    levers = FixedCharField(
        null=True,
        max_length=180,
        help_text='Lever configuration for the entire EU.'
    )
    cntry_levers = FixedCharField(
        null=True,
        max_length=180,
        help_text='Lever configuration for a certain country.'
    )
    cntry = FixedCharField(
        null=True,
        max_length=2,
        help_text='Country relates to ``cntry_lever`` configuration.'
    )
    time = IntegerField(
        null=False,
        default=int(time()),
        help_text='Unix timestamp in UTC +00.'
    )
    precached = TinyIntegerField(
        null=False,
        help_text='One if entry is pre-cached, zero if entry is dynamically cached.'
    )

    # TODO ADD this as get_from_blob(query: Output, pre-cached: bool)
    # TODO ADD if pathway is pre-cached: COMPRESSALGO.decompress(query.ouputs).decode()
    #  ADD if not pre-cached: query.outputs.decode()
    @classmethod
    def get_outputs(cls, request: ModelOutputsRequest) -> Union[ModelOutputsResponse, None]:
        """Get cached model result outputs for a request from the TPE.

        Args:
            request (``proxilly.parsers.ModelOutputsRequest``): For this request a matching database entry will
                be searched.

        Returns:
            ``proxilly.parsers.ModelOutputsResponse``, ``None``: Returns object if a ``Pathway``-``Output``
            database entry pair exists for the request, otherwise ``None``.

        Raises
            ``peewee.IntegrityError``: This error will be raised if a ``Pathway`` entry exists but no ``Output``
            is assigned to it.
        """
        query = cls.get_or_none(cls.uuid == request.uuid)

        if query:
            query = query.pathway.first()

            try:
                return ModelOutputsResponse.from_json(query.outputs, checked=False)
            except AttributeError:
                raise IntegrityError('Pathway has no output assigned.')

        return query

    @classmethod
    def save_request(cls, request: ModelOutputsRequest, precached: bool = False) -> 'Pathway':
        """This method saves a request in the database.

        In case the request already exists in the database the corresponding database entry is returned.

        Args:
            request (``proxilly.parsers.ModelOutputsRequest``): The request that should be stored in the database.
            precached (``bool``): This flag marks pre-cached entries, generally it should be false.

        Returns:
            ``proxilly.models.Pathway``: The pathway table entry of the stored request.
        """
        query = cls.get_or_none(cls.uuid == request.uuid)

        if not query:
            if request.is_exceptional():
                query = cls.create(
                    uuid=request.uuid,
                    levers=request.levers,
                    cntry_levers=request.cntry_levers,
                    cntry=request.cntry,
                    precached=int(precached)
                )

            else:
                query = cls.create(
                    uuid=request.uuid,
                    levers=request.levers,
                    precached=int(precached)
                )

        return query


class Output(BaseModel):
    """This class represents the database table for model results. Each entry is linked with a pathway configuration.
    Selected field properties ensure a 1:1 relationship.

    Attributes:
        pathway (``int``): Foreign key from ``Pathway``. Links a model result with a pathway configuration.
        outputs (``str``): EUCALC model results stored as a JSON string.

    """
    pathway = ForeignKeyField(
        model=Pathway,
        backref='pathway',
        on_delete='CASCADE',
        null=False,
        unique=True,
        help_text='Representative pathway configuration for the model outputs.'
    )
    # TODO CHANGE MEDIUMBLOB
    outputs = MediumTextField(
        null=True,
        help_text='EUCalc model run outputs stored as a JSON string.'
    )

    # TODO ADD this a method get_as_blob(response: ModelOutputResponse, pre-cached: bool) -> binary
    # TODO ADD if pathway is pre-cached: COMPRESSALGO.compress(response.as_json().encode(), compresslevel=1)
    #  ADD if pathway is not pre-cached: response.as_json().encode()
    @classmethod
    def create_response(cls, pathway: Pathway, response: ModelOutputsResponse) -> 'Output':
        """Creates a new database entry for model results. Creation fails with a ``IntegrityError`` if
        an entry already exists.

        Args:
            pathway (``proxilly.models.Pathway``): The linked ``Pathway`` configuration.
            response (``proxilly.parser.ModelOutputsResponse``): The model results to store.

        Returns:
            ``proxilly.model.Output``: The response as a ``proxilly.models.Output`` instance.

        Raises:
            ``peewee.IntegrityError``: If a model results entry with the given ``Pathway`` configuration already exist.
        """
        query = cls.create(pathway=pathway)  # Raises IntegrityError if Output with this configuration already exists.
        query.outputs = response.as_json()
        query.save()
        return query

    @classmethod
    def update_response(cls, pathway: Pathway, response: ModelOutputsResponse) -> 'Output':
        """Updates a database entry for model results.

        Args:
            pathway (``proxilly.models.Pathway``): The pathway configuration of the model to update.
            response (``proxilly.parsers.ModelOutputsResponse``): The model results that should be applied as update.

        Returns:
            ``proxilly.model.Output``: The updated database entry.

        Raises:
            ``peewee.IntegrityError``: If no entry with the given ``Pathway`` configuration exists.
        """
        query = cls.get_or_none(pathway=pathway)

        if not query:
            raise IntegrityError('Can not update absent output for uuid "{}" id "{}".'.format(pathway.uuid, pathway.id))

        query.outputs = response.as_json()
        query.save()

        return query


class OutputID(BaseModel):
    """Stores all outputs that are supported by the EUCALC model.

    Attributes:
        name (``str``): Name of the output (refers to column in the Excel sheet).
        rename: If set it stores a replacement name of the corresponding output.
        frontend (``int``): One if supported by the frontend otherwise zero (value range is limited between
            0 <= x <= 1, for MySQL this check constraint does not work).
    """
    name = FixedCharField(
        max_length=80,
        null=False,
        unique=True,
        help_text='Pathway explorer output name.'
    )
    rename = FixedCharField(
        max_length=80,
        null=True,
        unique=True,
        help_text='Pathway explorer output name.'
    )
    frontend = TinyIntegerField(
        null=False,
        default=0,
        # constraints=[Check('frontend >= 0'), Check('frontend <= 1')],  # MySQL does not support check constraints
        help_text='One if output is supported by the frontend, otherwise zero.'
    )

    @classmethod
    def get_frontend_outputs(cls) -> List[str]:
        """Get all outputs that are supported by the frontend.

        Returns:
            ``list``: The outputs that are available in the frontend as a list of ``str``.
        """
        query = cls.select(cls.name).where(cls.frontend == 1)
        return [ele.name for ele in query]

    @classmethod
    def get_all_outputs(cls) -> List[str]:
        """Get all outputs in the database.

        Returns:
            ``list``: All outputs stored in the database.
        """
        query = cls.select(cls.name)
        return [ele.name for ele in query]

    @classmethod
    def get_renamed_outputs(cls) -> Dict[str, str]:
        """Get output id renaming dictionary.

        Returns:
            A renaming dictionary, key is current output id and value is the new output id.
        """
        query = cls.select(cls.name, cls.rename).where(cls.rename.is_null(False))
        return {ele.name: ele.rename for ele in query}


class LeverHeadline(BaseModel):
    """Model stores lever headlines.

    Attributes:
        name (``str``): Lever headline corresponds to the groups headline in the frontend (refers to
            headline col in Excel).
    """
    name = FixedCharField(
        max_length=25,
        null=False,
        unique=True,
        help_text='Lever headline corresponds to the groups headline in the frontend (refers to headline col in Excel).'
    )


class LeverGroup(BaseModel):
    """Model stores lever groups.

    Attributes:
        headline (``int``): Foreign key from ``LeverHeadline``, links groups with headlines.
        name (``str``): Lever group name corresponds to the group title in the TPE(refers to group col in the Excel
         sheet)
    """
    headline = ForeignKeyField(
        model=LeverHeadline,
        backref='headline',
        null=False,
        on_delete='CASCADE',
        help_text='Foreign key from ``LeverHeadline``, links groups with headlines.'
    )
    name = FixedCharField(
        max_length=25,
        null=False,
        unique=True,
        help_text='Lever group name corresponds to the group title in the TPE(refers to group col in the Excel sheet)'
    )

    @classmethod
    def get_group_intervals(cls, exclude: Union[List[str], str]) -> List[Tuple[int, int]]:
        """Returns the group intervals of the lever.

        Args:
            exclude (``str``, ``list``): Lever group or groups to exclude from group intervals.

        Returns:
            ``list``: A list of tuples with the upper and lower bound of the lever group.
        """
        if type(exclude) == str:
            exclude = [exclude]

        intervals = []

        for group in cls.select().where(cls.name.not_in(exclude)):
            lower, upper = inf, -inf

            for lever in group.group:
                if lever.position < lower:
                    lower = lever.position
                if lever.position > upper:
                    upper = lever.position

            intervals.append((lower, upper + 1))

        return intervals


class Lever(BaseModel):
    """Model stores the attributes of the unique levers.

    Attributes:
        group (``int``): Foreign key from ``LeverGroup``, links levers with groups.
        position (``int``): Marks the position of the lever config in the request (refers to the # col in the Excel
            sheet).
        name (``str``): Programmatically id of the lever (refers to the code col in the Excel sheet).
        min (``int``): Lower bound of the value range (refers to the display_range col in the Excel sheet).
        max (``int``): Upper bound of the value range (refers to the display_range col in the Excel sheet).
        increment (``float``): states=(max-min)/increment
        title (``str``): Displayable name of the lever (refers to the title col in the Excel sheet).
    """
    group = ForeignKeyField(
        model=LeverGroup,
        backref='group',
        null=False,
        on_delete='CASCADE',
        help_text='Foreign key from ``LeverGroup``, links levers with groups.'
    )
    position = TinyIntegerField(
        null=False,
        unique=True,
        help_text='Marks the position of the lever config in the request (refers to the # col in the Excel sheet).'
    )
    name = FixedCharField(
        max_length=40,
        null=False,
        unique=True,
        help_text='Programmatically id of the lever (refers to the code col in the Excel sheet).'
    )
    min = TinyIntegerField(
        null=False,
        help_text='Lower bound of the value range (refers to the display_range col in the Excel sheet)'
    )
    max = TinyIntegerField(
        null=False,
        help_text='Upper bound of the value range (refers to the display_range col in the Excel sheet)'
    )
    increment = DecimalField(
        max_digits=2,
        decimal_places=1,
        null=False,
        help_text='states=(max-min)/increment'
    )
    title = FixedCharField(
        max_length=40,
        null=False,
        help_text='Displayable name of the lever (refers to the title col in the Excel sheet).'
    )
