"""
keys
****

:Author: Tobias Seydewitz
:Date: 19.11.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: This module contains key abstractions to define field schemas.
"""
import re
from abc import ABCMeta
from abc import abstractmethod
from typing import Any
from typing import Tuple
from typing import Union

from proxilly.marshalling import marshall
from proxilly.marshalling.errors import ValidationError

__all__ = [
    'StringKey',
    'GenericKey'
]

Regex_Type = type(re.compile(''))


class _Key(metaclass=ABCMeta):
    """Base Key class from which other key classes should inherit.

    Key classes are used to fetch key value pairs from dictionaries of objects that inherit from ``Marshall``.
    These key classes are used internally by `proxilly.marshalling.fields._Field` classes.

    Args:
        name: The name/identifier of the key.
    """
    def __init__(self, name: Union[str, None]) -> None:
        self._name = name

    @property
    def name(self) -> str:
        """Get the name of the key."""
        return self._name

    @abstractmethod
    def get_from_dict(self, struct: dict) -> Tuple[str, Any]:
        """Abstract method child classes should implement this method.

        Method should fetch the value assigned to this key from the provided dictionary.
        If the key is not found in the dictionary raise a ``ValidationError``.

        Use ``StringKey`` or ``GenericKey`` as examples for the implementation.

        Args:
            struct: A dictionary where to fetch from the value assigned to this key.

        Returns:
            A key, value pair.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If the key is not found in the structure.
        """
        pass

    @abstractmethod
    def get_from_object(self, obj: 'marshall.Marshall') -> Tuple[str, Any]:
        """Abstract method child classes should implement this method.

        Method should fetch the value assigned to this key from a class that is derived from the ``Marshall`` class.
        If the the key is not found in the object raise a ``ValidationError``.

        Use ``StringKey`` or ``GenericKey`` as examples for the implementation.

        Args:
            obj: A instance of an object that is derived from ``Marshall``.

        Returns:
            A key, value pair.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If the key is not found in the object.
        """
        pass

    def __str__(self) -> str:
        return '{}(name=())'.format(self.__class__.__name__, self.name)

    def __repr__(self) -> str:
        return '<{}(name={}) at {}>'.format(self.__class__.__name__, self.name, hex(id(self)))


class StringKey(_Key):
    """This class allows to define string keys."""
    def __init__(self, name: str) -> None:
        if isinstance(name, str):
            super().__init__(name)
        else:
            msg = '{} name arg must be a string, but name is a {}.'.format(self.__class__.__name__, type(name).__name__)
            raise ValidationError(msg)

    def get_from_dict(self, struct: dict) -> Tuple[str, Any]:
        """Fetch key, value pair from a dictionary (please refer to the base class for a detailed method
        description)."""
        try:
            return self.name, struct[self.name]
        except KeyError:
            raise ValidationError('Missing key "%s".' % self.name)

    def get_from_object(self, obj: 'marshall.Marshall') -> Tuple[str, Any]:
        """Fetch key, value pair from an object (please refer to the base class for a detailed method
        description)."""
        try:
            return self.name, getattr(obj, self.name)
        except AttributeError:
            raise ValidationError('Attribute "%s" not found in "%s".' % (self.name, obj.__class__.__name__))


class GenericKey(_Key):
    """This class allows to define generic keys by using regular expressions.

    Please consider to use ``StringKey`` this class is just a workaround for a bad JSON request structure (e.g. {
    "foo": {["DE", "UK", "PL", ...]: List()}} the list key is variable and can be any country code to avoid introducing
    for each country code a separate not required field this class was written).

    Important, never attach generic keys to root field, fields that contain a generic key should never be flattened,
    and never attach more than one generic key to a field.

    Args:
        regex: A compiled regular expression.

    Raises:
        ``proxilly.marshalling.errors.ValidationError``:
    """
    def __init__(self, regex: Regex_Type) -> None:
        if type(regex) == Regex_Type:
            super().__init__(None)
        else:
            msg = '"{}" regex arg must be a "{}", but regex is a "{}".'.format(
                self.__class__.__name__, Regex_Type.__name__, type(regex).__name__
            )
            raise ValidationError(msg)

        self._regex = regex

    @property
    def name(self) -> str:
        """Get the key name.

        Returns:
            The key name.

        Raises:
            ``proxilly.marshalling.errors.VsalidationError``: GenericKey name must be initialized via ``get_from_dict``
                or ``get_from_object``.
        """
        if self._name:
            return self._name
        raise ValidationError('GenericKeys have only a name if you called get_from_dict or get_from_object before.')

    def get_from_dict(self, struct: dict) -> Tuple[str, Any]:
        """Fetch key, value pair from a dictionary.

        The key will be matched by using the defined regular expression.

        Args:
            struct: A dictionary where to fetch from the value assigned to this key.

        Returns:
            A key, value pair.

        Raises:
            ``proxilly.marshalling.errors.VsalidationError``: If no matching patter is found within the provided
                structure.
        """
        for key in struct.keys():
            if self._regex.match(key):
                self._name = key
                return self.name, struct[self.name]

        raise ValidationError('No key found for pattern "{}".'.format(self._regex.pattern))

    def get_from_object(self, obj: 'marshall.Marshall') -> Tuple[str, Any]:
        """Fetch key, value pair from an object.

        The key will be matched by using the defined regular expression.

        Args:
            obj: An object where to fetch from the value assigned to this key.

        Returns:
            A key, value pair.

        Raises:
            ``proxilly.marshalling.errors.VsalidationError``: If no matching patter is found within the provided
                object.
        """
        for attr in dir(obj):
            if not callable(getattr(obj, attr)) and self._regex.match(attr):
                self._name = attr
                return self.name, getattr(obj, self.name)

        raise ValidationError(
            'No attr found that matches "{}" in "{}".'.format(self._regex.pattern, obj.__class__.__name__)
        )
