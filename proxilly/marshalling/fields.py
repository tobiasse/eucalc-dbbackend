"""
fields
******

:Author: Tobias Seydewitz
:Date: 20.11.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: This module contains all field classes that are required to define schemas for ``Marshall`` classes.
"""
from numbers import Number
from typing import Any
from typing import Callable
from typing import Sequence
from typing import Tuple
from typing import Union

from proxilly.marshalling import marshall
from proxilly.marshalling.errors import ValidationError
from proxilly.marshalling.keys import GenericKey
from proxilly.marshalling.keys import Regex_Type
from proxilly.marshalling.keys import StringKey
from proxilly.marshalling.keys import _Key

__all__ = [
    'Str',
    'Int',
    'Float',
    'Num',
    'Bool',
    'Dict',
    'List'
]


class _Field:
    """Base field from which other field types should inherit.

    This class defines a common set of arguments that all field types support. Further, it provides the implementation
    of the public API methods parse, deserialize, and serialize. These methods can be called exclusively for root fields
    otherwise a ``ValidationError`` occurs (at the moment only Dict fields are root fields). In the provided example the
    outermost Dict field is the root field.

    The provided example shows how to define parsing schemas and the return values of the public API methods. Most
    convenient parsing is achieved by using field schemas together with ``Marshalling`` classes.

    IMPORTANT this is a private class. Please, use the derived field classes for the definition of field schemas.

    Examples:
        >>> s = Dict(
        ...     fields=[
        ...         Dict(
        ...             key='person',
        ...             flat=True,
        ...             fields=[
        ...                 List(key='names', field=Str(modifier=lambda name: name.capitalize())),
        ...                 Num(key='age'),
        ...                 Str(
        ...                     key='sex',
        ...                     required=False,
        ...                     store_missing=True,
        ...                     missing='na',
        ...                     validator=lambda sex: sex in ('male', 'female', 'divers', 'na'),
        ...                     modifier=lambda sex: sex.lower()
        ...                 )
        ...             ]
        ...         ),
        ...         Dict(
        ...             key='address',
        ...             fields=[
        ...                 Int(key='zip')
        ...             ]
        ...         ),
        ...     ]
        ... )
        >>> shiva = {'person': {'names': ['Shiva', 'Marie'], 'age': 31}, 'address': {'zip': 123456}}
        >>> bjoern = {'person': {'names': ['bjoern', 'BAUM'], 'age': 28, 'sex': 'MALE'}, 'address': {'zip': 123456}}
        >>> s.parse(shiva)
        {'person': {'names': ['Shiva', 'Marie'], 'age': 31, 'sex': 'na'}, 'address': {'zip': 123456}}
        >>> s.deserialize(s.parse(shiva))
        {'names': ['Shiva', 'Marie'], 'age': 31, 'sex': 'na', 'address': {'zip': 123456}}
        >>> s.parse(bjoern)
        {'person': {'names': ['Bjoern', 'Baum'], 'age': 28, 'sex': 'male'}, 'address': {'zip': 123456}}
        >>> s.deserialize(s.parse(bjoern))
        {'names': ['Bjoern', 'Baum'], 'age': 28, 'sex': 'male', 'address': {'zip': 123456}}

    Args:
        key: The key that is assigned to the field (set only if field is a Dict member).
        required: Triggers a ValidationError during parsing if field values is not supplied.
        nullable: If set to true None is accepted as field value.
        store_missing: If the field is not required and missing during parsing, it will be added.
        missing: The field value to add if field is missing.
        validator: A validator function or a sequence of validator functions. Validator functions must accept as
            argument the fields value and should return a boolean.
        modifier: A modifier function or a sequence of modifier functions. Modifier functions must accept as argument
            the fields value and should return a modified version of the field value.

    Attributes:
        key: A key object either StringKey or GenericKey.
        validator: A sequence of validator functions.
        modifier: A sequence of modifier functions.
        missing: The value used to substitute missing fields.
        required: Boolean indicated if field should be in structure.
        nullable: Boolean indicates if None is a acceptable field value.
        store_missing: Boolean indicates if field should be substituted when missing.

    Raises:
        ``proxilly.marshalling.errors.ValidationError``: If a argument does not fit the specifications.
    """
    # Override this in child classes with the type the child represents.
    TYPE = None

    def __init__(
            self,
            key: Union[_Key, str, Regex_Type, None] = None,
            required: bool = True,
            nullable: bool = False,
            store_missing: bool = False,
            missing: Any = None,
            validator: Union[
                Callable[[Any], bool],
                Sequence[Callable[[Any], bool]],
            ] = None,
            modifier: Union[
                Callable[[Any], Any],
                Sequence[Callable[[Any], Any]],
            ] = None
    ) -> None:

        if not (missing is None or isinstance(missing, self.__class__.TYPE)):
            raise ValidationError('Field "{}" parameter "missing" must be "None" or "{}".'.format(
                self.__class__.__name__,
                self.__class__.TYPE.__name__
            ))

        if required and missing is not None:
            raise ValidationError('Parameter "missing" must not be set for required fields.')

        if not required and not nullable and missing is None:
            raise ValidationError('Logic error, set "nullable" true or set "missing" to "{}".'.format(
                self.__class__.TYPE.__name__ if self.__class__.TYPE is not None else type(self.__class__.TYPE).__name__
            ))

        if callable(validator):
            self.validator = [validator]
        elif isinstance(validator, (list, tuple, set)) and all(callable(func) for func in validator):
            self.validator = validator
        elif validator is None:
            self.validator = list()
        else:
            raise ValidationError('Parameter "validator" must be a callable or a sequence of callable.')

        if callable(modifier):
            self.modifier = [modifier]
        elif isinstance(modifier, (list, tuple, set)) and all(callable(func) for func in modifier):
            self.modifier = modifier
        elif modifier is None:
            self.modifier = list()
        else:
            raise ValidationError('Parameter "modifier" must be a callable or a sequence of callable.')

        if isinstance(key, _Key) or key is None:
            self.key = key
        elif isinstance(key, str):
            self.key = StringKey(key)
        elif isinstance(key, Regex_Type):
            self.key = GenericKey(key)
        else:
            raise ValidationError('Key must be a instance of "%s" or "%s".' % (type(_Key), None))

        self.missing = missing
        self.required = required
        self.nullable = nullable
        self.store_missing = store_missing

        self._root = True
        self._flat = False
        self._errors = list()
        self._in_dict = isinstance(self.key, _Key)

    @property
    def in_dict(self) -> bool:
        """Returns whether the field is a member of a Dict field."""
        return self._in_dict

    @property
    def flat(self) -> bool:
        """Returns whether the field and its members should be flattened (can only set to true for Dict fields)."""
        return self._flat

    @property
    def root(self) -> bool:
        """Returns whether the field is a root field (root means top field). At the moment only Dict fields can
        be root."""
        return self._root

    def is_root(self) -> bool:
        """Determines if the field is a root field."""
        if self.root and not self.in_dict:
            return True
        return False

    def serialize(self, obj: 'marshall.Marshall') -> dict:
        """Serializes a object corresponding to the given schema definition.

        The assembled structure is not tested on type, value, and structural integrity. Please, use ``parse`` to perform
        a validation.

        Args:
            obj: A valid sub-class of ``proxilly.marshall.Marshalling``. Valid means that a schema is defined and the
                constructor arguments match the schema definition.

        Returns:
            ``dict``: A dictionary that corresponds to the provided field schema.

        Raises:
            ``proxilly.marshall.errors.ValidationError``: If field is not a root field.
        """
        if self.is_root():
            return self._serialize(obj)[1]
        else:
            raise ValidationError('Serialization should start from root field.')

    def deserialize(self, struct: dict) -> dict:
        """Deserializes a structure corresponding to the given schema.

        This method does not test the provided ``struct`` on structural integrity. Please, use ``parse`` before
        calling deserialize to validate the structure. Especially, if you can not trust the provided structure because
        it comes from remotes.

        Args:
            struct: The structure to deserialize.

        Returns:
            ``dict``: The deserialized structure that can be used a constructor arguments for a sub-class of
            ``Marshall``.

        Raises:
            ``proxilly.marshall.errors.ValidationError``: If field is neither a root field nor a dictionary.
        """
        if type(struct) != dict:
            raise ValidationError('Deserialization requires a dict, but arg is a "%s".' % type(struct))
        elif not self.is_root():
            raise ValidationError('Deserialization should start from root field.')
        else:
            return self._deserialize(struct)[1]

    def parse(self, struct: dict) -> dict:
        """Parses and validates structures corresponding to the given schema.

        This methods performs on the give structure a structural integrity validation. During this validation missing
        fields are set if wished, user defined modifications are applied, extra user defined validations are performed,
        and a type checking is applied. If structural miss-matches are detected the errors are collected and will be
        reported at the end of the parsing process.

        Args:
            struct: The structure to parse and validate.

        Returns:
            ``dict``: The validated structure and eventually applied user defined modifications.

        Raises:
            ``proxilly.marshall.errors.ValidationError``: If field is neither a root field nor a dictionary.
        """
        if type(struct) != dict:
            raise ValidationError('Parsing requires a dict, but arg is a "%s".' % type(struct))
        elif not self.is_root():
            raise ValidationError('Parsing should start from root field.')
        else:
            return self._parse_from_list(0, struct)[1]

    def _serialize(self, obj: 'marshall.Marshall') -> Tuple[Union[str, None], Any]:
        """Private API method, serialize fields from an ``object``.

        Args:
            obj: A sub-class of ``Marshall``.

        Returns:
            Key, value pair of the field or in case the field is not required and it was not found in ``obj`` a None,
            None pair.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If the field is required but not found in the ``obj``
                instance attributes.
        """
        try:
            return self.key.get_from_object(obj)

        except ValidationError as err:
            if self.required:
                raise err
            else:
                return None, None

    def _deserialize(self, struct: dict) -> Tuple[Union[str, None], Any]:
        """Private API method, deserializes fields from a dictionary.

        Args:
            struct: The dictionary to deserialize from.

        Returns:
            Key, value pair of the field or in case the field is not required and it was not found in the dictionary
            a None, None pair.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If the field is required but not found in the dictionary.
        """
        try:
            return self.key.get_from_dict(struct)

        except ValidationError as err:
            if self.required:
                raise err
            else:
                return None, None

    def _parse_from_list(self, idx: int, value: Any) -> Tuple[int, Any]:
        """Private API method, parses a field from a list.

        Args:
            idx: The index of the value.
            value: The field value.

        Returns:
            The index and value of the field.
        """
        return self._validate(idx, value)

    def _parse_from_dict(self, struct: dict) -> Tuple[Union[str, None], Any]:
        """Private API method, parses a field from a dictionary.

        Args:
            struct: The dictionary to parse from.

        Returns:
            The key, value pair of the field.
        """
        try:
            return self._validate(*self.key.get_from_dict(struct))

        except ValidationError as err:
            if self.required:
                raise err
            elif self.store_missing:
                return self._validate(self.key.name, self.missing)
            else:
                return None, None

    def _validate(self, idx_or_key: Union[int, str], value: Any) -> Tuple[Any, Any]:
        """Private API method, validates the field value.

        Validation includes: applying user-defined value modifications, checking if the the type of the value
            corresponds to the field type, and applying user-defined validations.

        Args:
            idx_or_key: The key or index of the field.
            value: The field value.

        Returns:
            The key/index, value pair of the field.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If field, value type differ or a user-defined validator
                returns false.
        """
        value = self._apply_modifier(value)

        if not self._validate_type(value):
            raise ValidationError(
                'At index/key "{}" type miss-match must be "{}" but is "{}".'.format(
                    idx_or_key,
                    self.__class__.TYPE.__name__,
                    type(value).__name__
                )
            )

        self._apply_validator(idx_or_key, value)

        self._raise_errors()

        return idx_or_key, value

    def _validate_type(self, value: Any) -> bool:
        """Private API method, checks if value type corresponds to field type.

        Args:
            value: The field value.
        """
        if type(value) == self.__class__.TYPE or (value is None and self.nullable):
            return True
        return False

    def _apply_validator(self, idx_or_key: Union[int, str], value: Any) -> None:
        """Private API method, executed user-defined validations.

        Args:
            idx_or_key: The field key or index.
            value: The field value.
        """
        for func in self.validator:
            if not func(value):  # TODO send idx_or_key to validation func
                self._errors.append(
                    'At index/key "{}" validation "{}({})" failed.'.format(idx_or_key, func.__name__, value)
                )

    # TODO maybe catch modifier errors
    def _apply_modifier(self, value: Any) -> Any:
        """Private API method, apply user defined modifications on the current field value.

        Args:
            value: The current field value.

        Returns:
            The modified field value.
        """
        for func in self.modifier:
            value = func(value)
        return value

    def _raise_errors(self) -> None:
        """Reports collected errors for the current field.

        Raises:
            ``proxilly.marshall.errors.ValidationError``: If errors are collected these will be aggregated and attached
                to the error message.
        """
        if self._errors:
            msg = '\n'.join(self._errors)
            self._errors = list()
            raise ValidationError(msg)

    def _set_root(self) -> None:
        """Set field as root field or the inverse."""
        self._root = not self._root

    def __str__(self) -> str:
        return '{}(k={}, r={}, n={}, sm={}, m={}, r={}, id={})'.format(
            self.__class__.__name__,
            str(self.key),
            self.required,
            self.nullable,
            self.store_missing,
            self.missing,
            self.root,
            self.in_dict
        )

    def __repr__(self) -> str:
        return '{}(k={}, r={}, n={}, sm={}, m={}, r={}, id={}) at {}'.format(
            self.__class__.__name__,
            str(self.key),
            self.required,
            self.nullable,
            self.store_missing,
            self.missing,
            self.root,
            self.in_dict,
            hex(id(self))
        )


class Str(_Field):
    """This class allows to define string fields that are members of dictionaries or lists.

    Supports all parameter detailed in ``proxilly.marshalling._Field`` base class.
    """
    TYPE = str


class Int(_Field):
    """This class allows to define integer fields that are members of dictionaries or lists.

    Supports all parameter detailed in ``proxilly.marshalling._Field`` base class.
    """
    TYPE = int


class Float(_Field):
    """This class allows to define float fields that are members of dictionaries or lists.

    Supports all parameter detailed in ``proxilly.marshalling._Field`` base class.
    """
    TYPE = float


class Num(_Field):
    """This class allows to define number fields that are members of dictionaries or lists.

    Supports all parameter detailed in ``proxilly.marshalling._Field`` base class.
    Values of number fields can have the type integer or float.
    """
    TYPE = Number

    def _validate_type(self, value: Any) -> bool:
        """Check if field value is a number (float or integer)."""
        if (type(value) == int or type(value) == float) or (value is None and self.nullable):
            return True
        return False


class Bool(_Field):
    """This class allows to define boolean fields that are members of dictionaries or lists.

    Supports all parameter detailed in ``proxilly.marshalling._Field`` base class.
    """
    TYPE = bool


# TODO GenericKey can never be attached to root fields and never to fields to be flattened (both constraints must be set
#  on Dict fields), ensure that not more than one generic key is defined per dict
class Dict(_Field):
    """This class allows to define dictionary fields and to assign other field types to it.

    The assigned fields must have key and the keys should not be duplicated. Dictionary fields support flattening where
    each assigned field is attached to the parent dictionary (see example). This will not work if a dictionary field
    is assigned to a ``proxilly.marshalling.List`` field. Outermost dictionary fields should not have a key defined
    otherwise the de- and serialization will fail. Dictionary fields support all parameters detailed in
    ``proxilly.marshalling._Field`` additional to the field type unique parameters.

    Important, all dictionary members that are not covered by a field definition will be ignored and dropped during
    de- or serialization.

    Examples:
        >>> s = Dict(
        ...     fields=[
        ...         Dict(key='person', flat=True, fields=Str(key='name')),
        ...         Dict(key='address', fields=[Str(key='street'), Int(key='number')])
        ...     ]
        ... )
        >>> s.deserialize({'person' : {'name': 'foo bar'}, 'address': {'street': 'foo', 'number': 3}, 'a': 'b'})
        {'name': 'foo bar', 'address': {'street': 'foo', 'number': 3}}

    Args:
        fields: A single or list of dictionary fields where each field must
            have a key.
        flat: If set to true the dictionary fields are flattened and attached to the parent dictionary
            (see example).
        kwargs: The same keyword arguments that ``proxilly.marshalling.fields._Field`` receives.

    Raises:
        ``proxilly.marshalling.errors.ValidationError``: If field or fields does not have a key or the fields are not
            a subclass of ``proxilly.marshalling.fields._Field``. Additional, a ``ValidationError`` is thrown if fields
            with duplicate keys are assigned to this class.
    """
    TYPE = dict

    def __init__(self, fields: Union[_Field, Sequence[_Field]] = None, flat: bool = False, **kwargs):
        super().__init__(**kwargs)

        if isinstance(fields, (list, tuple, set)) and all(isinstance(field, _Field) for field in fields):
            self.fields = fields
        elif isinstance(fields, _Field):
            self.fields = [fields]
        elif fields is None:
            self.fields = []
        else:
            raise ValidationError('Fields sequence must be a sequence of "%s".' % __class__.__name__)

        if not all(field.in_dict for field in self.fields):
            raise ValidationError('For "%s" fields a key is required.' % self.__class__.__name__)

        keys = [field.key.name for field in self.fields if type(field.key) == StringKey]
        if len(keys) != len(set(keys)):
            raise ValidationError('Dictionary contains duplicate keys.')

        self._flat = flat
        # Set on child fields root false, ensures that only for top dictionary root is true.
        [field._set_root() for field in self.fields]

    @property
    def flat(self) -> bool:
        """Returns if field should be flattened."""
        return self._flat

    def _serialize(self, obj: 'marshall.Marshall') -> Tuple[Union[str, None], Union[dict, None]]:
        """Private API method, serialize dictionary field.

        Serialization revers the dictionary back to its state defined by the field definition.
        Does not perform type validation, if validation is required call _validate before via
        its public API.

        Args:
            obj: An object that is a subclass of ``proxilly.marshalling.marshall.Marshall``.

        Returns:
            The serialized dictionary, the corresponding key, and the serialized assigned fields.
        """
        if self.root:
            dkey = 'root'
        elif self.flat:
            dkey = self.key.name
        else:  # Is not flat and can simply be fetched from obj
            return super()._serialize(obj)

        parsed = self.__class__.TYPE()

        for field in self.fields:
            fkey, fvalue = field._serialize(obj)

            if fkey:
                parsed[fkey] = fvalue

        return dkey, parsed

    def _deserialize(self, struct: dict) -> Tuple[Union[str, None], Union[dict, None]]:
        """Private API method, deserialize dictionary field.

        Flattens child fields or copies child fields to their corresponding keys.
        Does not perform type validation, if validation is required call _validate before via
        its public API.

        Args:
            struct: The dictionary that should be deserialized.

        Returns:
            The deserialized dictionary, the corresponding key, and the deserialized assigned fields.
        """
        if self.root:
            dkey, dvalue = 'root', struct
        else:
            dkey, dvalue = super()._deserialize(struct)

            if dkey is None:  # Is not required and not in struct
                return dkey, dvalue

        parsed = self.__class__.TYPE()

        for field in self.fields:
            fkey, fvalue = field._deserialize(dvalue)

            if fkey:
                if field.flat:
                    parsed.update(fvalue)
                else:
                    parsed[fkey] = fvalue

        return dkey, parsed

    def _validate(self, idx_or_key: Union[int, str], value: Any) -> Tuple[Any, Any]:
        """Private API method, validates structural integrity of the dictionary and its child members.

        Args:
            idx_or_key: The index or key that is assigned to the dictionary.
            value: The structure to evaluate.

        Returns:
            The validated structure, the corresponding key or index.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If a structural miss match is found like
                type miss match, missing fields etc.
        """
        idx_or_key, value = super()._validate(idx_or_key, value)
        parsed = self.__class__.TYPE()

        for field in self.fields:
            try:
                key, val = field._parse_from_dict(value)
                if key:
                    parsed[key] = val

            except ValidationError as err:
                self._errors.append(str(err))

        self._raise_errors()

        return idx_or_key, parsed

    def __str__(self) -> str:
        return '{}(k={}, r={}, n={}, sm={}, m={}, r={}, id={}, f={}, f={})'.format(
            self.__class__.__name__,
            str(self.key),
            self.required,
            self.nullable,
            self.store_missing,
            self.missing,
            self.root,
            self.in_dict,
            self.flat,
            [str(field) for field in self.fields]
        )


class List(_Field):
    """This class allows to define list fields and to assign another field to it.

    The assigned field should not have a key. List field support only one field type as its child and can never be flat
    (see example). Divergent from the JSON standard (RFC 8259) list elements can never be the outermost element. List
    fields support all parameters detailed in ``proxilly.marshalling.fields._Field`` additional to the field type
    unique parameter.

    Examples:
        >>> s = Dict(
        ...     fields=[
        ...         Dict(key='person', flat=True, fields=List(key='names', field=Str())),
        ...         Dict(key='address', fields=[Str(key='street'), Int(key='number')])
        ...     ]
        ... )
        >>> s.deserialize(
        ...     {'person' : {'names': ['foo', 'von', 'bar']}, 'address': {'street': 'foo', 'number': 3}, 'a': 'b'}
        ... )
        {'names': ['foo', 'von', 'bar'], 'address': {'street': 'foo', 'number': 3}}

    Args:
        field: A field definition that should not have a key.
        kwargs: The same keyword arguments that ``proxilly.marshalling.fields._Field`` receives.

    Raises:
        ``proxilly.marshalling.ValidationError``: If field has a key or the field is not
            a subclass of ``proxilly.marshalling.errors._Field``. Additional, a error occurs if the child field
            is a dictionary and it should be flattened.
    """
    TYPE = list

    def __init__(self, field: _Field, **kwargs) -> None:
        super().__init__(**kwargs)

        if not isinstance(field, _Field):
            raise ValidationError('Field must be an instance of "%s", but is "%s".' % (__class__.__name__, type(field)))

        if type(field) == Dict and field.flat:
            raise ValidationError('Dictionaries in lists can not be flattened.')

        if field.in_dict:
            raise ValidationError('A "%s" field should not have a key.' % self.__class__.__name__)

        self.field = field
        self.field._set_root()  # Set on child field root false.

    def _validate(self, idx_or_key: Union[int, str], value: Any) -> Tuple[Any, Any]:
        """Private API method, validates structural integrity of the list and its child members.

        Args:
            idx_or_key: The index or key that is assigned to the dictionary.
            value: The structure to evaluate.

        Returns:
            The validated structure, the corresponding key or index.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If a structural miss match is found like
                type miss match, missing fields etc.
        """
        idx_or_key, value = super()._validate(idx_or_key, value)
        parsed = self.__class__.TYPE([None]) * len(value)

        for idx, val in enumerate(value):
            try:
                idx, val = self.field._parse_from_list(idx, val)
                parsed[idx] = val

            except ValidationError as err:
                self._errors.append(str(err))

        self._raise_errors()

        return idx_or_key, parsed

    def __str__(self) -> str:
        return '{}(k={}, r={}, n={}, sm={}, m={}, r={}, id={}, f={})'.format(
            self.__class__.__name__,
            str(self.key),
            self.required,
            self.nullable,
            self.store_missing,
            self.missing,
            self.root,
            self.in_dict,
            self.field,
        )
