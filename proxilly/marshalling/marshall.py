"""
marshall
********

:Author: Tobias Seydewitz
:Date: 20.11.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: Classes that should be de- and serializable must inherit from the ``Marshall`` class defined in this
    module.
"""
from json import JSONDecodeError
from json import dumps
from json import loads

from proxilly.marshalling import fields
from proxilly.marshalling.errors import ValidationError


class Marshall:
    """Class to support de- and serialization of JSONs or Python dictionaries

    A class that should support de- and serialization of JSON strings or Python dictionaries should inherit from
    ``Marshall``. This class must override the ``SCHEMA`` class attribute and assign a field or field structure from
    ``proxilly.marshalling.fields``. Further, a marshallable class must define a ``__init__`` method that accepts the
    same number of parameters as the structure to marshall. Additional, the parameter names must correspond to the
    key names of the defined schema (see given example).

    Examples:
        >>> class Person(Marshall):
        ...     SCHEMA = fields.Dict(
        ...                 fields=[
        ...                     fields.Str(key='name'),
        ...                     fields.Int(key='age'),
        ...                     fields.Str(key='sex', required=False, store_missing=True, missing='')]
        ...              )
        ...     def __init__(self, name, age, sex):
        ...         self.name = name
        ...         self.age = age
        ...         self.sex = sex

        >>> obj = Person.from_dict({'name': 'foo', 'age': 15})
        >>> obj.as_dict()
        {'name': 'foo', 'age': 15, 'sex': ''}

    Attributes:
        SCHEMA (``proxilly.marshalling.fields``): A field schema.
    """
    # Override this class attribute in child classes.
    SCHEMA: 'fields._Field' = None

    def as_json(self, checked: bool = False) -> str:
        """Get object as JSON string. The returned JSON string structure corresponds to the defined ``SCHEMA``.

        Args:
            checked: If set to true the structure is validated with the defined schema. To increase
                serialization performance ``checked`` defaults to false (it can be assumed that the object conforms to
                the schema).

        Returns:
            ``str``: From object serialized structure according to ``SCHEMA`` as a JSON string.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If from class serialized structure does not conform
                to defined ``SCHEMA``.
        """
        struct = self.__class__.SCHEMA.serialize(self)

        if checked:
            struct = self.__class__.SCHEMA.parse(struct)

        return dumps(struct)

    def as_dict(self, checked: bool = False) -> dict:
        """Get object as Python dictionary. The returned dictionary corresponds to the defined ``SCHEMA``.

        Args:
            checked: If set to true the structure is validated with the defined schema. To increase
                serialization performance ``checked`` defaults to false (it can be assumed that the object conforms to
                the schema).

        Returns:
            ``str``: From object serialized structure according to ``SCHEMA`` as a dictionary.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If from class serialized structure does not conform
                to defined ``SCHEMA``.
        """
        struct = self.__class__.SCHEMA.serialize(self)

        if checked:
            struct = self.__class__.SCHEMA.parse(struct)

        return struct

    @classmethod
    def from_json(cls, content: str, checked: bool = True) -> 'Marshall':
        """Deserializes from a JSON string.

        Args:
            content: A JSON string to deserialize.
            checked: If set to true the structure is validated with the defined schema.

        Returns:
            ``obj``: A Python class with attributes set to the values of the deserialized JSON fields.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If from JSON deserialized structure does not conform
                to defined ``SCHEMA``.
        """
        try:
            struct = loads(content)
        except JSONDecodeError as err:
            raise ValidationError(str(err))

        if checked:
            struct = cls.SCHEMA.parse(struct)

        return cls(**cls.SCHEMA.deserialize(struct))

    @classmethod
    def from_dict(cls, struct: dict, checked: bool = True) -> 'Marshall':
        """Deserializes from a Python dictionary.

        Args:
            struct: A JSON string to deserialize.
            checked: If set to true the structure is validated with the defined schema.

        Returns:
            ``obj``: A Python class with attributes set to the values of the deserialized dictionary fields.

        Raises:
            ``proxilly.marshalling.errors.ValidationError``: If from dictionary deserialized structure does not conform
                to defined ``SCHEMA``.
        """
        if checked:
            struct = cls.SCHEMA.parse(struct)

        return cls(**cls.SCHEMA.deserialize(struct))
