"""
errors
******

:Author: Tobias Seydewitz
:Date: 29.12.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: This module contains all errors that can occur by executing classes within this package.
"""


class ValidationError(Exception):
    """Raise if error occurs in schema validation."""
