"""
parsers
*******

:Author: Tobias Seydewitz
:Date: 18.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: This module contains all classes that are required to parse requests made to the endpoint and to parse
    responses made by the endpoint.
"""
import re
import typing as t
from copy import copy
from hashlib import md5
from numbers import Number

from numpy import array
from peewee import DatabaseError
from peewee import OperationalError

from proxilly.marshalling.fields import Bool
from proxilly.marshalling.fields import Dict
from proxilly.marshalling.fields import Int
from proxilly.marshalling.fields import List
from proxilly.marshalling.fields import Num
from proxilly.marshalling.fields import Str
from proxilly.marshalling.marshall import Marshall


def levers_length(levers: t.List[Number]) -> bool:
    """Tests if the provided levers list length corresponds to the number levers defined
    in the database.

    Args:
        levers: The levers configuration selected in the TPE as a list of numbers.

    Returns:
        ``bool``: True if validation successful otherwise false.
    """
    from proxilly.models import Lever  # Avoid circular import (models imports parsers, parsers imports models)
    try:
        return len(levers) == len(Lever)
    except (DatabaseError, OperationalError, AttributeError):
        return True


def normalize(values: t.List[t.Union[int, float]]) -> t.List[t.Union[int, float]]:
    """Normalize numbers without decimals to integers."""
    return [int(v) if (v * 10) % 10 == 0 else v for v in values]


class ModelOutputsRequest(Marshall):
    """Parser for requests made to the application endpoint.

    Class should be handled as immutable (dont set attributes from the outside, only use provided methods for
    interactions).

    Args:
        default: Lever configuration of the EU.
        exceptions: Lever configuration for other countries.
        outputs: The requested outputs.
        getFromModel: If true the request can be forwarded to the model API.
    """
    SCHEMA = Dict(
        fields=[
            Dict(
                key='levers',
                flat=True,
                fields=[
                    List(key='default', field=Num(), validator=levers_length),  # TODO modifier=normalize
                    Dict(
                        key='exceptions',
                        fields=List(key=re.compile(r'[A-Z]{2}'), field=Num(), required=False,
                                    nullable=True, validator=levers_length))  # TODO modifier=normalize
                ]
            ),
            List(
                key='outputs',
                field=Dict(
                    fields=[
                        Str(key='id'),
                        Bool(key='allCountries', required=False, store_missing=True, missing=False)
                    ]
                )
            ),
            Bool(key='getFromModel', required=False, store_missing=True, missing=False),
        ]
    )

    def __init__(
            self,
            default: t.List[Number],
            exceptions: t.Dict[str, t.List[Number]],
            outputs: t.List[dict],
            getFromModel: bool
    ) -> None:

        self.default = default
        self.exceptions = exceptions
        self.outputs = outputs
        self.getFromModel = getFromModel

        # Derived attributes
        self.default_cntry = 'EU'
        self.levers = ''.join(map(str, self.default))
        self.output_ids = [output['id'] for output in self.outputs]

        if self.exceptions:
            self.cntry = list(self.exceptions.keys())[0]
            self.cntry_levers = ''.join(map(str, list(self.exceptions.values())[0]))
        else:
            self.cntry = ''
            self.cntry_levers = self.levers

    @property
    def countries(self) -> t.List[str]:
        """Get requested countries.

        Returns:
            ``list``: Returns a list of str where each element are the requested countries.
        """
        return [self.default_cntry, self.cntry] if self.cntry else [self.default_cntry]

    @property
    def country(self) -> str:
        """Get requested country - special getter for /localised endpoint

        Returns:
            Either 'EU' if the request is not exceptional or one of the EU member states.
        """
        return self.cntry if self.cntry else self.default_cntry

    @property
    def uuid(self) -> str:
        """Get the unique identifier of the request.

        This method creates by means of a MD5 hashing algorithm the unique identifier of a request.
        The hashing arguments are ``self.levers + self.cntry_levers + self.default_cntry + self.cntry``.

        Returns:
            ``str``: A 32 character MD5 hash of the object as a hex code.
        """
        if self.is_exceptional():
            hashable = self.levers + self.cntry_levers + self.default_cntry + self.cntry
        else:
            hashable = self.levers + self.default_cntry

        return md5(hashable.encode()).hexdigest()  # TODO encoding type defaults atm to UTF8

    def is_exceptional(self) -> bool:
        """Test if exceptions pathway configuration differs from default pathway configuration.

        Returns:
            ``bool``: True if eu pathway configuration correspond to country config (or country not set),
            otherwise false.
        """
        return self.levers != self.cntry_levers

    def get_from_model(self):
        return self.getFromModel

    def all_countries(self, item: str) -> bool:
        """Get if ``allCountries`` is ``True`` for output id.

        Args:
            item: An output id.

        Returns:
            ``bool``: True if ``allCountries`` is set to true for output id, otherwise false.
        """
        return self.outputs[self.output_ids.index(item)]['allCountries']

    def filter(self, outputs: t.List[str]) -> 'ModelOutputsRequest':
        """Removes unwanted outputs from request and returns a new request.

        Args:
            outputs: Outputs to keep in the list, all outputs that are not in the provided list will be removed from
                the request.

        Returns:
            ``ModelOutputsRequest``: A new request instance filtered from unwanted outputs.
        """
        filtered: t.List[dict] = [self[o] for o in self.output_ids if o in outputs]
        return ModelOutputsRequest(self.default, self.exceptions, filtered, self.getFromModel)

    def __getitem__(self, item: str) -> dict:
        """Get outputs request for an output id.

        Args:
            item: An output id.

        Returns:
            ``dict``: The outputs request dictionary for output id.
        """
        return self.outputs[self.output_ids.index(item)]

    def __contains__(self, item: str) -> bool:
        """Check if an item is in request.

        True if in ``output_ids``, false if not.

        Args:
            item: A output id.

        Returns:
            ``bool``
        """
        return item in self.output_ids

    def __len__(self) -> int:
        """Get request length.

        Request length is the total of outputs requested.

        Returns:
            ``int``
        """
        return len(self.outputs)


class ModelOutputsResponse(Marshall):
    """Parser for responses made by the application endpoint.

    Class should be handled as immutable (dont set attributes from the outside, only use provided methods for
    interactions).

    Args:
        outputs: Outputs provided by the model.
        warnings: Warnings provided by the model.
        status: One of `` ``, ``nodata``, or ``busy``.
    """
    CNTRY_CODES = [
        'AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR',
        'DE', 'EL', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL',
        'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'CH', 'UK', 'RW',
        'WD', 'EU',
        'AT_whole-country', 'BE_whole-country', 'BG_whole-country',
        'CH_whole-country', 'CY_whole-country', 'CZ_whole-country',
        'DE_Danube', 'DE_ElbeOder', 'DE_Rhine', 'DK_whole-country',
        'EE_whole-country', 'EL_N', 'EL_S', 'ES_Ebro', 'ES_NW', 'ES_S',
        'FI_whole-country', 'FR_N', 'FR_S', 'HR_whole-country',
        'HU_whole-country', 'IE_whole-country', 'IT_M', 'IT_N', 'IT_S',
        'LT_whole-country', 'LU_whole-country', 'LV_whole-country',
        'MT_whole-country', 'NL_whole-country', 'PL_whole-country',
        'PT_whole-country', 'RO_whole-country', 'SE_whole-country',
        'SI_whole-country', 'SK_whole-country', 'UK_United Kingdom-M',
        'UK_United Kingdom-N', 'UK_United Kingdom-S'
    ]

    CNTRY_CODE_FIELDS = [
        List(key=code, required=False, missing=[], field=Num())
        for code in CNTRY_CODES
    ]
    SCHEMA = Dict(
        fields=[
            List(
                key='outputs',
                field=Dict(
                    fields=[
                        Str(key='id'),
                        Str(key='title'),
                        List(
                            key='timeAxis',
                            field=Int()
                        ),
                        Dict(
                            key='data',
                            fields=CNTRY_CODE_FIELDS
                        )
                    ]
                )
            ),
            List(
                key='warnings',
                field=Dict(fields=[Str(key='id'), Num(key='level')])
            ),
            Str(key='status', required=False, missing='', store_missing=True)
        ]
    )

    def __init__(self, outputs: t.List[dict], warnings: t.List[dict], status: str = '') -> None:
        self.outputs = outputs
        self.warnings = warnings
        self.status = status

    @property
    def output_ids(self) -> t.List[str]:
        """Get the output ids of the response."""
        return [output['id'] for output in self.outputs]

    def filter(self, request: ModelOutputsRequest) -> 'ModelOutputsResponse':
        """Filter a response by a ``ModelOutputsRequest`` object.

        Filtering is applied on the outputs (return only requested outputs) and the
        output data attribute (return only data for the requested countries).

        Args:
            request: Request to use for filtering response.

        Returns:
            ``ModelOutputsResponse``: The filtered ModelOutputsResponse.
        """
        outputs = list(filter(lambda output: output['id'] in request, self.outputs))

        for output in outputs:
            if not request.all_countries(output['id']):
                output['data'] = {
                    cntry: output['data'][cntry]
                    for cntry in request.countries
                    if cntry in output['data']
                }

        return ModelOutputsResponse(outputs, self.warnings, self.status)

    def singular(self, request: ModelOutputsRequest) -> 'ModelOutputsResponse':
        """Removes all entries from data which are not a request country

        Returns:
            A new instance where data is limited to the request country
        """
        outputs = copy(self.outputs)

        for output in outputs:
            if request.country in output['data']:
                output['data'] = {request.country: output['data'][request.country]}
            else:
                output['data'] = {}

        return ModelOutputsResponse(outputs, self.warnings, self.status)

    def rename(self, renaming: t.Dict[str, str]) -> 'ModelOutputsResponse':
        """Rename output id by applying the supplied dictionary.

        Args:
            renaming: A dictionary of outputs to rename, key is current id of the output and value is the new id.

        Returns:
            A new instance with renamed outputs
        """
        outputs = copy(self.outputs)

        for output in outputs:
            new: str = renaming.get(output['id'], None)
            if new:
                output['id'] = new

        return ModelOutputsResponse(outputs, self.warnings, self.status)

    def trim(self, year: int) -> 'ModelOutputsResponse':
        """Removes data below year from the outputs

        Returns:
            A new instance with trimmed output data
        """
        outputs = copy(self.outputs)

        for output in outputs:
            years = array(output['timeAxis'], dtype=int)
            idxs = years >= year
            output['timeAxis'] = years[idxs].tolist()
            output['data'] = {
                cntry: array(output['data'][cntry])[idxs].tolist()
                for cntry in output['data'].keys()
                if array(output['data'][cntry])[idxs].size > 0
            }

        return ModelOutputsResponse(outputs, self.warnings, self.status)

    def remove(self) -> 'ModelOutputsResponse':
        """Removes outputs where data is empty

        Returns:
            A new instance with empty outputs removed
        """
        return ModelOutputsResponse(
            list(filter(lambda output: True if output['data'] else False, self.outputs)),
            self.warnings,
            self.status
        )

    def __len__(self) -> int:
        """The length of the response.

        Length is the total of outputs.

        Returns:
            ``int``
        """
        return len(self.outputs)

    @classmethod
    def empty_nodata(cls) -> 'ModelOutputsResponse':
        """Creates a nodata response.

        Returns:
            ``proxilly.parsers.ModelOutputsResponse``: A response object with status set to nodata.
        """
        return cls(list(), list(), 'nodata')

    @classmethod
    def empty_busy(cls) -> 'ModelOutputsResponse':
        """Creates a busy response.

        Returns:
            ``proxilly.parser.ModelOutputsResponse``: A response object with status set to busy.
        """
        return cls(list(), list(), 'busy')


class ModelOutputsErrorResponse(Marshall):
    SCHEMA = Dict(
        fields=[
            Int(key='code'),
            Str(key='description'),
            Str(key='payload')
        ]
    )

    def __init__(self, code: int, description: str, response: str) -> None:
        self.code = code
        self.description = description
        self.payload = response
