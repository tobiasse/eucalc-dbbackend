from proxilly.app import create_app

__all__ = ['create_app']
__version__ = '0.9'
