"""
app
***

:Author: Tobias Seydewitz
:Date: 10.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
:Description: Module contains exclusively the Flask app factory. Call this factory function with your preferred
    WSGI-Server.
"""
from os.path import join

from flask import Flask
from peewee import DatabaseProxy
from peewee import MySQLDatabase
from peewee import SqliteDatabase

from proxilly.marshalling.errors import ValidationError

# Global database instance for models module
DB = DatabaseProxy()


def create_app(development: bool = False, my2050: bool = False) -> Flask:
    """Entry point, create an app instance for a WSGI-Server.

    Args:
        development (``bool``): If ``true`` development config will be loaded. Defaults to
            ``false`` and loads production config. (See ``instance.flask.prod`` or ``instance.flask.dev`` for details)
        my2050 (``bool``): If ``true`` my2050 production config is loaded otherwise the proxilly config. Defaults to
            ``true`` and loads production config. (See ``instance.flask.prod_proxilly``, ``instance.flask.prod_my2050``,
            or ``instance.flask.dev`` for details)

    Returns:
        ``Flask.app``: Configured app instance.
    """
    global DB

    config = 'prod_my2050.py' if my2050 else 'prod_proxilly.py'
    app_name = 'my2050' if my2050 else 'proxilly'

    app = Flask(app_name, instance_relative_config=True)

    if development:
        app.config.from_pyfile(join(app.instance_path, 'flask', 'dev.py'))
        DB.initialize(
            SqliteDatabase(
                join(app.instance_path, app.config['DB']),
                pragmas=app.config['PRAGMAS'],
                autoconnect=False
            )
        )

    else:
        app.config.from_pyfile(join(app.instance_path, 'flask', config))
        DB.initialize(
            MySQLDatabase(
                app.config['DB'],
                user=app.config['USER'],
                passwd=app.config['PASSWORD'],
                autoconnect=False,
            )
        )

    app.config['DATABASE'] = DB

    from proxilly.resources import Proxilly

    app.add_url_rule('/results', view_func=Proxilly.as_view('proxilly'), methods=['POST', ])

    if not my2050:
        from proxilly.resources import Localised
        app.add_url_rule('/localised', view_func=Localised.as_view('localised'), methods=['POST', ])

    from proxilly.resources import http_error
    from proxilly.resources import validation_error

    app.register_error_handler(400, http_error)
    app.register_error_handler(411, http_error)
    app.register_error_handler(413, http_error)
    app.register_error_handler(503, http_error)
    app.register_error_handler(ValidationError, validation_error)

    return app
