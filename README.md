# proxilly


Proxy server for the model API of the EUCalc project. Manages persistent storage of the model results and performs
basic load balancing.


## Installation


Download the proxilly repository via ``git clone https://gitlab.pik-potsdam.de/tobiasse/eucalc-dbbackend`` and follow
the steps described in the following sections. Proxilly is developed with Python version 3.6.9 but should run as well
under newer versions (without warranty). To avoid polluting your global python environment you may want to install
`pyenv`_ and create a virtual environment within the projects root directory via ``python -m venv venv``.


### Setup: development


To install proxilly as a development API execute ``make dev_install`` in the project's root directory. This will install
all the required dependencies and create an SQLite development database. The development database is populated utilizing
:py:func:`proxilly.cli.populate.populate`, to alter this behavior edit in the Makefile the ``dev_install`` rule.
The arguments for the CLI tool are documented in the :doc:`api`.

After, installing proxilly you may start the development server by executing either ``make dev_werkzeug`` or
``make dev_gunicorn``. The latter command starts a gunicorn WSGI server by using the configuration file
``instance/gunicorn/dev.py`` and the former a werkzeug server with the following configuration file
``instance/gunicorn/dev.py``. Additionally, you may start for testing interactions a mock server of the EUCalc model API
by executing ``make modelapi``. The configuration and implementation of this server can be found at
``tests/res/modelapimock``.

Further, you can run via ``make load_test`` a load test through `locust.io`_, build the HTML version of the
documentation with ``make docs``, and test the application by executing ``make tests``.

For reverting the installation just execute ``make dev_clean``, it removes the development database and removes proxilly
from the venv.

### Setup: production

To install proxilly as a production API execute ``make install`` in the projects root directory. After, you may migrate
the database structure by executing ``proxilly migrate``. Before, migrating the database structure ensure that a user
and database exists corresponding to the parameters in the ``instance/flask/prod_proxilly.py`` config file (this file is
not content of the repository but available upon request to the author). Additionally, the following permissions are
required for the database user: SELECT, CREATE, REFERENCES, UPDATE, INSERT, and DROP.