class ValidationError(Exception):
    """Raise if error occurs in schema validation."""
