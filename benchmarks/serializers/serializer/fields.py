from numbers import Number


class MissingKeyValuePair:
    def __init__(self, name):
        self.name = name


class Field:
    def __init__(self, _type, required=True, default=None, store_missing=False, validate=None):
        if default and not isinstance(default, _type):
            raise TypeError('Default "%s" must have type "%s".' % (default, _type))

        if validate and not callable(validate):
            raise TypeError('Func "%s" must be a callable.' % validate)

        self.type = _type
        self.required = required
        self.validate = validate
        self.store_missing = store_missing

        self._default = default

    @property
    def default(self):
        if self._default:
            return self._default
        else:
            return self.type()

    def parse(self, value):
        if self.required:
            return self._parse_required(value)

        else:
            return self._parse_optional(value)

    def _parse_required(self, value):
        if isinstance(value, MissingKeyValuePair):
            val, err = value, 'Missing key value pair.'

        elif not isinstance(value, self.type):
            val, err = value, 'Value has wrong type {} != {}.'.format(type(value), self.type)

        elif self.validate and not self.validate(value):
            val, err = value, 'Additional validation failed.'

        else:
            val, err = value, None

        return val, err

    def _parse_optional(self, value):
        if not isinstance(value, MissingKeyValuePair):
            if not isinstance(value, self.type):
                val, err = value, 'Value has wrong type {} != {}.'.format(type(value), self.type)

            elif self.validate and not self.validate(value):
                val, err = value, 'Additional validation failed.'

            else:
                val, err = value, None

        elif isinstance(value, MissingKeyValuePair) and self.store_missing:
            val, err = self.default, None

        else:
            val, err = value, None

        return val, err


class Int(Field):
    def __init__(self, **kwargs):
        super().__init__(int, **kwargs)


class Float(Field):
    def __init__(self, **kwargs):
        super().__init__(float, **kwargs)


class Num(Field):
    def __init__(self, **kwargs):
        super().__init__(Number, **kwargs)

    @property
    def default(self):
        if self._default:
            return self._default
        return 0


class Bool(Field):
    def __init__(self, **kwargs):
        super().__init__(bool, **kwargs)


class Str(Field):
    def __init__(self, **kwargs):
        super().__init__(str, **kwargs)


class List(Field):
    def __init__(self, **kwargs):
        super().__init__(list, **kwargs)


class Dict(Field):
    def __init__(self, **kwargs):
        super().__init__(dict, **kwargs)
