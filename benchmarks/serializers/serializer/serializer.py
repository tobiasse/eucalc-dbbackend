import json
from copy import copy, deepcopy

from benchmarks.serializers.serializer.fields import Field
from benchmarks.serializers.serializer.fields import MissingKeyValuePair
from benchmarks.serializers.serializer.errors import ValidationError


class Serializer:
    SCHEMA = None

    def as_json(self):
        pattern = deepcopy(self.__class__.SCHEMA)
        structure = self._serialize(pattern)
        return json.dumps(structure)

    def as_dict(self):
        pattern = deepcopy(self.__class__.SCHEMA)
        return self._serialize(pattern)

    def _serialize(self, schema):
        for key, field in schema.items():
            if field and isinstance(field, dict):
                schema[key] = self._serialize(field)

            else:
                schema[key] = self.__getattribute__(key)

        return schema

    @classmethod
    def from_json(cls, content, checked=True):
        structure = json.loads(content)

        if checked:
            structure = cls._validate(structure, cls.SCHEMA)

        return cls(**cls._deserialize(structure, cls.SCHEMA))

    @classmethod
    def from_dict(cls, structure, checked=True):
        struct = copy(structure)

        if checked:
            struct = cls._validate(struct, cls.SCHEMA)

        return cls(**cls._deserialize(struct, cls.SCHEMA))

    @classmethod
    def _deserialize(cls, structure, schema):
        kwargs = dict()

        for key, field in schema.items():
            if isinstance(field, Field):
                kwargs[key] = structure[key]

            elif isinstance(field, list):
                kwargs[key] = structure[key]

            else:
                kwargs.update(cls._deserialize(structure[key], field))

        return kwargs

    @classmethod
    def _validate(cls, structure, schema):
        if isinstance(schema, dict) and isinstance(structure, dict):
            for key, field in schema.items():
                value = structure.get(key, MissingKeyValuePair(key))
                if isinstance(field, Field):
                    value, error = field.parse(value)
                    if error:
                        raise ValidationError('%s: %s' % (key, error))
                    if field.store_missing:
                        structure[key] = value
                elif isinstance(field, dict) and isinstance(value, dict):
                    cls._validate(value, field)
                elif isinstance(field, list) and isinstance(value, list):
                    cls._validate(value, field)
                else:
                    raise ValidationError('%s: Unknown type "%s".' % (key, type(value)))

        elif isinstance(schema, list) and isinstance(structure, list):
            if len(schema) > 1:
                raise ValidationError('Lists allow one field definition.')

            field = schema[0]

            for item in structure:
                if isinstance(field, Field):
                    _, error = field.parse(item)
                    if error:
                        raise ValidationError('%s: %s' % (item, error))
                elif isinstance(field, (list, dict)):
                    cls._validate(item, field)
                else:
                    raise ValidationError('%s: Unknown type "%s".' % (item, type(item)))

        else:
            raise ValidationError()

        return structure
