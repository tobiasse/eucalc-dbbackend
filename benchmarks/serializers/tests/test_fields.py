from unittest import TestCase
from unittest.mock import patch

from benchmarks.serializers.serializer.fields import Bool
from benchmarks.serializers.serializer.fields import Dict
from benchmarks.serializers.serializer.fields import Field
from benchmarks.serializers.serializer.fields import Float
from benchmarks.serializers.serializer.fields import Int
from benchmarks.serializers.serializer.fields import List
from benchmarks.serializers.serializer.fields import Num
from benchmarks.serializers.serializer.fields import Str
from benchmarks.serializers.serializer.fields import MissingKeyValuePair


class TestFields(TestCase):
    def test_init_default_wrong_type(self):
        with self.assertRaises(TypeError):
            Field(int, default='foo')
            Num(default='foo')

    def test_init_func_non_callable(self):
        with self.assertRaises(TypeError):
            Field(int, validate='foo')

    @patch.object(Field, '_parse_required')
    def test_parse_required(self, method):
        obj = Field(int)

        obj.parse(0)

        method.assert_called_once()
        method.assert_called_with(0)

    def test_parse_required_none(self):
        obj = Field(int)

        expected = None, 'Value has wrong type {} != {}.'.format(type(None), int)
        actual = obj._parse_required(None)

        self.assertEqual(expected, actual)

    def test_parse_required_wrong_type(self):
        obj = Field(int)

        expected = .0, 'Value has wrong type {} != {}.'.format(float, int)
        actual = obj._parse_required(.0)

        self.assertEqual(expected, actual)

    def test_parse_required_func(self):
        obj = Field(int, validate=lambda x: x > 4)

        expected = 0, 'Additional validation failed.'
        actual = obj._parse_required(0)

        self.assertEqual(expected, actual)

    def test_parse_required_missing(self):
        obj = Field(int)
        value = MissingKeyValuePair('foo')

        expected = value, 'Missing key value pair.'
        actual = obj._parse_required(value)

        self.assertEqual(expected, actual)

    @patch.object(Field, '_parse_optional')
    def test_parse_optional(self, method):
        obj = Field(int, required=False)

        obj.parse(0)

        method.assert_called_once()
        method.assert_called_with(0)

    def test_parse_optional_missing_store(self):
        obj = Field(int, required=False, store_missing=True)
        value = MissingKeyValuePair('foo')

        expected = 0, None
        actual = obj._parse_optional(value)

        self.assertEqual(expected, actual)

    def test_parse_optional_missing(self):
        obj = Field(int, required=False)
        value = MissingKeyValuePair('foo')

        expected = value, None
        actual = obj._parse_optional(value)

        self.assertEqual(expected, actual)

    def test_parse_optional_type(self):
        obj = Field(int, required=False)
        value = 'foo'

        expected = value, 'Value has wrong type {} != {}.'.format(type(value), int)
        actual = obj._parse_optional(value)

        self.assertEqual(expected, actual)

    def test_parse_optional_validate(self):
        obj = Field(int, required=False, validate=lambda x: x > 0)
        value = 0

        expected = value, 'Additional validation failed.'
        actual = obj._parse_optional(value)

        self.assertEqual(expected, actual)

    def test_parse_optional_none(self):
        obj = Field(int, required=False)
        value = None

        expected = value, 'Value has wrong type {} != {}.'.format(type(value), int)
        actual = obj._parse_optional(value)

        self.assertEqual(expected, actual)

    def test_int_field(self):
        obj = Int()

        expected = 0, None
        actual = obj.parse(0)

        self.assertEqual(expected, actual)

    def test_float_field(self):
        obj = Float()

        expected = .0, None
        actual = obj.parse(.0)

        self.assertEqual(expected, actual)

    def test_num_field_float(self):
        obj = Num()

        expected = .0, None
        actual = obj.parse(.0)

        self.assertEqual(expected, actual)

    def test_num_field_int(self):
        obj = Num()

        expected = 0, None
        actual = obj.parse(0)

        self.assertEqual(expected, actual)

    def test_num_field_default(self):
        obj = Num()
        self.assertEqual(0, obj.default)

    def test_bool_field(self):
        obj = Bool()

        expected = True, None
        actual = obj.parse(True)

        self.assertEqual(expected, actual)

    def test_str_field(self):
        obj = Str()

        expected = '', None
        actual = obj.parse('')

        self.assertEqual(expected, actual)

    def test_list_field(self):
        obj = List()

        expected = [], None
        actual = obj.parse([])

        self.assertEqual(expected, actual)

    def test_dict_field(self):
        obj = Dict()

        expected = {}, None
        actual = obj.parse({})

        self.assertEqual(expected, actual)
