from unittest import TestCase

from benchmarks.serializers.marshalling.keys import StringKey
from benchmarks.serializers.marshalling.errors import ValidationError
from benchmarks.serializers.marshalling.marshall import Marshall


class Foo(Marshall):
    def __init__(self):
        super().__init__()
        self.foo = 'foo'


class TestStringKey(TestCase):
    def test_successful_init_if_argument_is_string(self):
        key = StringKey('foo')
        self.assertTrue(isinstance(key, StringKey))

    def test_error_raised_if_init_argument_is_not_a_string(self):
        with self.assertRaises(ValidationError):
            StringKey(1)

    def test_get_from_dict_with_key_exists(self):
        key = StringKey('foo')

        expected = ('foo', 'bar')
        actual = key.get_from_dict({'foo': 'bar'})

        self.assertEqual(expected, actual)

    def test_get_from_object_returns_key_value_pair_if_key_exists(self):
        key = StringKey('foo')

        expected = 'foo'
        key, actual = key.get_from_object(Foo())

        self.assertEqual(expected, actual)

    def test_get_from_object_raises_error_if_key_not_exists(self):
        key = StringKey('bar')

        with self.assertRaises(ValidationError):
            key.get_from_object(Foo())

    def test_name_property(self):
        expected = 'foo'

        key = StringKey(expected)
        actual = key.name

        self.assertEqual(expected, actual)
