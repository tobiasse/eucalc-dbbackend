from unittest import TestCase
from unittest import skip
from unittest.mock import PropertyMock
from unittest.mock import patch

from benchmarks.serializers.serializer.errors import ValidationError
from benchmarks.serializers.serializer.fields import Bool
from benchmarks.serializers.serializer.fields import Dict
from benchmarks.serializers.serializer.fields import Float
from benchmarks.serializers.serializer.fields import Int
from benchmarks.serializers.serializer.fields import List
from benchmarks.serializers.serializer.fields import Num
from benchmarks.serializers.serializer.fields import Str
from benchmarks.serializers.serializer.serializer import Serializer


class Subclass(Serializer):
    SCHEMA = dict()

    def __init__(self, *args, **kwargs):
        pass


@skip
class TestSerializer(TestCase):
    def test_as_json(self):
        self.fail()

    def test_as_dict(self):
        self.fail()

    def test_serialize(self):
        self.fail()

    @patch('json.loads', return_value='structure')
    @patch.object(Serializer, '_deserialize', return_value={'foo': 'bar'})
    @patch.object(Serializer, '_validate', return_value='structure')
    @patch.object(Subclass, 'SCHEMA', new_callable=PropertyMock, return_value='pattern')
    @patch.object(Subclass, '__init__', return_value=None)
    def test_from_json_checked(self, init, pattern, validate, deserialize, json):
        Subclass.from_json('content')

        json.assert_called_once()
        json.assert_called_with('content')
        pattern.assert_called()
        validate.assert_called_once()
        validate.assert_called_with('structure', 'pattern')
        deserialize.assert_called_once()
        deserialize.assert_called_with('structure', 'pattern')
        init.assert_called_once()
        init.assert_called_with(foo='bar')

    @patch('json.loads', return_value='structure')
    @patch.object(Serializer, '_deserialize', return_value={'foo': 'bar'})
    @patch.object(Serializer, '_validate')
    @patch.object(Subclass, 'SCHEMA', new_callable=PropertyMock, return_value='pattern')
    @patch.object(Subclass, '__init__', return_value=None)
    def test_from_json_unchecked(self, init, pattern, validate, deserialize, json):
        Subclass.from_json('content', checked=False)

        validate.assert_not_called()

    @patch.object(Serializer, '_deserialize', return_value={'foo': 'bar'})
    @patch.object(Serializer, '_validate', return_value='structure')
    @patch.object(Subclass, 'SCHEMA', new_callable=PropertyMock, return_value='pattern')
    @patch.object(Subclass, '__init__', return_value=None)
    def test_from_dict_checked(self, init, pattern, validate, deserialize):
        Subclass.from_dict('structure')

        pattern.assert_called()
        validate.assert_called_once()
        validate.assert_called_with('structure', 'pattern')
        deserialize.assert_called_once()
        deserialize.assert_called_with('structure', 'pattern')
        init.assert_called_once()
        init.assert_called_with(foo='bar')

    @patch.object(Serializer, '_deserialize', return_value={'foo': 'bar'})
    @patch.object(Serializer, '_validate')
    @patch.object(Subclass, 'SCHEMA', new_callable=PropertyMock, return_value='pattern')
    @patch.object(Subclass, '__init__', return_value=None)
    def test_from_dict_unchecked(self, init, pattern, validate, deserialize):
        Subclass.from_dict('structure', checked=False)

        validate.assert_not_called()

    def test_deserialize_dictionary(self):
        schema = {'attr1': Int(), 'attr2': List(), 'attr3': Dict()}
        struct = {'attr1': 1, 'attr2': [], 'attr3': {}}

        expected = {'attr1': 1, 'attr2': [], 'attr3': {}}
        actual = Serializer._deserialize(struct, schema)

        self.assertEqual(expected, actual)

    def test_deserialize_nested_dictionary(self):
        schema = {'attr1': Int(), 'attr2': Str(),
                  'attr3': {'attr4': {'attr5': Bool(), 'attr6': List()}},
                  'attr7': {'attr8': Str(), 'attr9': Dict()}}
        struct = {'attr1': 1, 'attr2': 'Foo',
                  'attr3': {'attr4': {'attr5': True, 'attr6': [1, 2, 3]}},
                  'attr7': {'attr8': 'Bar', 'attr9': {}}}

        expected = {'attr1': 1, 'attr2': 'Foo',
                    'attr5': True, 'attr6': [1, 2, 3],
                    'attr8': 'Bar', 'attr9': {}}
        actual = Serializer._deserialize(struct, schema)

        self.assertEqual(expected, actual)

    def test_deserialize_list_dictionary(self):
        schema = None
        struct = None

        expected = None
        actual = Serializer._deserialize(struct, schema)

        self.assertEqual(expected, actual)

    def test_validate_dictionary(self):
        schema = {'attr1': Str(), 'attr2': Int(), 'attr3': Num(),
                  'attr4': List(), 'attr5': Float(required=False, store_missing=True, default=2.),
                  'attr6': Dict()}
        struct = {'attr1': '', 'attr2': 0, 'attr3': .1, 'attr4': [1, 2, 3], 'attr6': {'EU': ['data']}}

        expected = {'attr1': '', 'attr2': 0, 'attr3': .1,
                    'attr4': [1, 2, 3], 'attr5': 2., 'attr6': {'EU': ['data']}}
        actual = Serializer._validate(struct, schema)

        self.assertEqual(expected, actual)

    def test_validate_dictionary_wrong_type(self):
        schema = {'attr1': Int()}
        struct = {'attr1': 'Foo'}

        with self.assertRaises(ValidationError):
            Serializer._validate(struct, schema)

    def test_validate_dictionary_missing_key(self):
        schema = {'attr1': Str()}
        struct = {}

        with self.assertRaises(ValidationError):
            Serializer._validate(struct, schema)

    def test_validate_dictionary_validate(self):
        schema = {'attr1': Int(validate=lambda x: x > 5)}
        struct = {'attr1': 0}

        with self.assertRaises(ValidationError):
            Serializer._validate(struct, schema)

    def test_validate_nested_dictionary(self):
        schema = {'attr1': {'attr2': Int(), 'attr3': Bool(required=False)},
                  'attr4': {'attr5': {'attr6': Str(required=False, store_missing=True, default='foo')}}}
        struct = {'attr1': {'attr2': 1},
                  'attr4': {'attr5': {}}}

        expected = {'attr1': {'attr2': 1},
                    'attr4': {'attr5': {'attr6': 'foo'}}}
        actual = Serializer._validate(struct, schema)

        self.assertEqual(expected, actual)

    def test_validate_list_dictionary(self):
        schema = {'attr1': [Num(validate=lambda x: x > 4)],
                  'attr2': [Str(required=False)],
                  'attr3': [{'attr4': Int(), 'attr5': Str(required=False, store_missing=True, default='Foo')}],
                  'attr4': List(required=False)}
        struct = {'attr1': [5, 6, 7, 8],
                  'attr2': [],
                  'attr3': [{'attr4': 1}, {'attr4': 1, 'attr5': 'Bar'}]}

        expected = {'attr1': [5, 6, 7, 8],
                    'attr2': [],
                    'attr3': [{'attr4': 1, 'attr5': 'Foo'}, {'attr4': 1, 'attr5': 'Bar'}]}
        actual = Serializer._validate(struct, schema)

        self.assertEqual(expected, actual)

    def test_validate_list_wrong_type(self):
        schema = {'attr1': [Num()]}
        struct = {'attr1': [1, 2, 3, 4, 'foo']}

        with self.assertRaises(ValidationError):
            Serializer._validate(struct, schema)

    def test_validate_list(self):
        schema = [Num()]
        struct = [1., 2, .3, .4]

        expected = [1., 2, .3, .4]
        actual = Serializer._validate(struct, schema)

        self.assertEqual(expected, actual)
