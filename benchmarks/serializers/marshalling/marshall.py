from json import JSONDecodeError
from json import dumps
from json import loads
from typing import TypeVar

from benchmarks.serializers.marshalling.errors import ValidationError
from benchmarks.serializers.marshalling.fields import _Field

TMarshall = TypeVar('TMarshall', bound='Marshall')


class Marshall:
    # Override this class attribute in child classes.
    SCHEMA: _Field = None

    def __init__(self, **kwargs) -> None:
        self.kwargs = kwargs

    def as_json(self, checked: bool = False) -> str:
        struct = self.__class__.SCHEMA.serialize(self)

        if checked:
            struct = self.__class__.SCHEMA.parse(struct)

        return dumps(struct)

    def as_dict(self, checked: bool = False) -> dict:
        struct = self.__class__.SCHEMA.serialize(self)

        if checked:
            struct = self.__class__.SCHEMA.parse(struct)

        return struct

    @classmethod
    def from_json(cls, content: str, checked: bool = True) -> TMarshall:
        try:
            struct = loads(content)
        except JSONDecodeError as err:
            raise ValidationError(str(err))

        if checked:
            struct = cls.SCHEMA.parse(struct)

        return cls(**cls.SCHEMA.deserialize(struct))

    @classmethod
    def from_dict(cls, struct: dict, checked: bool = True) -> TMarshall:
        if checked:
            struct = cls.SCHEMA.parse(struct)

        return cls(**cls.SCHEMA.deserialize(struct))
