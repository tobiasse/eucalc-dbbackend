from abc import ABCMeta
from abc import abstractmethod
from typing import Any
from typing import TYPE_CHECKING
from typing import Tuple
from typing import Union

from benchmarks.serializers.marshalling.errors import ValidationError

if TYPE_CHECKING:
    from benchmarks.serializers.marshalling.marshall import Marshall


# TODO optimizations:
#  call get_from_dict/get_from_object directly
class _Key(metaclass=ABCMeta):
    def __init__(self, name: str) -> None:
        self._name = name

    @property
    @abstractmethod
    def name(self) -> str:
        pass

    def get(self, struct_or_obj: Union[dict, 'Marshall']) -> Tuple[str, Any]:
        from benchmarks.serializers.marshalling.marshall import Marshall

        if isinstance(struct_or_obj, dict):
            return self._get_from_dict(struct_or_obj)

        elif isinstance(struct_or_obj, Marshall):
            return self._get_from_object(struct_or_obj)

        else:
            msg = 'Get is only allowed on dictionaries, but struct is a %s.' % type(struct_or_obj).__name__
            raise ValidationError(msg)

    @abstractmethod
    def _get_from_dict(self, struct: dict) -> Tuple[str, Any]:
        pass

    @abstractmethod
    def _get_from_object(self, obj: 'Marshall') -> Tuple[str, Any]:
        pass

    def __str__(self) -> str:
        return '{}(name=())'.format(self.__class__.__name__, self._name)

    def __repr__(self) -> str:
        return '<{}(name={}) at {}>'.format(self.__class__.__name__, self._name, hex(id(self)))


class StringKey(_Key):
    def __init__(self, name: str) -> None:
        if isinstance(name, str):
            super().__init__(name)

        else:
            msg = '{} name must be a string, but name is a {}.'.format(self.__class__.__name__, type(name).__name__)
            raise ValidationError(msg)

    @property
    def name(self) -> str:
        return self._name

    # TODO OPTIMIZED
    def _get_from_dict(self, struct: dict) -> Tuple[str, Any]:
        try:
            return self.name, struct[self.name]
        except KeyError:
            raise ValidationError('Missing key "%s".' % self.name)

        # TODO OPTIMIZED old code
        # value = struct.get(self.name, ValidationError('Missing key "%s".' % self.name))
        #
        # if isinstance(value, ValidationError):
        #     raise value
        #
        # return self.name, value

    # TODO OPTIMIZED no improvement
    def _get_from_object(self, obj: 'Marshall') -> Tuple[str, Any]:
        try:
            # TODO OPTIMIZED
            # value = obj.__getattribute__(self.name)  # old code
            # return self.name, obj.__getattribute__(self.name)  # old code
            return self.name, getattr(obj, self.name)  # no relevant performance improvement
        except AttributeError:
            raise ValidationError('Attribute "%s" not found in "%s".' % (self.name, obj.__class__.__name__))

        # TODO OPTIMIZED
        # return self.name, value # old code
