from numbers import Number
from typing import Any
from typing import Callable
from typing import Sequence
from typing import TYPE_CHECKING
from typing import Tuple
from typing import Union

from benchmarks.serializers.marshalling.errors import ValidationError
from benchmarks.serializers.marshalling.keys import StringKey
from benchmarks.serializers.marshalling.keys import _Key

if TYPE_CHECKING:
    from benchmarks.serializers.marshalling.marshall import Marshall

__all__ = [
    'Str',
    'Int',
    'Float',
    'Num',
    'Bool',
    'Dict',
    'List'
]


# TODO optimisations:
#  call directly parse_from_dict/parse_from_list in fields Dict and List
#  call directly parse_from_list in parse _Field
#  call directly key._get_from_dict/key._get_from_object in _Field
class _Field:
    # Override this in child classes with the type the child represents.
    TYPE = None

    def __init__(
            self,
            key: Union[_Key, str, None] = None,
            required: bool = True,
            nullable: bool = False,
            store_missing: bool = False,
            missing: Any = None,
            validator: Union[
                Callable[[Any], bool],
                Sequence[Callable[[Any], bool]],
            ] = None,
            modifier: Union[
                Callable[[Any], Any],
                Sequence[Callable[[Any], Any]],
            ] = None
    ) -> None:

        if not (missing is None or isinstance(missing, self.__class__.TYPE)):
            raise ValidationError('Field "{}" parameter "missing" must be "None" or "{}".'.format(
                self.__class__.__name__,
                self.__class__.TYPE.__name__
            ))

        if required and missing is not None:
            raise ValidationError('Parameter "missing" must not be set for required fields.')

        if not required and not nullable and missing is None:
            raise ValidationError('Logic error, set "nullable" true or set "missing" to "{}".'.format(
                self.__class__.TYPE.__name__ if self.__class__.TYPE is not None else type(self.__class__.TYPE).__name__
            ))

        if callable(validator):
            self.validator = [validator]
        elif isinstance(validator, (list, tuple, set)) and all(callable(func) for func in validator):
            self.validator = validator
        elif validator is None:
            self.validator = list()
        else:
            raise ValidationError('Parameter "validator" must be a callable or a sequence of callable.')

        if callable(modifier):
            self.modifier = [modifier]
        elif isinstance(modifier, (list, tuple, set)) and all(callable(func) for func in modifier):
            self.modifier = modifier
        elif modifier is None:
            self.modifier = list()
        else:
            raise ValidationError('Parameter "modifier" must be a callable or a sequence of callable.')

        if isinstance(key, _Key) or key is None:
            self.key = key
        elif isinstance(key, str):
            self.key = StringKey(key)
        else:
            raise ValidationError('Key must be a instance of "%s" or "%s".' % (type(_Key), None))

        self.missing = missing
        self.required = required
        self.nullable = nullable
        self.store_missing = store_missing

        self._root = True
        self._flat = False
        self._errors = list()
        self._in_dict = isinstance(self.key, _Key)

    @property
    def in_dict(self) -> bool:
        return self._in_dict

    @property
    def flat(self) -> bool:
        return self._flat

    @property
    def root(self) -> bool:
        return self._root

    def is_root(self) -> bool:
        if self.root and not self.in_dict:
            return True
        return False

    def serialize(self, obj: 'Marshall') -> dict:
        if self.is_root():
            return self._serialize(obj)[1]
        else:
            raise ValidationError('Serialization should start from root field.')

    def deserialize(self, struct: dict) -> dict:
        if self.is_root():
            return self._deserialize(struct)[1]
        else:
            raise ValidationError('Deserialization should start from root field.')

    # TODO OPTIMIZED
    def parse(self, struct: dict) -> dict:
        if self.is_root():
            # TODO OPTIMIZED
            # return self._parse((0, struct))[1]  # old code
            return self._parse_from_list(0, struct)[1]
        else:
            raise ValidationError('Parsing should start from root field.')

    # TODO OPTIMIZED
    def _serialize(self, obj: 'Marshall') -> Tuple[Union[str, None], Any]:
        # TODO OPTIMIZED
        # if self.is_root():  # old code
        #     raise ValidationError('Serialization reached a root field, at this point only child fields are expected.')  # old code

        try:
            # TODO OPTIMIZED
            # return self.key.get(obj)  # old code
            return self.key._get_from_object(obj)
        except ValidationError as err:
            if self.required:
                raise err
            else:
                return None, None

    # TODO OPTIMIZED
    def _deserialize(self, struct: dict) -> Tuple[Union[str, None], Any]:
        # TODO OPTIMIZED
        # if self.is_root():  # old code
        #     raise ValidationError('Deserialization reached a root field, at this point only child fields are excepted.')  # old code

        try:
            # TODO OPTIMIZED
            # return self.key.get(struct)  # old code
            return self.key._get_from_dict(struct)
        except ValidationError as err:
            if self.required:
                raise err
            else:
                return None, None

    def _parse(self, value: Union[dict, Tuple[int, Any]]) -> Tuple[Union[str, int, None], Any]:
        if self._in_dict:
            return self._parse_from_dict(value)
        return self._parse_from_list(*value)

    def _parse_from_list(self, idx: int, value: Any) -> Tuple[int, Any]:
        return self._validate(idx, value)

    # TODO OPTIMIZED
    def _parse_from_dict(self, struct: dict) -> Tuple[Union[str, None], Any]:
        try:
            # TODO OPTIMIZED
            # key, value = self.key.get(struct)  # old code
            return self._validate(*self.key._get_from_dict(struct))
        except ValidationError as err:
            if self.required:
                raise err
            elif self.store_missing:
                # TODO OPTIMIZED
                # key, value = self.key.name, self.missing  # old code
                return self._validate(self.key.name, self.missing)
            else:
                return None, None

        # TODO OPTIMIZED
        # return self._validate(key, value)  # old code

    def _validate(self, idx_or_key: Union[int, str], value: Any) -> Tuple[Any, Any]:
        value = self._apply_modifier(value)

        if not self._validate_type(value):
            raise ValidationError(
                'At index/key "{}" type miss-match must be "{}" but is "{}".'.format(
                    idx_or_key,
                    self.__class__.TYPE.__name__,
                    type(value).__name__
                )
            )

        self._apply_validator(idx_or_key, value)

        if self._errors:
            msg = '\n'.join(self._errors)
            self._errors = list()
            raise ValidationError(msg)

        return idx_or_key, value

    # TODO OPTIMIZED
    def _validate_type(self, value: Any) -> bool:
        if type(value) == self.__class__.TYPE or (value is None and self.nullable):  # old code
            return True  # old code
        return False  # old code

        # TODO OPTIMIZED
        # if isinstance(value, self.__class__.TYPE) or (value is None and self.nullable):  # old code
        #     return True  # old code
        # return False  # old code

    def _apply_validator(self, idx_or_key: Union[int, str], value: Any) -> None:
        for func in self.validator:
            if not func(value):
                self._errors.append(
                    'At index/key "{}" validation "{}({})" failed.'.format(idx_or_key, func.__name__, value)
                )

    def _apply_modifier(self, value: Any) -> Any:
        for func in self.modifier:
            value = func(value)
        return value

    def _set_root(self) -> None:
        self._root = not self._root

    def __str__(self) -> str:
        return '{}(k={}, r={}, n={}, sm={}, m={}, r={}, id={})'.format(
            self.__class__.__name__,
            str(self.key),
            self.required,
            self.nullable,
            self.store_missing,
            self.missing,
            self.root,
            self.in_dict
        )

    def __repr__(self) -> str:
        return '{}(k={}, r={}, n={}, sm={}, m={}, r={}, id={}) at {}'.format(
            self.__class__.__name__,
            str(self.key),
            self.required,
            self.nullable,
            self.store_missing,
            self.missing,
            self.root,
            self.in_dict,
            hex(id(self))
        )


class Str(_Field):
    TYPE = str


class Int(_Field):
    TYPE = int


class Float(_Field):
    TYPE = float


class Num(_Field):
    TYPE = Number

    def _validate_type(self, value: Any) -> bool:
        if isinstance(value, self.__class__.TYPE) or (value is None and self.nullable):  # old code
            return True  # old code
        return False


class Bool(_Field):
    TYPE = bool


class Dict(_Field):
    TYPE = dict

    def __init__(self, fields: Union[_Field, Sequence[_Field]] = None, flat: bool = False, **kwargs):
        super().__init__(**kwargs)

        if isinstance(fields, (list, tuple, set)) and all(isinstance(field, _Field) for field in fields):
            self.fields = fields
        elif isinstance(fields, _Field):
            self.fields = [fields]
        elif fields is None:
            self.fields = []
        else:
            raise ValidationError('Fields sequence must be a sequence of "%s".' % __class__.__name__)

        if not all(field.in_dict for field in self.fields):
            raise ValidationError('For "%s" fields a key is required.' % self.__class__.__name__)

        self._flat = flat
        # Set on child fields root false, ensures that only for top dictionary root is true.
        [field._set_root() for field in self.fields]

    def _serialize(self, obj: 'Marshall') -> Tuple[Union[str, None], Union[dict, None]]:
        if self.root:
            dkey = 'root'
        elif self.flat:
            dkey = self.key.name
        else:
            return super()._serialize(obj)

        parsed = self.__class__.TYPE()

        for field in self.fields:
            fkey, fvalue = field._serialize(obj)

            if fkey:
                parsed[fkey] = fvalue

        return dkey, parsed

    # TODO OPTIMIZED no improvement
    def _deserialize(self, struct: dict) -> Tuple[Union[str, None], Union[dict, None]]:
        if self.root:
            dkey, dvalue = 'root', struct
        else:
            dkey, dvalue = super()._deserialize(struct)

            if dkey is None:
                return dkey, dvalue

        # TODO OPTIMIZED
        # if dkey is None:  # old code
        #     return dkey, dvalue  # old code

        parsed = self.__class__.TYPE()

        for field in self.fields:
            fkey, fvalue = field._deserialize(dvalue)

            if fkey:
                if field.flat:
                    parsed.update(fvalue)
                else:
                    parsed[fkey] = fvalue

        return dkey, parsed

    # TODO OPTIMIZED
    def _validate(self, idx_or_key: Union[int, str], value: Any) -> Tuple[Any, Any]:
        idx_or_key, value = super()._validate(idx_or_key, value)
        parsed = self.__class__.TYPE()

        for field in self.fields:
            try:
                # TODO OPTIMIZED
                # key, val = field._parse(value)  # old code
                key, val = field._parse_from_dict(value)
                if key:
                    parsed[key] = val
            except ValidationError as err:
                self._errors.append(str(err))

        if self._errors:
            msg = '\n'.join(self._errors)
            self._errors = list()
            raise ValidationError(msg)

        return idx_or_key, parsed

    def __str__(self) -> str:
        return '{}(k={}, r={}, n={}, sm={}, m={}, r={}, id={}, f={}, f={})'.format(
            self.__class__.__name__,
            str(self.key),
            self.required,
            self.nullable,
            self.store_missing,
            self.missing,
            self.root,
            self.in_dict,
            self.flat,
            [str(field) for field in self.fields]
        )


class List(_Field):
    TYPE = list

    def __init__(self, field: _Field, **kwargs) -> None:
        super().__init__(**kwargs)

        if not isinstance(field, _Field):
            raise ValidationError('Field must be an instance of "%s", but is "%s".' % (__class__.__name__, type(field)))

        if field.in_dict:
            raise ValidationError('A "%s" field should not have a key.' % self.__class__.__name__)

        self.field = field
        self.field._set_root()  # Set on child field root false.

    # TODO OPTIMIZED
    def _validate(self, idx_or_key: Union[int, str], value: Any) -> Tuple[Any, Any]:
        idx_or_key, value = super()._validate(idx_or_key, value)
        parsed = self.__class__.TYPE([None]) * len(value)

        for idx, val in enumerate(value):
            try:
                # TODO OPTIMIZED
                # idx, val = self.field._parse((idx, val))  # old code
                idx, val = self.field._parse_from_list(idx, val)
                parsed[idx] = val
            except ValidationError as err:
                self._errors.append(str(err))

        if self._errors:
            msg = '\n'.join(self._errors)
            self._errors = list()
            raise ValidationError(msg)

        return idx_or_key, parsed

    def __str__(self) -> str:
        return '{}(k={}, r={}, n={}, sm={}, m={}, r={}, id={}, f={})'.format(
            self.__class__.__name__,
            str(self.key),
            self.required,
            self.nullable,
            self.store_missing,
            self.missing,
            self.root,
            self.in_dict,
            self.field,
        )
