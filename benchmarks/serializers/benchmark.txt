Benchmark executed on Dell Latitude 7400

a. from_dict checked=true
b. from_dict checked=false
c. as_dict checked=true
d. as_dict checked=false

20 x 10 000 executions
     M vs. M                    Marshall                       S              M vs. S or S vs. M
a. 2.37x faster        1.792/ 1.217/ 1.072/ 1.026/ 0.755     0.688       serializer is 1.10x faster
b. 3.03x faster        0.176/ 0.101/ 0.062/ 0.058/ 0.058     0.021       serializer is 2.76x faster
c. 2.34x faster        1.698/ 1.152/ 1.041/ 0.997/ 0.725     NA          NA
d. 2.29x faster        0.080/ 0.036/ 0.038/ 0.035/ 0.035     0.487       marshall is 13.91x faster

- First four times are measured with Num()
- Last time is measured with Float()

20 x 100 000 executions
a. 12.907/ 10.095/ 10.516      7.145
b. 01.009/ 00.579/ 00.589      0.220
c. 11.639/ 09.880/ 10.425      NA
d. 00.370/ 00.355/ 00.384      5.268