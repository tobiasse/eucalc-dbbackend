from pprint import pprint
from timeit import repeat

import benchmarks.serializers.marshalling.fields as mf
import benchmarks.serializers.serializer.fields as sf
from benchmarks.serializers.marshalling.marshall import Marshall
from benchmarks.serializers.serializer.serializer import Serializer


class MarshallRequest(Marshall):
    SCHEMA = mf.Dict(
        fields=[
            mf.Dict(
                key='levers',
                fields=[
                    mf.List(key='default', field=mf.Float()),
                    mf.Dict(
                        key='exceptions',
                        fields=[mf.List(key='country', field=mf.Float(), required=False, nullable=True)]
                    )
                ],
                flat=True
            ),
            mf.List(
                key='outputs',
                field=mf.Dict(
                    fields=[
                        mf.Str(key='id'),
                        mf.Bool(key='allCountries', required=False, store_missing=True, missing=False)
                    ]
                )
            ),
            mf.Bool(
                key='getFromModel',
                required=False,
                store_missing=True,
                missing=False
            )
        ]
    )

    def __init__(self, default, exceptions, outputs, getFromModel):
        super().__init__()

        self.default = default
        self.exceptions = exceptions
        self.outputs = outputs
        self.getFromModel = getFromModel


class SerializerRequest(Serializer):
    SCHEMA = {
        'levers': {
            'default': [sf.Float(), ],
            'exceptions': sf.Dict()
        },
        'outputs': [{
            'id': sf.Str(),
            'allCountries': sf.Bool(required=False, store_missing=True, default=False)
        }, ],
        'getFromModel': sf.Bool(required=False, store_missing=True, default=False),
    }

    def __init__(self, default, exceptions, outputs, getFromModel):
        super().__init__()

        self.default = default
        self.exceptions = exceptions
        self.outputs = outputs
        self.getFromModel = getFromModel


if __name__ == '__main__':
    req = {
        'levers': {
            'default': [1.5, 2.1, 2.4, 2., 1.5, 2., 2., 1.8, 1.8, 2.,
                        1., 2., 1., 3., 1.9, 3.8, 3.5, 3.4, 3.7, 2.3,
                        2.6, 2., 3., 2., 3., 3., 3., 2.8, 2.9, 2.9, 2.9,
                        2.9, 2.8, 1., 4., 3., 4., 3.1, 3.1, 2., 3., 4.,
                        2., 2., 3., 1., 1., 1., 3., 1., 1., 2., 2., 1., 2., 2.,
                        2., 1., 2.],
            'exceptions': {}},
        'outputs': [
            {'id': 'ind_energy-cost[MEUR]', 'allCountries': True},
            {'id': 'elc_opex_RES_wind_onshore[MEUR]', 'allCountries': True},
            {'id': 'tra_technology-share-fleet_passenger_LDV_ICE-gasoline[%]', 'allCountries': True},
            {'id': 'bld_floor-area_non-residential_offices[million_m2]', 'allCountries': True},
            {'id': 'ind_energy-cost[MEUR]', 'allCountries': True},
            {'id': 'clt_global-temp[degC]', 'allCountries': True},
            {'id': 'elc_capex_RES_other_geothermal[MEUR]', 'allCountries': True},
            {'id': 'agr_domestic-production_afw_starch[kcal]', 'allCountries': True},
            {'id': 'tra_transport-demand_passenger_bus[pkm]', 'allCountries': True},
            {'id': 'ind_opex-cost[MEUR]', 'allCountries': True}],
        'getFromModel': False
    }

    ma_obj = MarshallRequest.from_dict(req, checked=True)
    se_obj = SerializerRequest.from_dict(req)

    n = 10000
    r = 20
    digits = 3

    ma_from_dict_checked = repeat('MarshallRequest.from_dict(req, checked=True)', globals=globals(), number=n, repeat=r)
    ma_from_dict_unchecked = repeat('MarshallRequest.from_dict(req, checked=False)', globals=globals(), number=n, repeat=r)
    se_from_dict_checked = repeat('SerializerRequest.from_dict(req, checked=True)', globals=globals(), number=n, repeat=r)
    se_from_dict_unchecked = repeat('SerializerRequest.from_dict(req, checked=False)', globals=globals(), number=n, repeat=r)

    ma_to_dict_checked = repeat('ma_obj.as_dict(checked=True)', globals=globals(), number=n, repeat=r)
    ma_to_dict_unchecked = repeat('ma_obj.as_dict(checked=False)', globals=globals(), number=n, repeat=r)
    se_to_dict_checked = 0
    se_to_dict_unchecked = repeat('se_obj.as_dict()', globals=globals(), number=n, repeat=r)

    msg = '{}(checked={}) with {}x{} executions : Marshall min={}s max={}s / Serializer min={}s max={}s'

    print(msg.format(
        'from_dict',
        True,
        r,
        n,
        round(min(ma_from_dict_checked), digits),
        round(max(ma_from_dict_checked), digits),
        round(min(se_from_dict_checked), digits),
        round(max(se_from_dict_checked), digits),
    ))
    print(msg.format(
        'from_dict',
        False,
        r,
        n,
        round(min(ma_from_dict_unchecked), digits),
        round(max(ma_from_dict_unchecked), digits),
        round(min(se_from_dict_unchecked), digits),
        round(max(se_from_dict_unchecked), digits),
    ))
    print(msg.format(
        'as_dict',
        True,
        r,
        n,
        round(min(ma_to_dict_checked), digits),
        round(max(ma_to_dict_checked), digits),
        se_to_dict_checked,
        se_to_dict_checked,
    ))
    print(msg.format(
        'as_dict',
        False,
        r,
        n,
        round(min(ma_to_dict_unchecked), digits),
        round(max(ma_to_dict_unchecked), digits),
        round(min(se_to_dict_unchecked), digits),
        round(max(se_to_dict_unchecked), digits),
    ))

    pprint(ma_obj.as_dict(), width=150, depth=3, compact=True)
    pprint(se_obj.as_dict(), width=150, depth=3, compact=True)
