import zlib
import gzip
import bz2
import lzma
from timeit import repeat

from proxilly.models import Output
from peewee import SqliteDatabase


def compress(data, num=10, rep=5, level=1):
    pass


if __name__ == '__main__':
    r = 5
    n = 10

    db = SqliteDatabase('/home/tobiasse/Documents/projects/proxilly/instance/eucalc.sqlite')
    db.connect()
    Output.bind(db)

    outputs = Output.get(pathway=1)
    json = outputs.outputs.encode()
    bz2_json = bz2.compress(json, compresslevel=1)
    zlib_json = zlib.compress(json, level=1)
    gzip_json = gzip.compress(json, compresslevel=1)

    bz2_compress = repeat(stmt='bz2.compress(json, compresslevel=1)', repeat=r, number=n, globals=globals())
    bz2_decompress = repeat(stmt='bz2.decompress(bz2_json)', repeat=r, number=n, globals=globals())
    zlib_compress = repeat(stmt='zlib.compress(json, level=1)', repeat=r, number=n, globals=globals())
    zlib_decompress = repeat(stmt='zlib.decompress(zlib_json)', repeat=r, number=n, globals=globals())
    gzip_compress = repeat(stmt='gzip.compress(json, level=1)', repeat=r, number=n, globals=globals())
    gzip_decompress = repeat(stmt='gzip.decompress(gzip_json)', repeat=r, number=n, globals=globals())

    print(bz2_compress)
    print(bz2_decompress)
    print(zlib_compress)
    print(zlib_decompress)
