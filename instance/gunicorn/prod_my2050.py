from multiprocessing import cpu_count
from os import getcwd
from os.path import join

# See http://docs.gunicorn.org/en/stable/

# logging
log_path = join(getcwd(), 'instance', 'log')

accesslog = join(log_path, 'gunicorn_access.log')
access_log_format = '%(t)s %({x-forwarded-for}i)s "%(r)s" %(s)s %(b)s %(L)s'

errorlog = join(log_path, 'gunicorn_error.log')

# process naming
proc_name = 'my2050'

# security
limit_request_fields = 20

# server mechanics
daemon = False  # systemd will daemonize it!

# server socket
bind = '127.0.0.1:5051'
backlog = 128  # Check and increase net.core.somaxconn, otherwise increasing this parameter has no effect

# worker processes
workerclass = 'sync'
workers = 8  # 2 * cpu_count()
threads = 1
timeout = 320
graceful_timeout = 30
