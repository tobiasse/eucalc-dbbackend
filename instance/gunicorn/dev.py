from multiprocessing import cpu_count

# See http://docs.gunicorn.org/en/stable/

# logging
accesslog = '-'
access_log_format = '%(t)s %({x-forwarded-for}i)s "%(r)s" %(s)s %(b)s %(L)s'

errorlog = '-'

loglevel = 'debug'

# process naming
proc_name = 'proxilly'

# security
limit_request_fields = 20

# server mechanics
daemon = False  # systemd will daemonize it!

# server socket
bind = '127.0.0.1:5050'
backlog = 128  # Check and increase net.core.somaxconn, otherwise increasing this parameter has no effect

# worker processes
worker_class = 'sync'
workers = 2 * cpu_count()
threads = 1
timeout = 320
graceful_timeout = 30
