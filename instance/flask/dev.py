# See http://flask.palletsprojects.com/en/1.1.x/

# Flask config
ENV = 'development'
DEBUG = True
TESTING = True
JSON_SORT_KEYS = True
JSONIFY_PRETTYPRINT_REGULAR = True

# Database config
DB = "proxilly.sqlite"
PRAGMAS = {
    "journal_mode": "wal",
    "cache_size": 25000,  # pages, or 1 ~GB
    "foreign_keys": 1,

}

# Proxilly route
PROXILLY_URL = 'http://127.0.0.1:5050/results'

# Model backend config
MODEL_URL = 'http://127.0.0.1:5001/api/v1.0/results'

# Both in seconds
CONN_TIMEOUT = 1  # IMPORTANT, controls how fast we can return a busy status if the model API is already serving
READ_TIMEOUT = 300  # IMPORTANT, controls how long the model API has time to respond

# Security
MAX_CONTENT_LENGTH = 81920  # In bytes or 81KB (size of request with all outputs approx. 60KB)
