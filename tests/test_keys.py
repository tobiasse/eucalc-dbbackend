"""
test_keys
*********

:Author: Tobias Seydewitz
:Date: 19.11.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
import re
from unittest import TestCase

from proxilly.marshalling.keys import GenericKey
from proxilly.marshalling.keys import StringKey
from proxilly.marshalling.keys import ValidationError
from proxilly.marshalling.marshall import Marshall


class Foo(Marshall):
    def __init__(self):
        self.foo = 'foo'


class TestStringKey(TestCase):
    def test_successful_init_if_argument_is_string(self):
        key = StringKey('foo')
        self.assertTrue(isinstance(key, StringKey))

    def test_error_raised_if_init_argument_is_not_a_string(self):
        with self.assertRaises(ValidationError):
            StringKey(1)

    def test_get_from_dict_with_key_exists(self):
        key = StringKey('foo')

        expected = ('foo', 'bar')
        actual = key.get_from_dict({'foo': 'bar'})

        self.assertEqual(expected, actual)

    def test_get_from_object_returns_key_value_pair_if_key_exists(self):
        key = StringKey('foo')

        expected = 'foo'
        key, actual = key.get_from_object(Foo())

        self.assertEqual(expected, actual)

    def test_get_from_object_raises_error_if_key_not_exists(self):
        key = StringKey('bar')

        with self.assertRaises(ValidationError):
            key.get_from_object(Foo())

    def test_name_property(self):
        expected = 'foo'

        key = StringKey(expected)
        actual = key.name

        self.assertEqual(expected, actual)


class TestGenericKey(TestCase):
    def test_init_raises_error_if_regex_arg_is_not_a_regex(self):
        with self.assertRaises(ValidationError):
            GenericKey(regex='')

    def test_init_successful_if_arg_is_regex(self):
        key = GenericKey(regex=re.compile(r''))

        self.assertTrue(isinstance(key, GenericKey))

    def test_name_property_raises_error_if_name_is_not_initialized(self):
        key = GenericKey(regex=re.compile(r''))

        with self.assertRaises(ValidationError):
            name = key.name

    def test_get_from_dict_raises_error_if_no_key_found_matching_pattern(self):
        key = GenericKey(regex=re.compile(r'^[A-Z]{2}$'))

        with self.assertRaises(ValidationError):
            key.get_from_dict({'foo': 'foo', 'bar': 'bar'})

    def test_get_from_dict_returns_key_value_pair_if_matching_key_found(self):
        key = GenericKey(regex=re.compile(r'^[A-Z]{2}$'))

        expected = 'EU', 0
        actual = key.get_from_dict({'foo': 'foo', 'EU': 0})

        self.assertEqual(expected, actual)

    def test_get_from_dict_sets_name_if_matching_key_found(self):
        key = GenericKey(regex=re.compile(r'^[A-Z]{2}$'))
        key.get_from_dict({'foo': 'foo', 'EU': 0})

        self.assertTrue(key.name == 'EU')

    def test_get_from_object_raises_error_if_no_attribute_found_matching_pattern(self):
        key = GenericKey(regex=re.compile(r'^[A-Z]{2}$'))

        with self.assertRaises(ValidationError):
            key.get_from_object(Foo())

    def test_get_from_object_returns_attribute_name_value_pair_if_matching_attribute_found(self):
        key = GenericKey(regex=re.compile(r'^[A-Za-z]{3}$'))

        expected = 'foo', 'foo'
        actual = key.get_from_object(Foo())

        self.assertEqual(expected, actual)
