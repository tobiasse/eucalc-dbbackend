"""
test_mixins
***********

:Author: Tobias Seydewitz
:Date: 19.09.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from unittest import TestCase
from unittest.mock import mock_open
from unittest.mock import patch

from proxilly.utils.mixins import FromFile
from proxilly.utils.errors import EmptyFileError


class TestFromFile(TestCase):
    @patch('builtins.open', new_callable=mock_open, read_data='test1\ntest2\ntest3\n')
    def test_read_file(self, m):
        expected = ['test1', 'test2', 'test3']
        actual = FromFile.read_file('path')

        m.assert_called_once_with('path')
        self.assertListEqual(expected, actual)

    @patch('builtins.open', new_callable=mock_open, read_data='')
    def test_read_empty_file(self, m):
        with self.assertRaises(EmptyFileError):
            FromFile.read_file('path')

    @patch('builtins.open', new_callable=mock_open, read_data='\t\n  test1\n\n test2 \ntest3   \n\n\n')
    def test_read_file_with_whitespace(self, m):
        expected = ['test1', 'test2', 'test3']
        actual = FromFile.read_file('path')

        self.assertListEqual(expected, actual)
