"""
test_fields
***********

:Author: Tobias Seydewitz
:Date: 21.11.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
import re
from unittest import TestCase

from proxilly.marshalling.errors import ValidationError
from proxilly.marshalling.fields import Bool
from proxilly.marshalling.fields import Dict
from proxilly.marshalling.fields import Float
from proxilly.marshalling.fields import Int
from proxilly.marshalling.fields import List
from proxilly.marshalling.fields import Num
from proxilly.marshalling.fields import Str
from proxilly.marshalling.fields import _Field
from proxilly.marshalling.keys import GenericKey
from proxilly.marshalling.keys import StringKey
from proxilly.marshalling.keys import _Key
from proxilly.marshalling.marshall import Marshall


class Foo(Marshall):
    def __init__(self):
        self.foo = 'foo'


class TestField(TestCase):
    def test_error_raised_if_not_required_missing_none_nullable_false_passed(self):
        with self.assertRaises(ValidationError):
            field = _Field(required=False, nullable=False, missing=None)

    def test_error_raised_if_uncallable_validator_passed(self):
        with self.assertRaises(ValidationError):
            field = _Field(validator=1)

    def test_error_raised_if_uncallable_validator_sequence_passed(self):
        with self.assertRaises(ValidationError):
            field = _Field(validator=[1, ])

    def test_error_raised_if_uncallable_modifier_passed(self):
        with self.assertRaises(ValidationError):
            field = _Field(modifier=1)

    def test_error_raised_if_uncallable_modifier_sequence_passed(self):
        with self.assertRaises(ValidationError):
            field = _Field(modifier=[1, ])

    def test_error_raised_if_key_is_not_none_or_instance_of_key(self):
        with self.assertRaises(ValidationError):
            field = _Field(key=1)

    def test_deserialize_raises_error_if_field_is_missing_and_required(self):
        field = _Field(key='foo')

        with self.assertRaises(ValidationError):
            field._deserialize({})

    def test_deserialize_raises_error_if_field_is_not_root(self):
        field = _Field(key='foo')

        with self.assertRaises(ValidationError):
            field.deserialize({})

    def test_deserialize_raises_error_if_arg_is_not_a_dict(self):
        field = _Field(key='foo')

        with self.assertRaises(ValidationError):
            field.deserialize([])

    def test_serialize_raises_error_if_field_is_missing_and_required(self):
        field = _Field(key='bar')

        with self.assertRaises(ValidationError):
            field._serialize(Foo())

    def test_serialize_raises_if_field_is_not_root(self):
        field = _Field(key='foo')

        with self.assertRaises(ValidationError):
            field.serialize(Foo())

    def test_parse_raises_errror_if_field_is_not_root(self):
        field = _Field(key='foo')

        with self.assertRaises(ValidationError):
            field.parse({})

    def test_parse_raises_error_if_arg_is_not_a_dict(self):
        field = _Field(key='foo')

        with self.assertRaises(ValidationError):
            field.parse([])

    def test_successful_init_if_not_required_missing_none_nullable_true_passed(self):
        self.assertTrue(isinstance(_Field(required=False, nullable=True, missing=None), _Field))

    def test_successful_init_if_callable_validator_passed(self):
        self.assertTrue(isinstance(_Field(validator=int), _Field))

    def test_successful_init_if_callable_validator_sequence_passed(self):
        self.assertTrue(isinstance(_Field(validator=[int, ]), _Field))

    def test_successful_init_if_callable_modifier_passed(self):
        self.assertTrue(isinstance(_Field(modifier=int), _Field))

    def test_successful_init_if_callable_modifier_sequence_passed(self):
        self.assertTrue(isinstance(_Field(modifier=[int, ]), _Field))

    def test_successful_init_if_key_is_none_or_instance_of_field(self):
        self.assertTrue(isinstance(_Field(key=None), _Field))
        self.assertTrue(isinstance(_Field(key='foo'), _Field))

    def test_in_dict_returns_false_if_key_is_none(self):
        field = _Field()
        self.assertFalse(field.in_dict)

    def test_in_dict_returns_true_if_key_is_StringKey(self):
        field = _Field(key=StringKey('foo'))
        self.assertTrue(field.in_dict)

    def test_in_dict_returns_true_if_key_is_string(self):
        field = _Field(key='foo')
        self.assertTrue(field.in_dict)

    def test_successful_init_if_key_is_string(self):
        field = _Field(key='foo')
        self.assertTrue(isinstance(field.key, _Key))

    def test_successful_init_if_key_is_regex(self):
        field = _Field(key=re.compile(r''))

        self.assertTrue(isinstance(field.key, GenericKey))

    def test_if_set_root_switches_state(self):
        field = _Field()
        field._set_root()

        self.assertFalse(field._root)

    def test_is_root_returns_false_if_field_has_key(self):
        field = _Field(key='foo')
        self.assertFalse(field.is_root())

    def test_is_root_returns_false_if_field_is_not_root(self):
        field = _Field()
        field._set_root()
        self.assertFalse(field.is_root())

    def test_is_root_returns_true_if_field_has_no_key_and_root_is_true(self):
        field = _Field()
        self.assertTrue(field.is_root())

    def test_apply_validator_append_error_if_validation_fails(self):
        field = _Field(validator=lambda x: x > 0)
        field._apply_validator(0, 0)

        self.assertTrue(len(field._errors) == 1)

    def test_deserialize_returns_none_if_field_is_missing_and_not_required(self):
        field = _Field(key='foo', required=False, nullable=True)

        expected = (None, None)
        actual = field._deserialize({})

        self.assertEqual(expected, actual)

    def test_deserialize_returns_key_value_if_field_exists(self):
        field = _Field(key='foo')

        expected = ('foo', 'bar')
        actual = field._deserialize({'foo': 'bar'})

        self.assertEqual(expected, actual)

    def test_serialize_returns_none_if_field_is_missing_and_not_required(self):
        field = _Field(key='bar', required=False, nullable=True)

        expected = (None, None)
        actual = field._serialize(Foo())

        self.assertEqual(expected, actual)

    def test_serialize_returns_key_value_pair_if_field_exists(self):
        field = _Field(key='foo')

        expected = ('foo', 'foo')
        actual = field._serialize(Foo())

        self.assertEqual(expected, actual)

    def test_if_field_is_flat(self):
        field = _Field()
        self.assertFalse(field.flat)

    def test_repr_returns_str(self):
        field = _Field()
        self.assertTrue(isinstance(field.__repr__(), str))

    def test_str_returns_str(self):
        field = _Field()
        self.assertTrue(isinstance(field.__str__(), str))


class TestString(TestCase):
    def test_error_raised_if_required_and_missing_not_none_passed(self):
        with self.assertRaises(ValidationError):
            string = Str(required=True, missing='')

    def test_error_raised_if_not_required_and_missing_wrong_type_passed(self):
        with self.assertRaises(ValidationError):
            string = Str(required=False, missing=1)

    def test_validate_type_false_if_non_string_passed(self):
        string = Str()
        self.assertFalse(string._validate_type(0))

    def test_validate_type_false_if_none_passed(self):
        string = Str()
        self.assertFalse(string._validate_type(None))

    def test_validate_type_true_if_string_passed(self):
        string = Str()
        self.assertTrue(string._validate_type(''))

    def test_validate_type_true_if_none_passed_and_nullable_true(self):
        string = Str(nullable=True)
        self.assertTrue(string._validate_type(None))

    def test_parse_from_list_raise_error_if_type_missmatch(self):
        string = Str()

        with self.assertRaises(ValidationError):
            string._parse_from_list(0, 0)

    def test_parse_from_list_raise_error_if_validation_fails(self):
        string = Str(validator=lambda x: x == 'foo')

        with self.assertRaises(ValidationError):
            string._parse_from_list(0, 'bar')

    def test_parse_from_list_success(self):
        string = Str()

        expected = 0, ''
        actual = string._parse_from_list(*expected)

        self.assertEqual(expected, actual)

    def test_parse_from_dict_raise_error_if_field_missing(self):
        string = Str(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            string._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_type_missmatch(self):
        string = Str(key='foo')
        struct = {'foo': 0}

        with self.assertRaises(ValidationError):
            string._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_validation_fails(self):
        string = Str(key='foo', validator=lambda x: x == 'bar')
        struct = {'foo': 'foo'}

        with self.assertRaises(ValidationError):
            string._parse_from_dict(struct)

    def test_parse_from_dict_success(self):
        string = Str(key='foo')
        struct = {'foo': 'bar'}

        key, val = string._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, 'bar')

    def test_parse_from_dict_raise_error_if_required_but_missing(self):
        string = Str(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            string._parse_from_dict(struct)

    def test_parse_from_dict_ignores_field_if_not_required(self):
        string = Str(key='foo', required=False, nullable=True)
        struct = {}

        key, val = string._parse_from_dict(struct)
        self.assertEqual(key, None)
        self.assertEqual(val, None)

    def test_parse_from_dict_adds_field_if_not_required_and_store_missing_true(self):
        string = Str(key='foo', required=False, store_missing=True, missing='bar')
        struct = {}

        key, val = string._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, 'bar')

    def test_if_modifier_modifies_string(self):
        string = Str(modifier=lambda x: x.lower())

        expected = 'foo'
        idx, actual = string._parse_from_list(0, 'FOO')

        self.assertEqual(expected, actual)


class TestInt(TestCase):
    def test_error_raised_if_required_and_missing_not_none_passed(self):
        with self.assertRaises(ValidationError):
            integer = Int(required=True, missing='')

    def test_error_raised_if_not_required_and_missing_wrong_type_passed(self):
        with self.assertRaises(ValidationError):
            integer = Int(required=False, missing='')

    def test_validate_type_false_if_non_integer_passed(self):
        integer = Int()
        self.assertFalse(integer._validate_type(''))

    def test_validate_type_false_if_none_passed(self):
        integer = Int()
        self.assertFalse(integer._validate_type(None))

    def test_validate_type_true_if_integer_passed(self):
        integer = Int()
        self.assertTrue(integer._validate_type(0))

    def test_validate_type_true_if_none_passed_and_nullable_true(self):
        integer = Int(nullable=True)
        self.assertTrue(integer._validate_type(None))

    def test_parse_from_list_raise_error_if_type_missmatch(self):
        integer = Int()

        with self.assertRaises(ValidationError):
            integer._parse_from_list(0, '')

    def test_parse_from_list_raise_error_if_validation_fails(self):
        integer = Int(validator=lambda x: x > 0)

        with self.assertRaises(ValidationError):
            integer._parse_from_list(0, 0)

    def test_parse_from_list_success(self):
        integer = Int()

        expected = 0, 0
        actual = integer._parse_from_list(*expected)

        self.assertEqual(expected, actual)

    def test_parse_from_dict_raise_error_if_field_missing(self):
        integer = Int(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            integer._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_type_missmatch(self):
        integer = Int(key='foo')
        struct = {'foo': ''}

        with self.assertRaises(ValidationError):
            integer._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_validation_fails(self):
        integer = Int(key='foo', validator=lambda x: x > 0)
        struct = {'foo': 0}

        with self.assertRaises(ValidationError):
            integer._parse_from_dict(struct)

    def test_parse_from_dict_success(self):
        integer = Int(key='foo')
        struct = {'foo': 0}

        key, val = integer._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, 0)

    def test_parse_from_dict_raise_error_if_required_but_missing(self):
        integer = Int(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            integer._parse_from_dict(struct)

    def test_parse_from_dict_ignores_field_if_not_required(self):
        integer = Int(key='foo', required=False, nullable=True)
        struct = {}

        key, val = integer._parse_from_dict(struct)
        self.assertEqual(key, None)
        self.assertEqual(val, None)

    def test_parse_from_dict_adds_field_if_not_required_and_store_missing_true(self):
        integer = Int(key='foo', required=False, store_missing=True, missing=0)
        struct = {}

        key, val = integer._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, 0)

    def test_if_modifier_modifies_integer(self):
        integer = Int(modifier=lambda x: x**2)

        expected = 4
        idx, actual = integer._parse_from_list(0, 2)

        self.assertEqual(expected, actual)


class TestFloat(TestCase):
    def test_error_raised_if_required_and_missing_not_none_passed(self):
        with self.assertRaises(ValidationError):
            float = Float(required=True, missing='')

    def test_error_raised_if_not_required_and_missing_wrong_type_passed(self):
        with self.assertRaises(ValidationError):
            float = Float(required=False, missing='')

    def test_validate_type_false_if_non_float_passed(self):
        float = Float()
        self.assertFalse(float._validate_type(''))

    def test_validate_type_false_if_none_passed(self):
        float = Float()
        self.assertFalse(float._validate_type(None))

    def test_validate_type_true_if_float_passed(self):
        float = Float()
        self.assertTrue(float._validate_type(0.))

    def test_validate_type_true_if_none_passed_and_nullable_true(self):
        float = Float(nullable=True)
        self.assertTrue(float._validate_type(None))

    def test_parse_from_list_raise_error_if_type_missmatch(self):
        float = Float()

        with self.assertRaises(ValidationError):
            float._parse_from_list(0, '')

    def test_parse_from_list_raise_error_if_validation_fails(self):
        float = Float(validator=lambda x: x > 0.)

        with self.assertRaises(ValidationError):
            float._parse_from_list(0, 0.)

    def test_parse_from_list_success(self):
        float = Float()

        expected = 0, 0.
        actual = float._parse_from_list(*expected)

        self.assertEqual(expected, actual)

    def test_parse_from_dict_raise_error_if_field_missing(self):
        float = Float(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            float._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_type_missmatch(self):
        float = Float(key='foo')
        struct = {'foo': ''}

        with self.assertRaises(ValidationError):
            float._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_validation_fails(self):
        float = Float(key='foo', validator=lambda x: x > 0.)
        struct = {'foo': 0.}

        with self.assertRaises(ValidationError):
            float._parse_from_dict(struct)

    def test_parse_from_dict_success(self):
        float = Float(key='foo')
        struct = {'foo': 0.}

        key, val = float._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, 0.)

    def test_parse_from_dict_raise_error_if_required_but_missing(self):
        float = Float(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            float._parse_from_dict(struct)

    def test_parse_from_dict_ignores_field_if_not_required(self):
        float = Float(key='foo', required=False, nullable=True)
        struct = {}

        key, val = float._parse_from_dict(struct)
        self.assertEqual(key, None)
        self.assertEqual(val, None)

    def test_parse_from_dict_adds_field_if_not_required_and_store_missing_true(self):
        float = Float(key='foo', required=False, store_missing=True, missing=0.)
        struct = {}

        key, val = float._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, 0.)

    def test_if_modifier_modifies_float(self):
        float = Float(modifier=lambda x: x**2)

        expected = 4.
        idx, actual = float._parse_from_list(0, 2.)

        self.assertEqual(expected, actual)


class TestNum(TestCase):
    def test_error_raised_if_required_and_missing_not_none_passed(self):
        with self.assertRaises(ValidationError):
            number = Num(required=True, missing='')

    def test_error_raised_if_not_required_and_missing_wrong_type_passed(self):
        with self.assertRaises(ValidationError):
            number = Num(required=False, missing='')

    def test_validate_type_false_if_non_float_passed(self):
        number = Num()
        self.assertFalse(number._validate_type(''))

    def test_validate_type_false_if_none_passed(self):
        number = Num()
        self.assertFalse(number._validate_type(None))

    def test_validate_type_true_if_float_passed(self):
        number = Num()
        self.assertTrue(number._validate_type(0.))

    def test_validate_type_true_if_none_passed_and_nullable_true(self):
        number = Num(nullable=True)
        self.assertTrue(number._validate_type(None))

    def test_parse_from_list_raise_error_if_type_missmatch(self):
        number = Num()

        with self.assertRaises(ValidationError):
            number._parse_from_list(0, '')

    def test_parse_from_list_raise_error_if_validation_fails(self):
        number = Num(validator=lambda x: x > 0.)

        with self.assertRaises(ValidationError):
            number._parse_from_list(0, 0.)

    def test_parse_from_list_success(self):
        number = Num()

        expected = 0, 0.
        actual = number._parse_from_list(*expected)

        self.assertEqual(expected, actual)

    def test_parse_from_dict_raise_error_if_field_missing(self):
        number = Num(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            number._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_type_missmatch(self):
        number = Num(key='foo')
        struct = {'foo': ''}

        with self.assertRaises(ValidationError):
            number._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_validation_fails(self):
        number = Num(key='foo', validator=lambda x: x > 0.)
        struct = {'foo': 0.}

        with self.assertRaises(ValidationError):
            number._parse_from_dict(struct)

    def test_parse_from_dict_success(self):
        number = Num(key='foo')
        struct = {'foo': 0.}

        key, val = number._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, 0.)

    def test_parse_from_dict_raise_error_if_required_but_missing(self):
        number = Num(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            number._parse_from_dict(struct)

    def test_parse_from_dict_ignores_field_if_not_required(self):
        number = Num(key='foo', required=False, nullable=True)
        struct = {}

        key, val = number._parse_from_dict(struct)
        self.assertEqual(key, None)
        self.assertEqual(val, None)

    def test_parse_from_dict_adds_field_if_not_required_and_store_missing_true(self):
        number = Num(key='foo', required=False, store_missing=True, missing=0.)
        struct = {}

        key, val = number._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, 0.)

    def test_if_modifier_modifies_number(self):
        number = Num(modifier=lambda x: x**2)

        expected = 4.
        idx, actual = number._parse_from_list(0, 2.)

        self.assertEqual(expected, actual)


class TestBool(TestCase):
    def test_error_raised_if_required_and_missing_not_none_passed(self):
        with self.assertRaises(ValidationError):
            boolean = Bool(required=True, missing='')

    def test_error_raised_if_not_required_and_missing_wrong_type_passed(self):
        with self.assertRaises(ValidationError):
            boolean = Bool(required=False, missing='')

    def test_validate_type_false_if_non_boolean_passed(self):
        boolean = Bool()
        self.assertFalse(boolean._validate_type(''))

    def test_validate_type_false_if_none_passed(self):
        boolean = Bool()
        self.assertFalse(boolean._validate_type(None))

    def test_validate_type_true_if_boolean_passed(self):
        boolean = Bool()
        self.assertTrue(boolean._validate_type(True))

    def test_validate_type_true_if_none_passed_and_nullable_true(self):
        boolean = Bool(nullable=True)
        self.assertTrue(boolean._validate_type(None))

    def test_parse_from_list_raise_error_if_type_missmatch(self):
        boolean = Bool()

        with self.assertRaises(ValidationError):
            boolean._parse_from_list(0, '')

    def test_parse_from_list_raise_error_if_validation_fails(self):
        boolean = Bool(validator=lambda x: x is True)

        with self.assertRaises(ValidationError):
            boolean._parse_from_list(0, False)

    def test_parse_from_list_success(self):
        boolean = Bool()

        expected = 0, True
        actual = boolean._parse_from_list(*expected)

        self.assertEqual(expected, actual)

    def test_parse_from_dict_raise_error_if_field_missing(self):
        boolean = Bool(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            boolean._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_type_missmatch(self):
        boolean = Bool(key='foo')
        struct = {'foo': ''}

        with self.assertRaises(ValidationError):
            boolean._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_validation_fails(self):
        boolean = Bool(key='foo', validator=lambda x: x is True)
        struct = {'foo': False}

        with self.assertRaises(ValidationError):
            boolean._parse_from_dict(struct)

    def test_parse_from_dict_success(self):
        boolean = Bool(key='foo')
        struct = {'foo': True}

        key, val = boolean._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, True)

    def test_parse_from_dict_raise_error_if_required_but_missing(self):
        boolean = Bool(key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            boolean._parse_from_dict(struct)

    def test_parse_from_dict_ignores_field_if_not_required(self):
        boolean = Bool(key='foo', required=False, nullable=True)
        struct = {}

        key, val = boolean._parse_from_dict(struct)
        self.assertEqual(key, None)
        self.assertEqual(val, None)

    def test_parse_from_dict_adds_field_if_not_required_and_store_missing_true(self):
        boolean = Bool(key='foo', required=False, store_missing=True, missing=True)
        struct = {}

        key, val = boolean._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, True)

    def test_if_modifier_modifies_boolean(self):
        boolean = Bool(modifier=lambda x: not x)

        expected = True
        idx, actual = boolean._parse_from_list(0, False)

        self.assertEqual(expected, actual)


class TestList(TestCase):
    def test_error_raised_if_field_is_dict_and_flat_true(self):
        with self.assertRaises(ValidationError):
            list = List(field=Dict(flat=True))

    def test_error_raised_if_field_is_not_instance_of_field(self):
        with self.assertRaises(ValidationError):
            list = List(field='')

    def test_error_raised_if_field_has_key(self):
        with self.assertRaises(ValidationError):
            list = List(field=Bool(key='foo'))

    def test_error_raised_if_list_elements_have_wrong_type(self):
        list = List(field=Bool())

        with self.assertRaises(ValidationError):
            list._parse_from_list(0, [1, 2, 3])

    def test_if_success_if_lists_are_nested(self):
        list = List(field=List(field=List(field=Int())))

        expected = [[[1, 2, 3], [1, 2, 3]], [[1, 2, 3], [1, 2, 3]]]
        idx, val = list._parse_from_list(0, expected)

        self.assertListEqual(expected, val)

    def test_error_raised_if_required_and_missing_not_none_passed(self):
        with self.assertRaises(ValidationError):
            list = List(field=Bool(), required=True, missing='')

    def test_error_raised_if_not_required_and_missing_wrong_type_passed(self):
        with self.assertRaises(ValidationError):
            list = List(field=Bool(), required=False, missing='')

    def test_validate_type_false_if_non_list_passed(self):
        list = List(field=Bool())
        self.assertFalse(list._validate_type(''))

    def test_validate_type_false_if_none_passed(self):
        list = List(field=Bool())
        self.assertFalse(list._validate_type(None))

    def test_validate_type_true_if_list_passed(self):
        list = List(field=Bool())
        self.assertTrue(list._validate_type([]))

    def test_validate_type_true_if_none_passed_and_nullable_true(self):
        list = List(field=Bool(), nullable=True)
        self.assertTrue(list._validate_type(None))

    def test_parse_from_list_raise_error_if_type_missmatch(self):
        list = List(field=Bool())

        with self.assertRaises(ValidationError):
            list._parse_from_list(0, '')

    def test_parse_from_list_raise_error_if_validation_fails(self):
        list = List(field=Bool(), validator=lambda x: len(x) > 0)

        with self.assertRaises(ValidationError):
            list._parse_from_list(0, [])

    def test_parse_from_list_success(self):
        list = List(field=Bool())

        expected = 0, [True, False]
        actual = list._parse_from_list(*expected)

        self.assertEqual(expected, actual)

    def test_parse_from_dict_raise_error_if_field_missing(self):
        list = List(field=Bool(), key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            list._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_type_missmatch(self):
        list = List(field=Bool(), key='foo')
        struct = {'foo': ''}

        with self.assertRaises(ValidationError):
            list._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_validation_fails(self):
        list = List(field=Bool(), key='foo', validator=lambda x: len(x) > 0)
        struct = {'foo': []}

        with self.assertRaises(ValidationError):
            list._parse_from_dict(struct)

    def test_parse_from_dict_success(self):
        list = List(field=Bool(), key='foo')
        struct = {'foo': []}

        key, val = list._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, [])

    def test_parse_from_dict_raise_error_if_required_but_missing(self):
        list = List(field=Bool(), key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            list._parse_from_dict(struct)

    def test_parse_from_dict_ignores_field_if_not_required(self):
        list = List(field=Bool(), key='foo', required=False, nullable=True)
        struct = {}

        key, val = list._parse_from_dict(struct)
        self.assertEqual(key, None)
        self.assertEqual(val, None)

    def test_parse_from_dict_adds_field_if_not_required_and_store_missing_true(self):
        list = List(field=Bool(), key='foo', required=False, store_missing=True, missing=[True, True, True])
        struct = {}

        key, val = list._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertListEqual(val, [True, True, True])

    def test_if_modifier_modifies_list(self):
        list = List(field=Int(), modifier=lambda x: x[:2])

        expected = [0]*2
        idx, actual = list._parse_from_list(0, [0]*4)

        self.assertListEqual(expected, actual)

    def test_if_list_elements_are_modified_and_set(self):
        list = List(field=Int(modifier=lambda x: 0))

        expected = [0]*4
        idx, actual = list._parse_from_list(0, [1]*4)

        self.assertListEqual(expected, actual)


class TestDict(TestCase):
    def test_error_raised_if_duplicate_keys_found(self):
        with self.assertRaises(ValidationError):
            dictionary = Dict(fields=[Str(key='name'), Str(key='name')])

    def test_if_dict_set_root_to_false_on_child_fields(self):
        string = Str(key='foo')
        integer = Int(key='bar')
        dictionary = Dict(fields=[string, integer])

        self.assertFalse(string.is_root())
        self.assertFalse(integer.is_root())
        self.assertTrue(dictionary.is_root())

    def test_if_set_flat_on_dict_returns_true(self):
        dictionary = Dict(flat=True)
        self.assertTrue(dictionary.flat)

    def test_error_raised_if_field_is_not_instance_of_field(self):
        with self.assertRaises(ValidationError):
            dictionary = Dict(fields='')

    def test_error_raised_if_sequence_of_fields_is_not_instance_of_field(self):
        with self.assertRaises(ValidationError):
            dictionary = Dict(fields=[Bool(), ''])

    def test_instance_created_if_fields_is_none(self):
        dictionary = Dict()
        self.assertTrue(isinstance(dictionary, Dict))

    def test_error_raised_if_field_has_not_a_key(self):
        with self.assertRaises(ValidationError):
            dictionary = Dict(fields=Bool())

    def test_error_raised_if_dict_elements_have_wrong_type(self):
        dictionary = Dict(fields=Bool(key='foo'))
        struct = {'foo': 'bar'}

        with self.assertRaises(ValidationError):
            dictionary._parse_from_list(0, struct)

    def test_if_success_if_dictionaries_are_nested(self):
        dictionary = Dict(
            fields=Dict(
                fields=Dict(
                    fields=Bool(key='foo'),
                    key='foo'
                ),
                key='foo'
            )
        )

        expected = {'foo': {'foo': {'foo': True}}}
        idx, val = dictionary._parse_from_list(0, expected)

        self.assertDictEqual(expected, val)

    def test_error_raised_if_required_and_missing_not_none_passed(self):
        with self.assertRaises(ValidationError):
            dictionary = Dict(required=True, missing='')

    def test_error_raised_if_not_required_and_missing_wrong_type_passed(self):
        with self.assertRaises(ValidationError):
            dictionary = Dict(required=False, missing='')

    def test_validate_type_false_if_non_dict_passed(self):
        dictionary = Dict()
        self.assertFalse(dictionary._validate_type(''))

    def test_validate_type_false_if_none_passed(self):
        dictionary = Dict()
        self.assertFalse(dictionary._validate_type(None))

    def test_validate_type_true_if_dict_passed(self):
        dictionary = Dict()
        self.assertTrue(dictionary._validate_type({}))

    def test_validate_type_true_if_none_passed_and_nullable_true(self):
        dictionary = Dict(nullable=True)
        self.assertTrue(dictionary._validate_type(None))

    def test_parse_from_list_raise_error_if_type_missmatch(self):
        dictionary = Dict()

        with self.assertRaises(ValidationError):
            dictionary._parse_from_list(0, '')

    def test_parse_from_list_raise_error_if_validation_fails(self):
        dictionary = Dict(validator=lambda x: len(x) > 0)

        with self.assertRaises(ValidationError):
            dictionary._parse_from_list(0, {})

    def test_parse_from_list_success(self):
        dictionary = Dict(fields=Bool(key='foo'))

        expected = 0, {'foo': True}
        actual = dictionary._parse_from_list(*expected)

        self.assertEqual(expected, actual)

    def test_parse_from_dict_raise_error_if_field_missing(self):
        dictionary = Dict(fields=Bool(key='foo'), key='foo')
        struct = {}

        with self.assertRaises(ValidationError):
            dictionary._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_type_missmatch(self):
        dictionary = Dict(fields=Bool(key='foo'), key='foo')
        struct = {'foo': ''}

        with self.assertRaises(ValidationError):
            dictionary._parse_from_dict(struct)

    def test_parse_from_dict_raise_error_if_validation_fails(self):
        dictionary = Dict(fields=Bool(key='foo'), key='foo', validator=lambda x: len(x) > 0)
        struct = {'foo': {}}

        with self.assertRaises(ValidationError):
            dictionary._parse_from_dict(struct)

    def test_parse_from_dict_success(self):
        dictionary = Dict(key='foo')
        struct = {'foo': {}}

        key, val = dictionary._parse_from_dict(struct)
        self.assertEqual(key, 'foo')
        self.assertEqual(val, {})

    def test_deserialize_returns_root_dict_if_root_field(self):
        dictionary = Dict()

        expected = 'root', {}
        actual = dictionary._deserialize({})

        self.assertEqual(expected, actual)

    def test_deserialize_returns_none_none_if_nested_dictionary_is_not_required(self):
        child = Dict(key='foo', required=False, nullable=True)
        dictionary = Dict(fields=child)

        expected = None, None
        actual = child._deserialize({})

        self.assertEqual(expected, actual)
        self.assertEqual(('root', {}), dictionary._deserialize({}))

    def test_deserialize_returns_structure_if_nested_dictionaries_should_not_be_flattened(self):
        child1 = Dict(key='foo', fields=Bool(key='foo'))
        child2 = Dict(key='bar', fields=Bool(key='bar'))
        dictionary = Dict(fields=[child1, child2])

        expected = {'foo': {'foo': True}, 'bar': {'bar': False}}
        actual = dictionary.deserialize(expected)

        self.assertDictEqual(expected, actual)

    def test_deserialize_returns_flattened_structure_if_nested_dictionaries_should_be_flat(self):
        child1 = Dict(key='foo', fields=Bool(key='foo'), flat=True)
        child2 = Dict(key='bar', fields=Bool(key='bar'), flat=True)
        dictionary = Dict(fields=[child1, child2])

        expected = {'foo': True, 'bar': False}
        actual = dictionary.deserialize({'foo': {'foo': True}, 'bar': {'bar': False}})

        self.assertDictEqual(expected, actual)

    def test_serialize_returns_root_dict_if_field_is_root(self):
        dictionary = Dict()

        expected = ('root', {})
        actual = dictionary._serialize(Foo())

        self.assertEqual(expected, actual)

    def test_serialize_returns_none_if_nested_dictionary_is_not_required_and_flat_is_false(self):
        child = Dict(key='bar', required=False, nullable=True)
        dictionary = Dict(fields=child)

        expected = (None, None)
        actual = child._serialize(Foo())

        self.assertEqual(expected, actual)
        self.assertEqual({}, dictionary.serialize(Foo()))

    def test_serialize_returns_dict_if_nested_dictionary_is_not_required_and_flat_is_true(self):
        child = Dict(key='bar', required=False, nullable=True, flat=True)
        dictionary = Dict(fields=child)

        expected = ('bar', {})
        actual = child._serialize(Foo())

        self.assertEqual(expected, actual)
        self.assertEqual({'bar': {}}, dictionary.serialize(Foo()))



