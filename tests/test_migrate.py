"""
test_migrate
************

:Author: Tobias Seydewitz
:Date: 06.10.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from unittest import TestCase
from unittest.mock import PropertyMock

from click.testing import CliRunner
from peewee import SqliteDatabase

from proxilly.cli.env import env
from proxilly.models import Lever
from proxilly.models import LeverGroup
from proxilly.models import LeverHeadline
from proxilly.models import Output
from proxilly.models import OutputID
from proxilly.models import Pathway
from tests import instance_path

db = SqliteDatabase(':memory:')
models = [Output, Pathway, OutputID, LeverHeadline, LeverGroup, Lever]


class TestCreate(TestCase):
    def setUp(self):
        db.bind(models)

        app = PropertyMock()
        app.config = {'DATABASE': db}
        app.instance_path = instance_path

        self.ctx = {
            'app': app,
            'models': models,
            'output': Output,
            'pathway': Pathway,
            'outputid': OutputID,
            'leverheadline': LeverHeadline,
            'levergroup': LeverGroup,
            'lever': Lever,
        }

    def test_create(self):
        runner = CliRunner()
        result = runner.invoke(env, ['migrate'], obj=self.ctx, catch_exceptions=False)

        self.assertTrue(result.exit_code == 0)
