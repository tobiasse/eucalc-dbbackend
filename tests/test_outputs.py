"""
test_outputs
************

:Author: Tobias Seydewitz
:Date: 13.01.20
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from os.path import join
from unittest import TestCase

from proxilly.utils.outputs import OutputsGenerator
from proxilly.utils.outputs import RandomizedOutputsGenerator
from tests import test_resources


class TestOutputsGenerator(TestCase):
    def test_get_all_countries_returns_true_if_all_countries_is_true(self):
        outputs = OutputsGenerator(['foo'], all_countries=True, size=0)
        self.assertTrue(outputs._get_all_countries('foo'))

    def test_get_all_countries_returns_false_if_all_countries_is_false(self):
        outputs = OutputsGenerator(['foo'], all_countries=False, size=0)
        self.assertFalse(outputs._get_all_countries('foo'))

    def test_get_all_countries_returns_true_if_output_is_in_list_of_accepted_items(self):
        outputs = OutputsGenerator(['foo'], all_countries=True, size=0)
        self.assertTrue(outputs._get_all_countries('foo'))

    def test_get_all_countries_returns_false_if_outputs_is_not_in_list_of_accepted_items(self):
        outputs = OutputsGenerator(['foo', 'bar'], all_countries=True, exclude=['bar'], size=0)
        self.assertFalse(outputs._get_all_countries('bar'))

    def test_get_all_countries_returns_false_if_outputs_is_in_exclude_list(self):
        outputs = OutputsGenerator(['foo', 'bar'], all_countries=True, exclude=['bar'], size=0)
        self.assertTrue(outputs._get_all_countries('foo'))
        self.assertFalse(outputs._get_all_countries('bar'))

    def test_get_returns_list_of_outputs(self):
        outputs = OutputsGenerator(['foo'], 0, True)

        expected = [{'id': 'foo', 'allCountries': True}]
        actual = outputs.get()

        self.assertListEqual(expected, actual)

    def test_iter_returns_list_of_outputs_equal_to_size(self):
        outputs = OutputsGenerator(['foo'], 2, True)
        self.assertEqual(2, len(list(outputs)))

    def test_outputsgenerator_read_content_from_file(self):
        outputs = OutputsGenerator(outputs=join(test_resources, 'outputs.txt'), size=1, all_countries=True)
        with open(join(test_resources, 'outputs.txt')) as src:
            expected = len(src.read().split('\n'))

        self.assertEqual(expected, len(list(outputs)[0]))


class TestRandomizedOutputsGenerator(TestCase):
    def test_get_returns_list_of_outputs(self):
        outputs = RandomizedOutputsGenerator(['foo', 'bar', 'test', 'tests'], 0, True)

        actual = outputs.get()

        self.assertTrue(isinstance(actual, list))
        self.assertTrue(isinstance(actual[0], dict))
        self.assertTrue('id' in actual[0])

    def test_iter_returns_list_of_outputs_equal_to_size(self):
        outputs = RandomizedOutputsGenerator(['foo', 'bar', 'test', 'tests'], 2, True)
        self.assertEqual(2, len(list(outputs)))


