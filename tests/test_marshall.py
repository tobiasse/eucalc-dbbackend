"""
test_marshall
*************

:Author: Tobias Seydewitz
:Date: 06.10.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from unittest import TestCase
from unittest.mock import Mock

from proxilly.marshalling.errors import ValidationError
from proxilly.marshalling.marshall import Marshall


class Foo(Marshall):
    def __init__(self, **kwargs):
        pass


class TestMarshall(TestCase):
    def test_from_json_raises_validation_error_if_arg_is_not_json_conform_string(self):
        with self.assertRaises(ValidationError):
            Marshall.from_json('FOO')

    def test_from_json_calls_parse_if_checked_true(self):
        mock = Mock()
        mock.parse.return_value = {'foo': 'bar'}
        mock.deserialize.return_value = {'foo': 'bar'}

        Marshall.SCHEMA = mock
        Foo.from_json('{"foo": "bar"}')

        mock.parse.assert_called_once()

    def test_from_json_skips_parse_if_checked_false(self):
        mock = Mock()
        mock.parse.return_value = {'foo': 'bar'}
        mock.deserialize.return_value = {'foo': 'bar'}

        Marshall.SCHEMA = mock
        Foo.from_json('{"foo": "bar"}', checked=False)

        mock.parse.assert_not_called()

    def test_from_dict_calls_parse_if_checked_true(self):
        mock = Mock()
        mock.parse.return_value = {'foo': 'bar'}
        mock.deserialize.return_value = {'foo': 'bar'}

        Marshall.SCHEMA = mock
        Foo.from_dict({"foo": "bar"})

        mock.parse.assert_called_once()

    def test_from_dict_skips_parse_if_checked_false(self):
        mock = Mock()
        mock.parse.return_value = {'foo': 'bar'}
        mock.deserialize.return_value = {'foo': 'bar'}

        Marshall.SCHEMA = mock
        Foo.from_dict({"foo": "bar"}, checked=False)

        mock.parse.assert_not_called()
