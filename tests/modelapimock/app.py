"""
app
***

:Author: Tobias Seydewitz
:Date: 12.12.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
import json
from os.path import join
from time import sleep

from flask import Flask
from flask import jsonify
from flask import request

from tests import test_resources


def api():
    _ = request.get_data()  # HACK solves sending RST packets
    sleep(5)

    with open(join(test_resources, 'res', 'data.json')) as src:
        payload = json.loads(src.read())

    return jsonify(payload)


def create_app() -> Flask:
    app = Flask(__name__)

    app.config['ENV'] = 'development'
    app.config['DEBUG'] = True
    app.config['TESTING'] = True
    app.config['PROPAGATE_EXCEPTIONS'] = True

    app.add_url_rule('/api/v1.0/results', view_func=api, methods=['POST'])

    return app
