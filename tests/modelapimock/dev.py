accesslog = '-'
errorlog = '-'
loglevel = 'debug'

limit_request_fields = 20

daemon = False

bind = '127.0.0.1:5001'
backlog = 0

worker_class = 'sync'
workers = 1
threads = 1
timeout = 120
graceful_timeout = 60
