"""
test_models
***********

:Author: Tobias Seydewitz
:Date: 05.10.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from unittest import TestCase
from unittest import skip
from unittest.mock import Mock

from peewee import IntegrityError
from peewee import SqliteDatabase

from proxilly.models import Output
from proxilly.models import OutputID
from proxilly.models import Pathway
from proxilly.parsers import ModelOutputsRequest


class TestOutput(TestCase):
    def setUp(self):
        self.db = SqliteDatabase(':memory:')
        self.db.connect()

        Pathway.bind(self.db)
        Output.bind(self.db)

        self.db.create_tables([Pathway, Output])

    def test_create_response_raises_error_if_entry_exists(self):
        response = Mock()
        response.as_json.return_value = 'foo'

        Output.create_response(0, response)

        with self.assertRaises(IntegrityError):
            Output.create_response(0, response)

    def test_create_response_returns_instance_of_output(self):
        response = Mock()
        response.as_json.return_value = 'foo'
        output = Output.create_response(0, response)

        self.assertTrue(isinstance(output, Output))
        self.assertEqual('foo', output.outputs)

    def test_update_response_raise_error_if_entry_does_not_exist(self):
        response = Mock()
        response.as_json.return_value = 'foo'

        with self.assertRaises(IntegrityError):
            Output.update_response(Pathway(id=1, uuid=''), response)

    def test_update_response_returns_instance_of_output(self):
        response = Mock()
        response.as_json.return_value = 'foo'
        Output.create_response(0, response)

        self.assertTrue(isinstance(Output.update_response(0, response), Output))

    def test_update_response_updates_entry(self):
        response = Mock()
        response.as_json.return_value = 'foo'

        pathway = Pathway.create(uuid='', precached=0)
        output = Output.create_response(pathway, response)

        response.as_json.return_value = 'bar'

        output = Output.update_response(pathway, response)

        self.assertEqual('bar', output.outputs)

    def tearDown(self):
        self.db.drop_tables([Pathway, Output])
        self.db.close()


class TestPathway(TestCase):
    def setUp(self):
        self.db = SqliteDatabase(':memory:')
        self.db.connect()

        Pathway.bind(self.db)
        Output.bind(self.db)

        self.db.create_tables([Pathway, Output])

    def test_create_raises_error_if_entry_for_uuid_already_exists(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={}, outputs=[], getFromModel=False)
        Pathway.create(uuid=req.uuid, levers=req.levers, cntry_levers=req.cntry_levers, cntry=req.cntry, precached=1)

        with self.assertRaises(IntegrityError):
            Pathway.create(uuid=req.uuid, levers=req.levers, cntry_levers=req.cntry_levers, cntry=req.cntry,
                           precached=1)

    def test_get_outputs_raises_error_if_pathway_entry_exists_and_output_entry_is_missing(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={}, outputs=[], getFromModel=False)
        Pathway.create(uuid=req.uuid, levers=req.levers, cntry_levers=req.cntry_levers, cntry=req.cntry, precached=1)

        with self.assertRaises(IntegrityError):
            Pathway.get_outputs(req)

    def test_get_outputs_returns_response_entry_exists(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={}, outputs=[], getFromModel=False)

        p = Pathway.create(uuid=req.uuid, levers=req.levers, cntry_levers=req.cntry_levers, cntry=req.cntry,
                           precached=1)
        Output.create(pathway=p, outputs='{"outputs": [], "warnings": [], "status": "foo"}')

        res = Pathway.get_outputs(req)

        expected = 'foo'
        actual = res.status

        self.assertEqual(expected, actual)

    def test_save_request_with_existing_entry(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={'DE': [1]}, outputs=[], getFromModel=False)
        Pathway.save_request(req)
        pathway = Pathway.save_request(req)

        expected = 'DE'
        actual = pathway.cntry

        self.assertEqual(expected, actual)

    def test_save_request_with_not_exceptional_request_1(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={}, outputs=[], getFromModel=False)
        pathway = Pathway.save_request(req)

        self.assertEqual(pathway.cntry, None)

    def test_save_request_with_not_exceptional_request_2(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={'DE': [1, 2, 3]}, outputs=[], getFromModel=False)
        pathway = Pathway.save_request(req)

        self.assertEqual(pathway.cntry, None)

    def test_save_request_with_exceptional_request(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={'DE': [4]}, outputs=[], getFromModel=False)
        pathway = Pathway.save_request(req)

        self.assertEqual(pathway.cntry, 'DE')

    def test_save_request_with_precached_true(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={'DE': [4]}, outputs=[], getFromModel=False)
        pathway = Pathway.save_request(req, precached=True)

        self.assertTrue(pathway.precached)

    def test_save_request_with_precached_false(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={'DE': [4]}, outputs=[], getFromModel=False)
        pathway = Pathway.save_request(req, precached=False)

        self.assertFalse(pathway.precached)

    def tearDown(self):
        self.db.drop_tables([Pathway])
        self.db.close()


class TestOutputID(TestCase):
    def setUp(self):
        self.db = SqliteDatabase(':memory:')
        self.db.connect()

        OutputID.bind(self.db)

        self.db.create_tables([OutputID])

    def test_add_entry_frontend_defaults_to_zero(self):
        output = OutputID.create(name='emissions')

        self.assertEqual(0, output.frontend)

    def test_add_entry_set_frontend(self):
        output = OutputID.create(name='emissions', frontend=1)

        self.assertEqual(1, output.frontend)

    def test_add_entry_violating_unique_name_raises_error(self):
        OutputID.create(name='emissions', frontend=1)

        with self.assertRaises(IntegrityError):
            OutputID.create(name='emissions', frontend=1)

    def test_get_frontend_outputs_returns_list_of_strings(self):
        OutputID.create(name='emissions', frontend=1)
        OutputID.create(name='agriculture', frontend=1)
        OutputID.create(name='minerals', frontend=1)

        self.assertTrue(all(isinstance(ele, str) for ele in OutputID.get_frontend_outputs()))

    def test_get_frontend_outputs_returns_only_frontend_supported_outputs(self):
        OutputID.create(name='emissions', frontend=1)
        OutputID.create(name='agriculture', frontend=1)
        OutputID.create(name='minerals')

        expected = ['emissions', 'agriculture']
        actual = OutputID.get_frontend_outputs()

        self.assertListEqual(expected, actual)

    def test_get_renamed_outputs(self):
        OutputID.create(name='earth', frontend=1)
        OutputID.create(name='emissions', rename='emission', frontend=1)
        OutputID.create(name='agricultures', rename='agriculture', frontend=1)

        expected = {'emissions': 'emission', 'agricultures': 'agriculture'}
        actual = OutputID.get_renamed_outputs()

        self.assertDictEqual(expected, actual)

    def test_all_outputs(self):
        OutputID.create(name='earth', frontend=1)
        OutputID.create(name='emissions', frontend=1)
        OutputID.create(name='agriculture', frontend=0)
        OutputID.create(name='buildings', frontend=0)
        OutputID.create(name='foo')

        expected = ["agriculture", "buildings", "earth", "emissions", "foo"]
        actual = OutputID.get_all_outputs()

        self.assertListEqual(expected, actual)

    @skip
    def test_value_range_constraint_on_frontend_columns(self):
        with self.assertRaises(IntegrityError):
            OutputID.create(name='foo', frontend=2)

        with self.assertRaises(IntegrityError):
            OutputID.create(name='foo', frontend=-1)

    def tearDown(self):
        self.db.close()

# TODO test LeverHeadline
# TODO test LeverGroup
# TODO test Lever
