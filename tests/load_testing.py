"""
load_testing
************

:Author: Tobias Seydewitz
:Date: 05.12.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from random import gauss
from random import randint

from locust import HttpLocust
from locust import TaskSet
from locust import between
from locust import task

from proxilly.utils.levers import LeversGenerator
from proxilly.utils.outputs import RandomizedOutputsGenerator
from proxilly.utils.payload import PayloadGenerator


endpoint = '/results'

pathway_generator = LeversGenerator('tests/res/levers.txt')
outputs_generator = RandomizedOutputsGenerator(
    'tests/res/outputs.txt',
    size=len(pathway_generator),
    max_items=40,
    all_countries=False
)
request_gen = PayloadGenerator(pathway_generator, outputs_generator)

countries = ['AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK',
             'EE', 'FI', 'FR', 'DE', 'EL', 'HU', 'IE',
             'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL',
             'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'CH',
             'UK']


def get_random_country():
    return countries[randint(0, len(countries)-1)]


class DefaultPathwayUserBehaviour(TaskSet):
    def on_start(self):
        self.new_pathway_selection()

    @task(1)
    def new_pathway_selection(self):
        self.current = request_gen.get()

        if -1 <= gauss(0, 1) <= 1:
            self.current.exceptions = {get_random_country(): self.current.default}

        self.client.post(endpoint, json=self.current.as_dict())

    @task(3)
    def query_current_pathway(self):
        self.current.outputs = outputs_generator.get()
        self.client.post(endpoint, json=self.current.as_dict())


class DefaultPathwayUser(HttpLocust):
    task_set = DefaultPathwayUserBehaviour
    wait_time = between(1, 30)
    weight = 1


class RandomPathwayUserBehaviour(TaskSet):
    def on_start(self):
        self.new_pathway_selection()

    @task(1)
    def new_pathway_selection(self):
        pass

    @task(3)
    def query_current_pathway(self):
        self.current.outputs = outputs_generator.get()
        self.client.post(endpoint, json=self.current.as_dict())


class RandomPathwayUser(HttpLocust):
    task_set = RandomPathwayUserBehaviour
    wait_time = between(1, 30)
    weight = 2
