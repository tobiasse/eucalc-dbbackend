"""
test_resources
**************

:Author: Tobias Seydewitz
:Date: 05.10.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from hashlib import md5
from os.path import join
from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import PropertyMock
from unittest.mock import patch

from numpy import all
from numpy import array
from peewee import OperationalError
from peewee import SqliteDatabase
from requests import ConnectTimeout

from proxilly import create_app
from proxilly.models import Output
from proxilly.models import OutputID
from proxilly.models import Pathway
from proxilly.parsers import ModelOutputsRequest
from proxilly.resources import Proxilly
from tests import test_resources


class TestEUCalcCache(TestCase):
    def setUp(self):
        self.decorators = Proxilly.decorators  # HACK store decorators

        self.db = SqliteDatabase(':memory:')
        self.db.connect()

        Pathway.bind(self.db)
        Output.bind(self.db)
        OutputID.bind(self.db)
        self.db.create_tables([Pathway, Output, OutputID])

        self.app = create_app(development=True)
        self.app.config['DATABASE'] = self.db

        with open(join(test_resources, 'res', 'data.json')) as src:
            pathway = Pathway.create(
                uuid=md5(('1' * 59 + 'EU').encode()).hexdigest(),
                levers='1' * 59,
                precached=True
            )

            Output.create(pathway=pathway, outputs=src.read())

            OutputID.create(name='emissions-CO2e[Mt]', frontend=1)
            OutputID.create(name='foo', frontend=1)
            OutputID.create(name='bar', frontend=1)
            OutputID.create(name='test', frontend=0)
            OutputID.create(
                name='ind_energy-demand_cement[TWh]', rename='ind_energy-demand_cement_total[TWh]', frontend=1
            )

    def tearDown(self):
        # HACK one test must be executed without the decorators but we must recover the class to its initial state
        Proxilly.decorators = self.decorators

        self.db.drop_tables([Pathway, Output, OutputID])
        self.db.close()

    def test_endpoint_returns_503_if_db_operational_error(self):
        mock_db = Mock()
        mock_db.connect.side_effect = OperationalError

        self.app.config['DATABASE'] = mock_db

        with self.app.test_client() as c:
            res = c.post('/results', json={})
            payload = res.get_json()

            self.assertTrue(payload['code'] == 503)

    def test_endpoint_returns_411_if_content_length_header_missing(self):
        with self.app.test_client() as c:
            res = c.post('/results')
            payload = res.get_json()

            self.assertTrue(payload['code'] == 411)

    def test_endpoint_returns_413_if_content_length_exceeds_limit(self):
        with self.app.test_client() as c:
            res = c.post('/results', content_length=self.app.config['MAX_CONTENT_LENGTH'] + 1)
            payload = res.get_json()

            self.assertTrue(payload['code'] == 413)

    def test_endpoint_returns_400_if_content_type_header_missing(self):
        with self.app.test_client() as c:
            res = c.post('/results', content_length=1, json={}, content_type=None)
            payload = res.get_json()

            self.assertTrue(payload['code'] == 400)

    def test_endpoint_returns_400_if_wrong_content_type(self):
        with self.app.test_client() as c:
            res = c.post('/results', content_length=1, content_type='application/pdf', json={})
            payload = res.get_json()

            self.assertTrue(payload['code'] == 400)

    def test_endpoint_returns_200_if_content_type_has_extra_para(self):
        data = {'levers': {'default': [1] * 59, 'exceptions': {}},
                'outputs': []}

        with self.app.test_client() as c:
            res = c.post('/results', content_length=1, content_type='application/json;charset=utf-8', json=data)

            self.assertTrue(res.status_code == 200)

    def test_endpoint_retunrs_400_if_request_has_structure_violation(self):
        data = {'levers': {'default': [1, True, 3, 'a'], 'exceptions': {}}}

        with self.app.test_client() as c:
            res = c.post('/results', json=data)
            payload = res.get_json()

            self.assertTrue(payload['code'] == 400)

    def test_endpoint_returns_empty_outputs_if_request_has_empty_outputs(self):
        data = {'levers': {'default': [1] * 59, 'exceptions': {}},
                'outputs': []}

        with self.app.test_client() as c:
            res = c.post('/results', json=data)
            payload = res.get_json()

            self.assertTrue(payload['outputs'] == list())

    def test_endpoint_returns_data_for_all_countries_if_requested(self):
        data = {'levers': {'default': [1] * 59, 'exceptions': {}},
                'outputs': [{'id': 'emissions-CO2e[Mt]', 'allCountries': True}]}

        with self.app.test_client() as c:
            res = c.post('/results', json=data)
            payload = res.get_json()

            # If data has more than one key it must be all countries
            self.assertTrue(len(payload['outputs'][0]['data'].keys()) > 1)

    def test_endpoint_returns_exclusive_eu_and_exception_country_data(self):
        req = ModelOutputsRequest(default=[1] * 59, exceptions={'BE': [1] * 59}, outputs=[{'id': 'emissions-CO2e[Mt]'}],
                                  getFromModel=False)

        with self.app.test_client() as c:
            res = c.post('/results', json=req.as_dict())
            payload = res.get_json()

        expected = {'BE', 'EU'}
        actual = set(payload['outputs'][0]['data'].keys())
        self.assertFalse(expected ^ actual)

    def test_endpoint_returns_nodata_status_if_no_db_entry_and_getFromModel_false(self):
        req = ModelOutputsRequest(default=[2] * 59, exceptions={}, outputs=[], getFromModel=False)

        with self.app.test_client() as c:
            res = c.post('/results', json=req.as_dict())
            payload = res.get_json()

        expected = 'nodata'
        actual = payload['status']
        self.assertEqual(expected, actual)

    def test_endpoint_returns_data_if_db_entry_and_getFromModel_true(self):
        req = ModelOutputsRequest(default=[1] * 59, exceptions={'BE': [1] * 59}, outputs=[{'id': 'emissions-CO2e[Mt]'}],
                                  getFromModel=True)

        with self.app.test_client() as c:
            res = c.post('/results', json=req.as_dict())
            payload = res.get_json()

        expected = ''
        actual = payload['status']
        self.assertEqual(expected, actual)

    def test_endpoint_returns_busy_if_getFromModel_true_no_db_entry_and_connection_error(self):
        req = ModelOutputsRequest(default=[2] * 59, exceptions={}, outputs=[], getFromModel=True)

        with self.app.test_client() as c:
            res = c.post('/results', json=req.as_dict())
            payload = res.get_json()

        expected = 'busy'
        actual = payload['status']
        self.assertEqual(expected, actual)

    def test_endpoint_returns_model_data_if_getFromModel_true_and_no_db_entry(self):
        req = ModelOutputsRequest(default=[2] * 59, exceptions={}, outputs=[], getFromModel=True)

        response = PropertyMock()
        response.text = '{"outputs": [], "warnings": []}'
        response.content = '{"outputs": [], "warnings": []}'

        with patch('proxilly.resources.POST', return_value=response) as mock:
            with self.app.test_client() as c:
                res = c.post('/results', json=req.as_dict())
                payload = res.get_json()

            mock.assert_called_once()

        self.assertTrue(payload['status'] == '')

    def test_endpoint_returns_400_data_if_getFromModel_true_and_no_db_entry_and_modelapi_returns_wrong_data(self):
        req = ModelOutputsRequest(default=[2] * 59, exceptions={}, outputs=[], getFromModel=True)

        response = PropertyMock()
        response.text = '{"foo": []}'
        response.content = '{"foo": []}'

        with patch('proxilly.resources.POST', return_value=response) as mock:
            with self.app.test_client() as c:
                res = c.post('/results', json=req.as_dict())
                payload = res.get_json()

            mock.assert_called_once()

        self.assertTrue(payload['code'] == 400)

    def test_endpoint_returns_busy_if_getFromModel_true_and_no_db_entry_and_connection_timeout(self):
        req = ModelOutputsRequest(default=[2] * 59, exceptions={}, outputs=[], getFromModel=True)

        with patch('proxilly.resources.POST', side_effect=ConnectTimeout) as mock:
            with self.app.test_client() as c:
                res = c.post('/results', json=req.as_dict())
                payload = res.get_json()

            mock.assert_called_once()

        self.assertTrue(payload['status'] == 'busy')

    def test_endpoint_calls_modelapi_with_frontend_outputs_true_if_request_not_exceptional(self):
        req = ModelOutputsRequest(default=[2] * 59, exceptions={'BE': [2] * 59}, outputs=[{'id': 'foo'}],
                                  getFromModel=True)

        response = PropertyMock()
        response.text = '{"outputs": [], "warnings": []}'
        response.content = '{"outputs": [], "warnings": []}'

        with patch('proxilly.resources.POST', return_value=response) as mock:
            with self.app.test_client() as c:
                res = c.post('/results', json=req.as_dict())
                payload = res.get_json()

            mock.assert_called_once()

            expected = ModelOutputsRequest(
                default=[2] * 59,
                exceptions={'BE': [2] * 59},
                outputs=[
                    {'id': 'emissions-CO2e[Mt]', 'allCountries': True},
                    {'id': 'foo', 'allCountries': True},
                    {'id': 'bar', 'allCountries': True},
                    {'id': 'ind_energy-demand_cement[TWh]', 'allCountries': True},
                ],
                getFromModel=True
            ).as_dict()
            actual = mock.call_args[1]['json']

            self.assertDictEqual(expected, actual)

    def test_endpoint_calls_modelapi_with_frontend_outputs_false_and_emissions_true_if_request_exceptional(self):
        req = ModelOutputsRequest(default=[2] * 59, exceptions={'BE': [1] * 59}, outputs=[{'id': 'foo'}],
                                  getFromModel=True)

        response = PropertyMock()
        response.text = '{"outputs": [], "warnings": []}'
        response.content = '{"outputs": [], "warnings": []}'

        with patch('proxilly.resources.POST', return_value=response) as mock:
            with self.app.test_client() as c:
                res = c.post('/results', json=req.as_dict())
                payload = res.get_json()

            mock.assert_called_once()

            expected = ModelOutputsRequest(
                default=[2] * 59,
                exceptions={'BE': [1] * 59},
                outputs=[
                    {'id': 'emissions-CO2e[Mt]', 'allCountries': True},
                    {'id': 'foo', 'allCountries': False},
                    {'id': 'bar', 'allCountries': False},
                    {'id': 'ind_energy-demand_cement[TWh]', 'allCountries': False},
                ],
                getFromModel=True
            ).as_dict()
            actual = mock.call_args[1]['json']

            self.assertDictEqual(expected, actual)

    def test_endpoint_returns_filtered_response_after_model_api_call(self):
        req = ModelOutputsRequest(default=[2] * 59, exceptions={'BE': [2] * 59}, outputs=[{'id': 'foo'}],
                                  getFromModel=True)

        response = PropertyMock()
        response.text = """{
        "outputs": [
        {"id": "emissions-CO2e[Mt]", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
        {"id": "foo", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
        {"id": "bar", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}}
        ],
        "warnings": []}
        """
        response.content = """{
        "outputs": [
        {"id": "emissions-CO2e[Mt]", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
        {"id": "foo", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
        {"id": "bar", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}}
        ],
        "warnings": []}
        """

        with patch('proxilly.resources.POST', return_value=response) as mock:
            with self.app.test_client() as c:
                res = c.post('/results', json=req.as_dict())
                payload = res.get_json()

            mock.assert_called_once()

            expected = {
                'outputs': [{'id': 'foo', 'timeAxis': [1, 2], 'title': '', 'data': {'EU': [1, 2], 'BE': [1, 2]}}],
                'warnings': [],
                'status': ''
            }
            actual = payload

            self.assertDictEqual(expected, actual)

    def test_endpoint_performs_dynamic_caching(self):
        # HACK for this test we must deactivate the decorators (especially connect_db) otherwise the memory db is
        # destroyed after a endpoint call
        Proxilly.decorators = []
        self.app = create_app(development=True)
        self.app.config['DATABASE'] = self.db

        req1 = ModelOutputsRequest(default=[2] * 59, exceptions={'BE': [2] * 59}, outputs=[{'id': 'foo'}],
                                   getFromModel=True)
        req2 = ModelOutputsRequest(default=[2] * 59, exceptions={}, outputs=[{'id': 'foo'}], getFromModel=False)

        response = PropertyMock()
        response.text = """{
        "outputs": [
        {"id": "emissions-CO2e[Mt]", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
        {"id": "foo", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
        {"id": "bar", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}}
        ],
        "warnings": []}
        """
        response.content = """{
        "outputs": [
        {"id": "emissions-CO2e[Mt]", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
        {"id": "foo", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
        {"id": "bar", "timeAxis": [1, 2], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}}
        ],
        "warnings": []}
        """

        with patch('proxilly.resources.POST', return_value=response) as mock:
            with self.app.test_client() as c:
                res = c.post('/results', json=req1.as_dict())
                res = c.post('/results', json=req2.as_dict())
                payload = res.get_json()

            expected = {
                'outputs': [{'id': 'foo', 'timeAxis': [1, 2], 'title': '', 'data': {'EU': [1, 2]}}],
                'warnings': [],
                'status': ''
            }
            actual = payload

            self.assertDictEqual(expected, actual)

    def test_localised(self):
        req = ModelOutputsRequest(
            default=[2] * 59,
            exceptions={'BE': [2] * 59},
            outputs=[{'id': 'foo'}, {'id': 'bar'}, {'id': 'baz'}, {'id': 'ind_energy-demand_cement[TWh]'}],
            getFromModel=True
        )

        response = PropertyMock()
        response.text = """{
                "outputs": [
                {"id": "foo", "timeAxis": [2015, 2020], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
                {"id": "bar", "timeAxis": [2015, 2020], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
                {"id": "ind_energy-demand_cement[TWh]", "timeAxis": [2015, 2020], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}}
                ],
                "warnings": []}
                """
        response.content = """{
                "outputs": [
                {"id": "foo", "timeAxis": [2015, 2020], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
                {"id": "bar", "timeAxis": [2015, 2020], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}},
                {"id": "ind_energy-demand_cement[TWh]", "timeAxis": [2015, 2020], "title": "", "data": {"EU": [1, 2], "BE": [1, 2], "DE": [1, 2]}}
                ],
                "warnings": []}
                """

        with patch('proxilly.resources.POST', return_value=response) as mock:
            with self.app.test_client() as c:
                res = c.post('/localised', json=req.as_dict())
                payload = res.get_json()

            mock.assert_called_once()

            expected = {
                'outputs': [
                    {'id': 'foo', 'timeAxis': [2015, 2020], 'title': '', 'data': {'BE': [1, 2]}},
                    {'id': 'bar', 'timeAxis': [2015, 2020], 'title': '', 'data': {'BE': [1, 2]}},
                    {'id': 'ind_energy-demand_cement_total[TWh]', 'timeAxis': [2015, 2020], 'title': '', 'data': {'BE': [1, 2]}}
                ],
                'warnings': [],
                'status': ''
            }
            actual = payload

            self.assertDictEqual(expected, actual)