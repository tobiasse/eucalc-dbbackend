"""
test_parsers
************

:Author: Tobias Seydewitz
:Date: 06.10.19
:Mail: seydewitz@pik-potsdam.de
:Institution: `Potsdam Institute for Climate Impact Research (PIK) <https://www.pik-potsdam.de/>`_
"""
from hashlib import md5
from unittest import TestCase

from proxilly.marshalling.errors import ValidationError
from proxilly.parsers import ModelOutputsRequest
from proxilly.parsers import ModelOutputsResponse
from proxilly.parsers import normalize


class TestModelOutputsRequest(TestCase):
    def test_init_with_empty_exceptions(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={}, getFromModel=False, outputs=[])

        self.assertListEqual(['EU'], req.countries)
        self.assertEqual('123', req.cntry_levers)

    def test_init_with_exceptions_set(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={'UK': [4, 5, 6]}, getFromModel=False, outputs=[])

        self.assertEqual('UK', req.cntry)
        self.assertListEqual(['EU', 'UK'], req.countries)
        self.assertEqual('456', req.cntry_levers)

    def test_uuid_returns_hash_if_exceptions_empty(self):
        req = ModelOutputsRequest(default=[1, 2, 3], exceptions={}, getFromModel=False, outputs=[])

        self.assertTrue(isinstance(req.uuid, str))

    def test_uuid_returns_same_hash_if_exceptions_has_same_lever_config_as_levers(self):
        req1 = ModelOutputsRequest(default=[1, 2, 3], exceptions={}, getFromModel=False, outputs=[])
        req2 = ModelOutputsRequest(default=[1, 2, 3], exceptions={'UK': [1, 2, 3]}, getFromModel=False, outputs=[])

        self.assertEqual(req1.uuid, req2.uuid)

    def test_uuid_differs_if_exceptions_pathway_is_similar_but_country_differs(self):
        req1 = ModelOutputsRequest(default=[1, 2, 3], exceptions={'DE': [1, 3]}, getFromModel=False, outputs=[])
        req2 = ModelOutputsRequest(default=[1, 2, 3], exceptions={'UK': [1, 3]}, getFromModel=False, outputs=[])

        self.assertNotEqual(req1, req2)

    def test_uuid_returns_valid_hash_for_non_exceptional_request_1(self):
        req = ModelOutputsRequest(default=[1, 2], exceptions={}, outputs=[], getFromModel=True)

        expected = md5((req.levers + req.default_cntry).encode()).hexdigest()
        actual = req.uuid

        self.assertEqual(expected, actual)

    def test_uuid_returns_valid_hash_for_non_exceptional_request_2(self):
        req = ModelOutputsRequest(default=[1, 2], exceptions={'DE': [1, 2]}, outputs=[], getFromModel=True)

        expected = md5((req.levers + req.default_cntry).encode()).hexdigest()
        actual = req.uuid

        self.assertEqual(expected, actual)

    def test_uuid_returns_valid_hash_for_exceptional_request(self):
        req = ModelOutputsRequest(default=[1, 2], exceptions={'DE': [1, 3]}, outputs=[], getFromModel=True)

        expected = md5((req.levers + req.cntry_levers + req.default_cntry + req.cntry).encode()).hexdigest()
        actual = req.uuid

        self.assertEqual(expected, actual)

    def test_is_exceptional_returns_false_if_request_is_not_exceptional_1(self):
        req = ModelOutputsRequest(default=[1, 2], exceptions={}, outputs=[], getFromModel=True)

        self.assertFalse(req.is_exceptional())

    def test_is_exceptional_returns_false_if_request_is_not_exceptional_2(self):
        req = ModelOutputsRequest(default=[1, 2], exceptions={'DE': [1, 2]}, outputs=[], getFromModel=True)

        self.assertFalse(req.is_exceptional())

    def test_is_exceptional_returns_true_if_request_is_exceptional(self):
        req = ModelOutputsRequest(default=[1, 2], exceptions={'DE': [5, 4]}, outputs=[], getFromModel=True)

        self.assertTrue(req.is_exceptional())

    def test_countries_returns_EU_if_no_exception_country_is_provided(self):
        req = ModelOutputsRequest.from_dict({
            'levers': {'default': [], 'exceptions': {}},
            'outputs': []
        })

        expected = ['EU']
        actual = req.countries

        self.assertListEqual(expected, actual)

    def test_countries_returns_EU_if_exception_country_not_match_convention(self):
        req = ModelOutputsRequest.from_dict({
            'levers': {'default': [], 'exceptions': {'Foo': [], 'Bar': []}},
            'outputs': []
        })

        expected = ['EU']
        actual = req.countries

        self.assertListEqual(expected, actual)

    def test_countries_returns_EU_and_GB_if_exception_country_matches(self):
        req = ModelOutputsRequest.from_dict({
            'levers': {'default': [], 'exceptions': {'GB': []}},
            'outputs': []
        })

        expected = ['EU', 'GB']
        actual = req.countries

        self.assertListEqual(expected, actual)

    def test_country_returns_EU_if_no_exception_country_is_provided(self):
        req = ModelOutputsRequest.from_dict({
            'levers': {'default': [], 'exceptions': {}},
            'outputs': []
        })

        expected = 'EU'
        actual = req.country

        self.assertEqual(expected, actual)

    def test_country_returns_EU_if_exception_country_not_match_convention(self):
        req = ModelOutputsRequest.from_dict({
            'levers': {'default': [], 'exceptions': {'Foo': [], 'Bar': []}},
            'outputs': []
        })

        expected = 'EU'
        actual = req.country

        self.assertEqual(expected, actual)

    def test_country_returns_EU_and_GB_if_exception_country_matches(self):
        req = ModelOutputsRequest.from_dict({
            'levers': {'default': [], 'exceptions': {'GB': []}},
            'outputs': []
        })

        expected = 'GB'
        actual = req.country

        self.assertEqual(expected, actual)

    def test_all_countries_returns_attribute_values_for_ids(self):
        req = ModelOutputsRequest.from_dict({
            'levers': {'default': [], 'exceptions': {}},
            'outputs': [{'id': 'Foo'}, {'id': 'Bar', 'allCountries': True}]
        })

        self.assertTrue(req.all_countries('Bar'))
        self.assertFalse(req.all_countries('Foo'))

    def test_contains_returns_true_if_output_id_in_request(self):
        req = ModelOutputsRequest.from_dict({
            'levers': {'default': [], 'exceptions': {}},
            'outputs': [{'id': 'Foo'}, {'id': 'Bar', 'allCountries': True}]
        })

        self.assertTrue('Foo' in req)
        self.assertFalse('test' in req)

    def test_successful_parse_from_valid_request(self):
        struct = {
            'levers': {
                'default': [1, 2, 3],
                'exceptions': {'BE': [1, 2, 3]}
            },
            'outputs': [{'id': 'foo'}]
        }

        expected = {
            'levers': {'default': [1, 2, 3], 'exceptions': {'BE': [1, 2, 3]}},
            'outputs': [{'id': 'foo', 'allCountries': False}],
            'getFromModel': False
        }
        actual = ModelOutputsRequest.SCHEMA.parse(struct)

        self.assertDictEqual(expected, actual)

    def test_filter(self):
        struct = {
            'levers': {
                'default': [1, 2, 3],
                'exceptions': {'BE': [1, 2, 3]}
            },
            'outputs': [
                {'id': 'emissions', 'allCountries': True}, {'id': 'buildings', 'allCountries': False},
                {'id': 'agriculture'}, {'id': 'foo'}, {'id': 'bar'}
            ]
        }
        req = ModelOutputsRequest.from_dict(struct)

        expected = [
            {'id': 'emissions', 'allCountries': True}, {'id': 'buildings', 'allCountries': False},
            {'id': 'agriculture', 'allCountries': False}]
        actual = req.filter(['emissions', 'buildings', 'agriculture']).outputs
        self.assertListEqual(expected, actual)

    def test_parse_throws_error_if_request_is_invalid(self):
        struct = {
            'levers': {
                'default': [1, 2, 3],
                'exceptions': {'BE': [1, 2, 3]}
            },
        }

        with self.assertRaises(ValidationError):
            ModelOutputsRequest.SCHEMA.parse(struct)


class TestModelOutputsResponse(TestCase):
    def test_successful_parse_from_valid_response(self):
        struct = {
            'outputs': [{
                'id': 'foo',
                'title': 'bar',
                'timeAxis': [1995, 1996, 1997],
                'data': {'EU': [.0, .0, .0], 'UK': [.0, .0, .0]}
            }],
            'warnings': [{'id': 'water', 'level': 3.5}],
        }

        expected = {
            'outputs': [{
                'id': 'foo',
                'title': 'bar',
                'timeAxis': [1995, 1996, 1997],
                'data': {'EU': [.0, .0, .0], 'UK': [.0, .0, .0]}
            }],
            'warnings': [{'id': 'water', 'level': 3.5}],
            'status': ''
        }
        actual = ModelOutputsResponse.SCHEMA.parse(struct)

        self.assertDictEqual(expected, actual)

    def test_parse_throws_error_if_response_is_invalid(self):
        struct = {
            'outputs': [{
                'title': 'bar',
                'timeAxis': [1995, 1996, 1997],
                'data': {'EU': [.0, .0, .0], 'UK': [.0, .0, .0]}
            }],
            'warnings': [{'id': 'water', 'level': 3.5}],
        }

        with self.assertRaises(ValidationError):
            ModelOutputsResponse.SCHEMA.parse(struct)

    def test_filter_unavailable_outputs(self):
        req = ModelOutputsRequest([], {}, [{'id': 'Foo'}, {'id': 'Bar'}], False)
        res = ModelOutputsResponse([{'id': 'Test'}, {'id': 'FooBar'}], [])

        res = res.filter(req)

        self.assertEqual(res.outputs, [])

    def test_filter_outputs(self):
        req = ModelOutputsRequest([], {}, [{'id': 'Foo', 'allCountries': False}], False)
        res = ModelOutputsResponse([{'id': 'Foo', 'data': {'EU': [], 'BE': [], 'DE': []}}, {'id': 'FooBar'}], [])

        res = res.filter(req)

        self.assertTrue(len(res.outputs) == 1)
        self.assertTrue(len(res.outputs[0]['data'].keys()) == 1)

    def test_filter_outputs_all_countries(self):
        req = ModelOutputsRequest([], {}, [{'id': 'Foo', 'allCountries': True}], False)
        res = ModelOutputsResponse([{'id': 'Foo', 'data': {'EU': [], 'BE': [], 'DE': []}}, {'id': 'FooBar'}], [])

        res = res.filter(req)

        self.assertTrue(len(res.outputs[0]['data'].keys()) == 3)

    def test_filter_outputs_with_not_existing_country(self):
        req = ModelOutputsRequest([], {'Foo': []}, [{'id': 'Foo', 'allCountries': False}], False)
        res = ModelOutputsResponse([{'id': 'Foo', 'data': {'EU': [], 'BE': [], 'DE': []}}], [])

        res = res.filter(req)

        self.assertTrue(len(res.outputs[0]['data'].keys()) == 1)

    def test_filter_outputs_with_exception_country(self):
        req = ModelOutputsRequest([], {'DE': []}, [{'id': 'Foo', 'allCountries': False}], False)
        res = ModelOutputsResponse([{'id': 'Foo', 'data': {'EU': [], 'BE': [], 'DE': []}}], [])

        res = res.filter(req)

        self.assertTrue(len(res.outputs[0]['data'].keys()) == 2)

    def test_rename(self):
        res = ModelOutputsResponse([{'id': 'foo'}, {'id': 'test'}, {'id': 'emissions'}], [])

        expected = [{'id': 'bar'}, {'id': 'notest'}, {'id': 'emissions'}]
        actual = res.rename({'foo': 'bar', 'test': 'notest'}).outputs

        self.assertListEqual(expected, actual)

    def test_trim(self):
        res = ModelOutputsResponse([
            {'id': 'timber', 'title': 'Timber', 'timeAxis': [2020, 2025, 2030, 2035, 2040, 2045, 2050],
             'data': {'EU': [1, 2, 3, 4, 5, 6, 7], 'BE': [1, 2, 3, 4, 5, 6, 7]}},
            {'id': 'crops', 'title': 'Crops', 'timeAxis': [2010, 2015, 2020, 2025, 2030, 2035, 2040, 2045, 2050],
             'data': {'EU': [1, 2, 3, 4, 5, 6, 7, 8, 9], 'BE': [1, 2, 3, 4, 5, 6, 7, 8, 9]}},
            {'id': 'crops', 'title': 'Crops', 'timeAxis': [2010, 2015, 2020, 2025, 2030, 2035, 2040, 2045, 2050],
             'data': {'EU': [0, 0, 0, 0, 0, 0, 0, 0, 0], 'BE': [0, 0, 0, 0, 0, 0, 0, 0, 0]}},
            {'id': 'crops', 'title': 'Crops', 'timeAxis': list(range(1990, 2100)),
             'data': {'EU': list(range(1990, 2100)), 'BE': list(range(1990, 2100))}},
            {'id': 'metal', 'title': 'Metal', 'timeAxis': [2015], 'data': {}},
            {'id': 'emissions', 'title': 'Emissions', 'timeAxis': [2050], 'data': {}},
            {'id': 'metal', 'title': 'Metal', 'timeAxis': [2015], 'data': {'EU': [1]}},
            {'id': 'emissions', 'title': 'Emissions', 'timeAxis': [2050], 'data': {'EU': [1]}},
            {'id': 'metal', 'title': 'Metal', 'timeAxis': [2015], 'data': {'EU': [1], 'GB': [2]}},
            {'id': 'emissions', 'title': 'Emissions', 'timeAxis': [2050], 'data': {'EU': [1], 'GB': [2]}},
        ], []).trim(2020)

        expected = [
            {'id': 'timber', 'title': 'Timber', 'timeAxis': [2020, 2025, 2030, 2035, 2040, 2045, 2050],
             'data': {'EU': [1, 2, 3, 4, 5, 6, 7], 'BE': [1, 2, 3, 4, 5, 6, 7]}},
            {'id': 'crops', 'title': 'Crops', 'timeAxis': [2020, 2025, 2030, 2035, 2040, 2045, 2050],
             'data': {'EU': [3, 4, 5, 6, 7, 8, 9], 'BE': [3, 4, 5, 6, 7, 8, 9]}},
            {'id': 'crops', 'title': 'Crops', 'timeAxis': [2020, 2025, 2030, 2035, 2040, 2045, 2050],
             'data': {'EU': [0, 0, 0, 0, 0, 0, 0], 'BE': [0, 0, 0, 0, 0, 0, 0]}},
            {'id': 'crops', 'title': 'Crops', 'timeAxis': list(range(2020, 2100)),
             'data': {'EU': list(range(2020, 2100)), 'BE': list(range(2020, 2100))}},
            {'id': 'metal', 'title': 'Metal', 'timeAxis': [], 'data': {}},
            {'id': 'emissions', 'title': 'Emissions', 'timeAxis': [2050], 'data': {}},
            {'id': 'metal', 'title': 'Metal', 'timeAxis': [], 'data': {}},
            {'id': 'emissions', 'title': 'Emissions', 'timeAxis': [2050], 'data': {'EU': [1]}},
            {'id': 'metal', 'title': 'Metal', 'timeAxis': [], 'data': {}},
            {'id': 'emissions', 'title': 'Emissions', 'timeAxis': [2050], 'data': {'EU': [1], 'GB': [2]}},
        ]
        actual = res.outputs

        self.assertListEqual(expected, actual)

    def test_singular(self):
        req = ModelOutputsRequest([], {}, [{'id': 'Foo', 'allCountries': False}], False)
        res = ModelOutputsResponse([{'id': 'Foo', 'data': {'EU': [], 'BE': [], 'DE': []}}], []).singular(req)

        expected = [{'id': 'Foo', 'data': {'EU': []}}]
        actual = res.outputs

        self.assertListEqual(expected, actual)

    def test_singular_exception(self):
        req = ModelOutputsRequest([], {'BE': []}, [{'id': 'Foo', 'allCountries': False}], False)
        res = ModelOutputsResponse([{'id': 'Foo', 'data': {'EU': [], 'BE': [], 'DE': []}}], []).singular(req)

        expected = [{'id': 'Foo', 'data': {'BE': []}}]
        actual = res.outputs

        self.assertListEqual(expected, actual)

    def test_singular_exception_only_eu_available(self):
        req = ModelOutputsRequest([], {'BE': []}, [{'id': 'Foo', 'allCountries': False}], False)
        res = ModelOutputsResponse([{'id': 'Foo', 'data': {'EU': []}}], []).singular(req)

        expected = [{'id': 'Foo', 'data': {}}]
        actual = res.outputs

        self.assertListEqual(expected, actual)

    def test_remove(self):
        res = ModelOutputsResponse([{'id': 'Foo', 'data': {'EU': []}}, {'id': 'Bar', 'data': {}}], []).remove()

        expected = [{'id': 'Foo', 'data': {'EU': []}}]
        actual = res.outputs

        self.assertListEqual(expected, actual)


class TestFunctions(TestCase):
    def test_normalize(self):
        expected = [1, 2, 3, 4, 1.1, 2.4, 3.55]
        actual = normalize([1.0, 2.0, 3.0, 4.0, 1.1, 2.4, 3.55])

        self.assertListEqual(expected, actual)
